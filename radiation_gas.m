function [Q_flu_gas_to_wall, sum_Quant] = radiation_gas(T_wall, T_avg, ... %T_adiabatic, C_p_avg, m_dot_gases, ... % these must be calculated
                                           s_layer_thickness, epsilon_inf, k_flame_parameter, epsilon_wall, C_s_black_body, A_wall) % these are fixed parameters


% find emissivity of flu gas
epsilon_flu_gas = epsilon_inf * (1 - exp( -k_flame_parameter * s_layer_thickness ) ); 

% find effective emissivity of flu gas and wall
epsilon_FW_effective = ((epsilon_flu_gas)^-1 +(epsilon_wall)^-1 -1)^-1;

% find nondimentional Konokov number
% K_0_Konakov = C_p_avg * m_dot_gases / (epsilon_FW_effective * C_s_black_body * A_wall * T_adiabatic^3);

% calculate exit temeprature of flu gas
% T_exit = T_adiabatic/2 * ( -K_0_Konakov + sqrt(K_0_Konakov^2 + 4*(T_wall/T_adiabatic)^4 + 4*K_0_Konakov) ); 

% calculate heat transfer rate between flu gas and wall (could have used
% radiative eqution instead of heat capacity "m-cAT", but result is the same)
% Q_flu_gas_to_wall= C_p_avg * m_dot_gases * (T_adiabatic - T_exit);


% for radiative heat transfer, you are supposed to use the area of the
% perpendicular plane on heat transfer (so the wall area), not the total
% pipe surface area 

% if all we care about is heat transfer, lets just skip to that:
Q_flu_gas_to_wall = epsilon_FW_effective .* C_s_black_body .* A_wall .* (T_avg.^4 - T_wall.^4);
sum_Quant = sum(C_s_black_body .* A_wall .* (T_avg.^4 - T_wall.^4));
% slag heat capacity: https://www.researchgate.net/figure/Specific-heat-of-slag-samples-with-different-thermal-histories-x-As-received_fig2_262764649
