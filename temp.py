# https://stackoverflow.com/questions/34199233/how-to-prevent-tensorflow-from-allocating-the-totality-of-a-gpu-memory
#import tensorflow as tf
#gpus = tf.config.experimental.list_physical_devices('GPU')
#for gpu in gpus:
#  tf.config.experimental.set_memory_growth(gpu, True)

import matplotlib.pyplot as plt
import numpy as np
import pickle

return_values_np, predicted_fuel_array, predicted_CV_array = pickle.load( open( "machine_learning_data/8_best_fast_up_SS/aaa_most_recent_results_32.p", "rb" ) )

time = return_values_np[:,0]
CV = return_values_np[:,1]
SP = return_values_np[:,2]
thermal_efficiency = return_values_np[:,3]
total_power_MW = return_values_np[:,4]
fuel_fraction = return_values_np[:,5]
n_inj_i = return_values_np[:,6:16]
n_inj_noBias_i = return_values_np[:,16:26]
normalized_power = return_values_np[:,26]
power_SP = return_values_np[:,27]

plt.plot(time,CV,label='CV')
plt.plot(time,SP,label='SP')
plt.legend()
plt.show()


