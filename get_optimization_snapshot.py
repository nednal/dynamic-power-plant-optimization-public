
import pickle
from plot_results import plot_results
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.models import load_model
from predict_horizon import make_prediction

def get_past_and_future_inputs_outputs(return_values_np, current_timestep, num_past_timesteps, num_future_timesteps, timestep_size=60):
    # note: return_values_np is indexed by seconds, not minutes

    current_index = current_timestep * timestep_size
    
    print('current_index_seconds',current_index)
    print('num_past_timesteps',num_past_timesteps)
    print('num_future_timesteps',num_future_timesteps)
    
    injection_normalization_factor = 2000
    time = return_values_np[:,0]
    CV = return_values_np[:,1]
    total_power_MW = return_values_np[:,4]
    fuel_fraction = return_values_np[:,5]
    n_inj_i = return_values_np[:,6:16]
    n_inj_noBias_i = return_values_np[:,16:26]
    normalized_power = return_values_np[:,26]
    load_fraction_raw = return_values_np[:,27]
    
    bias_percent = (np.array(n_inj_i) - np.array(n_inj_noBias_i)) / injection_normalization_factor
    bias_filter = [0,1,2,3,4,5,7,8] # drop out the bias values that are always zero
    bias_percent = bias_percent[:,bias_filter]
    
    load_fraction = np.array(load_fraction_raw).reshape([len(load_fraction_raw),1])
    bias_and_load = np.hstack((bias_percent,load_fraction))
    bias_and_load = bias_and_load.tolist()
    
    # we want a total of num_timesteps values, separated by timestep_size indexes
    print('total indexes:',len(return_values_np))
    index_start_past = current_index - timestep_size * (num_past_timesteps-1)
    index_end_past = current_index + 1
    print('index_start_past',index_start_past)
    print('index_end_past',index_end_past)

    past_inputs = bias_and_load[index_start_past:index_end_past:timestep_size]
    past_output_fuel = fuel_fraction[index_start_past:index_end_past:timestep_size]
    past_output_CV = CV[index_start_past:index_end_past:timestep_size]

    index_start_future = current_index + timestep_size
    index_end_future = index_start_future + timestep_size * num_future_timesteps
    print('index_start_future',index_start_future)
    print('index_end_future',index_end_future)

    future_load = load_fraction_raw[index_start_future:index_end_future:timestep_size]
    
    return past_inputs, past_output_fuel, past_output_CV, future_load

def get_objective_function(sim_file, current_index, num_past_timesteps, num_future_timesteps, keras_file_obj, keras_file_con, timestep_size = 60, SP = 0.04, bias_limit = 0.25, dMax = 0.5, use_constraints=True):
    # NOTE that CV model/predictions has been disabled

    load_values = pickle.load( open( sim_file, "rb" ) )
    return_values_np, predicted_fuel_array, predicted_CV_array = load_values
    predicted_time_array = np.arange(1,len(predicted_fuel_array)+1) * 60 # convert to seconds
    past_inputs, past_output_fuel, past_output_CV, future_load = get_past_and_future_inputs_outputs(return_values_np, current_index, num_past_timesteps, num_future_timesteps)

    print('\npast_inputs',past_inputs)
    print('\npast_output_fuel',past_output_fuel)
    print('\npast_output_CV',past_output_CV)
    print('\nfuture_load',future_load)

##    return None

    duplicate_filer = []
    decision_frequency = 1
    num_predictions = 10
    for i in range(int(num_predictions/decision_frequency)):
        for j in range(decision_frequency):
            duplicate_filer.append(i)

    if SP is not 0.4:
        print('using SP constraint',SP)
    
    model_obj = load_model(keras_file_obj) # preload the model so it only does it once
    model_con = load_model(keras_file_con) # preload the model so it only does it once
    
    future_load = np.reshape(future_load, [len(future_load),1])
    past_output_fuel = np.reshape(past_output_fuel, [len(past_output_fuel),1])
    
    matrix_shape = (int(num_predictions/decision_frequency),8) # 8 bias values
    array_shape = matrix_shape[0] * matrix_shape[1]
    num_timesteps = matrix_shape[0]
    
    lb = np.ones(array_shape) * -bias_limit
    ub = np.ones(array_shape) * bias_limit
    lb_array = lb.reshape(matrix_shape)
    ub_array = ub.reshape(matrix_shape)

    current_bias = np.array(past_inputs)[-1,:-1]
    def scale_dMax(bias_matrix):
        # scale from (0-1) to the desired range given dMax and ub/lb
        # row = time, col = air level
        previous_timestep = current_bias
        for t in range(num_timesteps):
            low = np.maximum(lb_array[t,:], previous_timestep - dMax)
            high = np.minimum(ub_array[t,:], previous_timestep + dMax)
            bias_matrix[t,:] = bias_matrix[t,:] * (high - low) + low
            previous_timestep = bias_matrix[t,:]
        return bias_matrix
    
    def obj_function(bias_array_guess_raw):
        # make a copy. This is super important, or it actually messes with the PSO
        bias_matrix_guess = np.copy(bias_array_guess_raw)
        bias_matrix_guess = bias_matrix_guess.reshape(matrix_shape)
        if use_constraints:
            bias_matrix_guess = scale_dMax(bias_matrix_guess)
        # apply the duplication filter to allow for variable step-size
        future_inputs_guess = bias_matrix_guess[duplicate_filer]
        future_inputs_guess = np.hstack([future_inputs_guess, future_load])
        # sum of predicted fuel
        predicted_fuel_sum = np.sum(make_prediction(past_output_fuel, past_inputs, future_inputs_guess, keras_file = keras_file_obj, model=model_obj))
        # array of predicted CV values
        predicted_CV_array = make_prediction(past_output_CV, past_inputs, future_inputs_guess, keras_file = keras_file_con, model=model_con)
        # create linear pentalty for going below SP. Positive penalty is bad, otherwise zero pentalty.
        CV_penalty = -np.sum(np.minimum(predicted_CV_array-SP,0) * 10000)
        # minimize predicted fuel plus the constraint penalty
        return predicted_fuel_sum + CV_penalty

    return obj_function, 8*num_predictions

##    print('dMax =',dMax)
##        
##    print('beginning optimization')
##
##    lb_pso = lb
##    ub_pso = ub
##    if use_constraints:
##        print('using constraints')
##        lb_pso = np.zeros(array_shape)
##        ub_pso = np.ones(array_shape)
##
##    xopt, fopt = pso(obj_function, lb_pso, ub_pso,
##                     swarmsize=100, #161
##                     omega=-0.2089,
##                     phip=-0.0787,
##                     phig=3.7637,
##                     maxiter=10,#100, #1200
##                     debug=False)
##    
##    print('optimization complete')
##        
##    if use_constraints:
##        future_inputs = scale_dMax(xopt.reshape(matrix_shape))[duplicate_filer]
##    else:
##        future_inputs = xopt.reshape(matrix_shape)[duplicate_filer]
##    future_inputs = np.hstack([future_inputs, future_load])
##
##    predicted_fuel_array = make_prediction(past_output_fuel, past_inputs, future_inputs, keras_file = keras_file_obj, model=model_obj)
##    predicted_CV_array = make_prediction(past_output_CV, past_inputs, future_inputs, keras_file = keras_file_con, model=model_con)
##
##    return future_inputs, predicted_fuel_array, predicted_CV_array



if __name__ == "__main__":

    file = "machine_learning_data/run24/aaa_most_recent_results_202.p"
    plot_partial = True
    
    selected_minute = 100
    lookback_minutes = 12
    lookahead_minutes = 10

    get_objective_function(file, selected_minute, lookback_minutes, lookahead_minutes, "", "")

    print('unpickling values')
    load_values = pickle.load( open( file, "rb" ) )
    return_values_np, predicted_fuel_array, predicted_CV_array = load_values
    predicted_time_array = np.arange(1,len(predicted_fuel_array)+1) * 60 # convert to seconds

##    print('length:',len(return_values_np))
##    print('length:',len(predicted_time_array))
##    print('length:',len(predicted_fuel_array))
##    print('length:',len(predicted_CV_array))

    

    increment = 60
    start_index = selected_minute - lookback_minutes
    end_index = selected_minute + lookahead_minutes
    if plot_partial:
        plot_results(return_values_np[increment*start_index:increment*end_index:increment],
                     predicted_time_array[start_index:end_index],
                     predicted_fuel_array[start_index:end_index],
                     predicted_CV_array[start_index:end_index])
    else:
        plot_results(return_values_np,
                     predicted_time_array,
                     predicted_fuel_array,
                     predicted_CV_array)

    

    plt.show()

##    duplicate_filer = []
##    for i in range(int(num_predictions/decision_frequency)):
##        for j in range(decision_frequency):
##            duplicate_filer.append(i)

