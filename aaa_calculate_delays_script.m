
%% manually edit the chop points to ensure ascending values

% time_array = SOFA_timeseries.time(:);
% time_array(12)

start_index = 20;
end_index = 90;

%% initialize values and arrays (assuming that simulink has already put all the timeseries into the workspace)

time_noDelay = 3000;
time_array = SOFA_timeseries.time(start_index:end_index);

SOFA_array = SOFA_timeseries.Data(start_index:end_index);
SOFA_noDelay_array = SOFA_noDelay_timeseries.Data(start_index:end_index);

air_inj_array = air_inj_timeseries.Data(start_index:end_index);
air_inj_noDelay_array = air_inj_noDelay_timeseries.Data(start_index:end_index);

m_dot_steam_array = m_dot_steam_timeseries.Data(start_index:end_index);
m_dot_steam_noDelay_array = m_dot_steam_noDelay_timeseries.Data(start_index:end_index);

%% calculate time delay

SOFA_value = interp1(time_array,SOFA_noDelay_array,time_noDelay);
SOFA_time = interp1(SOFA_array,time_array,SOFA_value);

air_inj_value = interp1(time_array,air_inj_noDelay_array,time_noDelay);
air_inj_time = interp1(air_inj_array,time_array,air_inj_value);

m_dot_steam_value = interp1(time_array,m_dot_steam_noDelay_array,time_noDelay);
m_dot_steam_time = interp1(m_dot_steam_array,time_array,m_dot_steam_value);

m_dot_steam_delay = m_dot_steam_time - time_noDelay
SOFA_delay = SOFA_time - time_noDelay
air_inj_delay = air_inj_time - time_noDelay
