# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 07:41:32 2020

@author: Landen
"""

# import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
#import tensorflow as tf
#from tensorflow.keras.models import Sequential
#from tensorflow.keras.layers import Dense
#from tensorflow.keras.layers import LSTM
from tensorflow.keras.models import load_model

# if we need to use python in real-time
# https://www.mathworks.com/matlabcentral/answers/458589-can-i-interact-with-a-simulink-real-time-simulation-from-python
# https://www.mathworks.com/help/matlab/matlab-engine-for-python.html

def make_prediction2(past_outputs, past_inputs, future_inputs, keras_file = 'model_1'): 
#def make_prediction(X, keras_file = 'model_1'):
    load_model(keras_file)
    return None
    
    num_predictions = len(future_inputs)
    num_timesteps = len(past_outputs)
    num_inputs = len(past_inputs[0])
    num_features = 1 + num_predictions * num_inputs
    stacked_inputs = np.concatenate([past_inputs,future_inputs],axis=0)
    #print('stacked_inputs',np.shape(stacked_inputs))
    # build 3D input matrix
    X = np.zeros([1,num_timesteps,num_features])
    X[:,:,0] = past_outputs
    for timestep in range(num_timesteps):
        for prediction in range(num_predictions):
            start_index = 1+prediction*num_inputs
            end_index = 1+(prediction+1)*num_inputs
            #print('X[0,timestep,start_index:end_index]',np.shape(X[0,timestep,start_index:end_index]))
            #print('stacked_inputs[timestep+prediction,:]',np.shape(stacked_inputs[timestep+prediction,:]))
            X[0,timestep,start_index:end_index] = stacked_inputs[timestep+prediction,:]
    # return X
    
    # load model
    #model = tf.keras.models.load_model(keras_file)
    model = load_model(keras_file)
    
    # make predictions
    print('making predictions')
    #predictions = model.predict(X, verbose=0)
    return None

def make_prediction(past_outputs, past_inputs, future_inputs, keras_file = 'model_1'): 
#def make_prediction(X, keras_file = 'model_1'):
    
    num_predictions = len(future_inputs)
    num_timesteps = len(past_outputs)
    num_inputs = len(past_inputs[0])
    num_features = 1 + num_predictions * num_inputs
    stacked_inputs = np.concatenate([past_inputs,future_inputs],axis=0)
    #print('stacked_inputs',np.shape(stacked_inputs))
    # build 3D input matrix
    X = np.zeros([1,num_timesteps,num_features])
    X[:,:,0] = past_outputs
    for timestep in range(num_timesteps):
        for prediction in range(num_predictions):
            start_index = 1+prediction*num_inputs
            end_index = 1+(prediction+1)*num_inputs
            #print('X[0,timestep,start_index:end_index]',np.shape(X[0,timestep,start_index:end_index]))
            #print('stacked_inputs[timestep+prediction,:]',np.shape(stacked_inputs[timestep+prediction,:]))
            X[0,timestep,start_index:end_index] = stacked_inputs[timestep+prediction,:]
    # return X
    
    # load model
    #model = tf.keras.models.load_model(keras_file)
    model = load_model(keras_file)
    
    # make predictions
    print('making predictions')
    predictions = model.predict(X, verbose=0)
    return predictions


def test_prediction(case):
    case = case[0]
    X_test = [[[ 3.92006708e+02, -9.10565869e-02, 1.02012878e-01, 4.04247969e-01,
       5.27181632e-02, -3.35431838e-01,  1.89560443e-01,  3.92846908e-02,
       3.42781579e-01,  1.16112502e-02,  4.70150864e-02,  8.19191290e-01],
     [ 3.95780594e+02, -1.03759313e-01,  4.36753854e-02,  3.98662904e-01,
       1.11189648e-01, -3.63315592e-01,  1.31756663e-01,  1.61267225e-02,
       1.74330190e-01, -8.16352772e-02,  1.54007799e-02,  8.70569057e-01],
     [ 3.96063862e+02, -1.22220474e-01,  1.59622364e-02,  5.16748801e-01,
       7.89881198e-02, -5.07770021e-01,  1.77048710e-01, -7.09333317e-02,
       2.87913170e-01,  3.13743006e-02,  1.43130587e-02,  8.07770673e-01],
     [ 3.95126022e+02, -1.63500502e-01,  6.89121648e-02,  4.22090278e-01,
       2.94241770e-02, -4.67159933e-01,  8.83775603e-02,  1.00890519e-02,
       1.90141529e-01,  3.78330045e-02, -3.36276506e-02,  8.36314958e-01],
     [ 3.92205392e+02, -6.62659127e-02,  1.67351985e-01,  4.74663665e-01,
       2.21493475e-01, -4.53420158e-01,  1.54795186e-01, -4.20436315e-02,
       2.75701508e-01,  4.28518603e-02,  5.16605041e-02,  8.47628971e-01],
     [ 4.04112922e+02, -6.40293946e-02,  8.20011240e-02,  3.52823984e-01,
       1.81680098e-01, -4.73010997e-01,  1.09932913e-01,  2.54187546e-02,
       3.08985857e-01,  7.14268333e-03, -3.81054696e-02,  8.36317303e-01],
     [ 4.00564895e+02, -1.10980336e-01,  2.84955353e-02,  4.26441696e-01,
       1.21383427e-01, -4.32764859e-01,  1.24919234e-01,  2.36793618e-02,
       2.96878425e-01,  8.38074505e-02,  1.42579250e-02,  9.94709053e-01],
     [ 3.93859625e+02, -1.17783863e-01,  8.49760270e-02,  4.77821790e-01,
       1.49267516e-01, -4.30585638e-01,  1.56312646e-01, -1.97306207e-02,
       2.78559371e-01, -8.31366353e-02,  1.66130779e-02,  8.36643063e-01],
     [ 4.05291174e+02, -1.47701611e-01,  8.86636006e-02,  4.61838813e-01,
       1.79618712e-01, -4.28121738e-01,  1.84528428e-01,  1.10684502e-02,
       2.35915679e-01,  4.13779155e-02,  4.96993884e-02,  8.22124445e-01],
     [ 3.93654433e+02, -1.12541362e-01,  1.73673206e-01,  3.42349175e-01,
       1.09445478e-01, -4.89228464e-01,  1.48508924e-01,  5.03837983e-03,
       2.73524716e-01,  5.45745302e-02, -1.28529649e-01,  8.20558620e-01],
     [ 4.05599334e+02, -1.07864196e-01,  3.15448462e-01,  4.65719438e-01,
       1.61667506e-01, -4.17500673e-01,  1.71056218e-01, -4.23219297e-02,
       3.11123169e-01, -1.18264579e-01,  2.73828284e-02,  8.92891297e-01],
     [ 4.07705976e+02, -1.79895088e-01, -4.23917307e-02,  3.81828359e-01,
       9.39533291e-02, -3.53804332e-01,  2.00628250e-01, -1.35146008e-03,
       3.43872575e-01,  1.63902550e-02, -1.17813178e-01,  9.06187290e-01]]]


    #X_test2 = [[[ 3.92006708e+02, -9.10565869e-02, 1.02012878e-01, 4.04247969e-01,
    #   5.27181632e-02, -3.35431838e-01,  1.89560443e-01,  3.92846908e-02,
    #   3.42781579e-01,  1.16112502e-02,  4.70150864e-02,  8.19191290e-01]]]

    #X_test3 = [[[ 3.95780594e+02, -1.03759313e-01,  4.36753854e-02,  3.98662904e-01,
    #   1.11189648e-01, -3.63315592e-01,  1.31756663e-01,  1.61267225e-02,
    #   1.74330190e-01, -8.16352772e-02,  1.54007799e-02,  8.70569057e-01]]]

    #X_test4 = [[X_test2[0][0], X_test3[0][0]]]

    past_outputs_1 = np.array(X_test)[0,:,0]
    past_inputs_1 = np.array(X_test)[0,:-1,1:]
    future_inputs_1 = np.array(X_test)[0,-1:,1:]
    #print('past_outputs',np.shape(past_outputs))
    #print('past_inputs',np.shape(past_inputs))
    #print('future_inputs',np.shape(future_inputs))
    #print(np.shape(past_inputs),np.shape(future_inputs))

    #print(np.shape(X_test))
    #print(make_prediction(X_test))
    #print(np.shape(X_test2))
    #print(make_prediction(X_test2))
    #print(np.shape(X_test3))
    #print(make_prediction(X_test3))
    #print(np.shape(X_test4))
    #print(make_prediction(X_test4))

    #prediction = make_prediction(past_outputs,past_inputs,future_inputs)
    #print(prediction)

    X_test5 = [[[ 3.89266886e+02, -1.55592852e-01, 1.54154489e-01, 5.89774996e-01,
      1.31971943e-01, -3.98947614e-01, 1.23506016e-01, -9.40690157e-02,
     3.22973851e-01, 7.91673536e-02, 3.61128149e-02, 8.43225420e-01,
     -2.47383795e-01, 1.99485066e-01, 5.27697801e-01, 7.65775056e-02,
     -4.14371965e-01, 2.30129968e-01, 3.81878032e-02, 2.24592100e-01,
     8.34726499e-02, 9.53275251e-02, 7.42036403e-01],
     [ 3.95487602e+02, -2.47383795e-01, 1.99485066e-01, 5.27697801e-01,
     7.65775056e-02, -4.14371965e-01, 2.30129968e-01, 3.81878032e-02,
     2.24592100e-01, 8.34726499e-02, 9.53275251e-02, 7.42036403e-01,
     -9.68336323e-02, 2.41931793e-01, 5.56622895e-01, 7.75289655e-02,
     -3.97803812e-01, 1.20886375e-01, -3.04284394e-02, 1.86543007e-01,
     -4.53009203e-03, -1.44357277e-02, 8.47483345e-01],
     [ 3.93237737e+02, -9.68336323e-02, 2.41931793e-01, 5.56622895e-01,
     7.75289655e-02, -3.97803812e-01, 1.20886375e-01, -3.04284394e-02,
     1.86543007e-01, -4.53009203e-03, -1.44357277e-02, 8.47483345e-01,
     -6.67200346e-02, 1.58845162e-01, 4.21501344e-01, 1.30508347e-01,
     -3.30471504e-01, 2.13045263e-01, 1.90796014e-03, 3.31267637e-01,
     2.11760160e-02, -8.88587631e-03, 9.03371279e-01],
     [ 3.94531434e+02, -6.67200346e-02, 1.58845162e-01, 4.21501344e-01,
     1.30508347e-01, -3.30471504e-01, 2.13045263e-01, 1.90796014e-03,
     3.31267637e-01, 2.11760160e-02, -8.88587631e-03, 9.03371279e-01,
     -6.76931308e-02, 7.88677814e-02, 4.70470177e-01, 1.14788665e-01,
     -4.05475810e-01, 6.64731651e-02, 1.58937645e-03, 3.14045845e-01,
     -5.76545081e-03, 2.75317170e-04, 7.78892426e-01],
     [ 4.00935474e+02, -6.76931308e-02, 7.88677814e-02, 4.70470177e-01,
     1.14788665e-01, -4.05475810e-01, 6.64731651e-02, 1.58937645e-03,
     3.14045845e-01, -5.76545081e-03, 2.75317170e-04, 7.78892426e-01,
     -6.88598797e-02, 6.87513773e-02, 4.87857936e-01, 1.39076063e-01,
     -4.65711580e-01, 1.60753025e-01, -9.62836296e-02, 3.14133676e-01,
     6.27142725e-02, 3.64034469e-02, 8.26271760e-01],
     [ 3.87209526e+02, -6.88598797e-02, 6.87513773e-02, 4.87857936e-01,
     1.39076063e-01, -4.65711580e-01, 1.60753025e-01, -9.62836296e-02,
     3.14133676e-01, 6.27142725e-02, 3.64034469e-02, 8.26271760e-01,
     -1.14348891e-01, 1.50363294e-01, 3.76224355e-01, 1.89908072e-01,
     -4.26295581e-01, 1.11362163e-01, -3.51070800e-02, 2.07228257e-01,
     5.97222138e-04, -1.51389822e-01, 8.51050577e-01],
     [ 3.97290979e+02, -1.14348891e-01, 1.50363294e-01, 3.76224355e-01,
     1.89908072e-01, -4.26295581e-01, 1.11362163e-01, -3.51070800e-02,
     2.07228257e-01, 5.97222138e-04, -1.51389822e-01, 8.51050577e-01,
     -1.21856107e-01, 1.85957167e-01, 4.33907150e-01, 2.12167608e-01,
     -3.55148650e-01, 7.95747102e-03, 3.60528481e-02, 2.42194473e-01,
     1.31513869e-02, 2.15963872e-02, 9.08344763e-01],
     [ 3.99741520e+02, -1.21856107e-01, 1.85957167e-01, 4.33907150e-01,
     2.12167608e-01, -3.55148650e-01, 7.95747102e-03, 3.60528481e-02,
     2.42194473e-01, 1.31513869e-02, 2.15963872e-02, 9.08344763e-01,
     -8.13200559e-02, 7.46460021e-02, 4.52607180e-01, 1.28705922e-01,
     -4.07136233e-01, 1.37266670e-01, -6.60596921e-02, 3.18221791e-01,
     3.98031169e-02, 2.64913706e-02, 9.30639478e-01],
     [ 3.93976030e+02, -8.13200559e-02, 7.46460021e-02, 4.52607180e-01,
     1.28705922e-01, -4.07136233e-01, 1.37266670e-01, -6.60596921e-02,
     3.18221791e-01, 3.98031169e-02, 2.64913706e-02, 9.30639478e-01,
     -8.23238241e-02, 1.14021197e-01, 3.47365239e-01, 1.17600783e-01,
     -4.37657765e-01, 1.10919905e-01, -2.83117441e-02, 2.20826933e-01,
     2.66805706e-02, 2.79701866e-02, 8.68234667e-01],
     [ 3.94395950e+02, -8.23238241e-02, 1.14021197e-01, 3.47365239e-01,
     1.17600783e-01, -4.37657765e-01, 1.10919905e-01, -2.83117441e-02,
     2.20826933e-01, 2.66805706e-02, 2.79701866e-02, 8.68234667e-01,
     -5.71134510e-02, 9.93442548e-02, 4.50916355e-01, 2.10677608e-01,
     -4.50170804e-01, 1.59453266e-01, -4.95078265e-02, 2.60357687e-01,
     5.03814777e-02, -6.10513094e-03, 9.09466593e-01],
     [ 3.95282452e+02, -5.71134510e-02, 9.93442548e-02, 4.50916355e-01,
     2.10677608e-01, -4.50170804e-01, 1.59453266e-01, -4.95078265e-02,
     2.60357687e-01, 5.03814777e-02, -6.10513094e-03, 9.09466593e-01,
     -4.86668428e-02, 2.90236003e-02, 4.57467055e-01, 3.49740235e-02,
     -3.88136096e-01, 1.84226561e-01, 8.86123040e-03, 1.87530565e-01,
     3.16582359e-02, -3.10958688e-02, 9.02189135e-01],
     [ 4.05917569e+02, -4.86668428e-02, 2.90236003e-02, 4.57467055e-01,
     3.49740235e-02, -3.88136096e-01, 1.84226561e-01, 8.86123040e-03,
     1.87530565e-01, 3.16582359e-02, -3.10958688e-02, 9.02189135e-01,
     -7.25811010e-02, 1.56096995e-01, 4.94837802e-01, 9.63026435e-02,
     -3.94482898e-01, 1.33489758e-01, -1.14336498e-01, 2.56290307e-01,
     -7.00551851e-02, -2.65268427e-02, 9.57669519e-01]]]

    past_outputs_5 = np.array(X_test5)[0,:,0]
    past_inputs_5 = np.array(X_test5)[0,:-1,1:12]
    future_inputs_5 = np.reshape(np.array(X_test5)[0,-1:,1:], [2,11])
    #print('past_outputs_5',np.shape(past_outputs_5))
    #print('past_inputs_5',np.shape(past_inputs_5))
    #print('future_inputs_5',np.shape(future_inputs_5))

    #print(np.shape(X_test5))
    #prediction = make_prediction(X_test5, keras_file = 'model_2')
    if case==1:
        prediction = make_prediction(past_outputs_1,past_inputs_1,future_inputs_1, keras_file = 'proof_of_concept_scripts/model_1')
    elif case==2:
        prediction = make_prediction(past_outputs_5,past_inputs_5,future_inputs_5, keras_file = 'proof_of_concept_scripts/model_2')
    elif case==3:
        prediction = make_prediction2(past_outputs_5,past_inputs_5,future_inputs_5, keras_file = 'proof_of_concept_scripts/model_2')
    #print(prediction)
    return prediction

#print(test_prediction([1]))

#print(test_prediction([2]))

print(test_prediction([3]))
