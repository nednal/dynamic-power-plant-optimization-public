

data = [];
max_offset_second = 59;

increment = 600;
initial_sim_number = 720;

% update these for each new closed-loop run
beginning_run = 78; % skip runs before this and go through the end 

% 10 per row:
max_value = [52320, 19920, 8520, 11520, 10920, 55920, 10920, 11520, 12120, 55920,...%10
    11520, 10920, 48120, 11520, 11520, 54120, 11520, 37320, 55320, 12120,...%20
    11520, 54120, 11520, 11520, 59520, 58920, 64920,  1320,  1320,  1320,...%30
    1320    1320,  1320,  1320,  1320,  1320,  1320,  1320,  1320,  6720,...%40
    6720,   6720,  6120,  6720,  6720,  6720, 64320,  1320,  1320,  3120,...%50
    3120,   1320,  3120,  6720,  1320,  3120,  3120,  3120,  1320,  1320,...%60
    3120,   1320,  1320,  1320,  3120,  3120,  3120,  3120,  1320,  3120,...%70
    3120,   3120,  1320,  1320,  3120,  3120,  3120,  3120,  3120,  1320,...%80
    3120,   3120,  6720,  6720, 75120,  3120,  3720,  3120,  3120, 73920,...%90
    74520, 45120, 83520];

read_folder_name = 'simulation_results/';
write_folder_name = 'machine_learning_data/';


% hardcoded into the simulink model when we give inputs; don't change this
% value unless you change it there too.
injection_normalization_factor = 2000;

for sim_folder_index = beginning_run:length(max_value)
    sim_folder = strcat('run',num2str(sim_folder_index),'/');
    
    disp("folder: "+sim_folder+", timestamp "+string(0)+" of "+max_value(sim_folder_index));
    
    load(strcat(read_folder_name, sim_folder, 'closed_loop', string(0)), 'simOut')
    simulation = simOut;
    
    % intialize data structure
    time                = simulation.timeseries_CV.time;
    CV                  = simulation.timeseries_CV.Data;
    SP                  = simulation.timeseries_SP.Data;
    thermal_efficiency  = simulation.timeseries_thermal_efficiency.Data;
    total_power_MW      = simulation.timeseries_total_power_MW.Data;
    normalized_power    = simulation.timeseries_normalized_power.Data;
    power_SP            = simulation.timeseries_power_SP.Data;
    fuel_fraction       = simulation.timeseries_fuel_fraction.Data;
    n_inj_i             = simulation.timeseries_n_inj_i.Data;
    n_inj_noBias_i      = simulation.timeseries_n_inj_noBias_i.Data;
    
    % squeeze() removes all dimensions of length 1. CV has a bunch of length-1 dimensions
    CV = squeeze(CV);
    if length(SP)==1
        SP = SP .* ones(size(time));
    end
    
    % loop through all pieces and append values

    for simulation_number=initial_sim_number:increment:max_value(sim_folder_index)
        
        disp("folder: "+sim_folder+", timestamp "+string(simulation_number)+" of "+max_value(sim_folder_index));

        load(strcat(read_folder_name, sim_folder, 'closed_loop', string(simulation_number)), 'simOut')
        simulation = simOut;

        
        time_new                = simulation.timeseries_CV.time;
        CV_new                  = simulation.timeseries_CV.Data;
        SP_new                  = simulation.timeseries_SP.Data;
        thermal_efficiency_new  = simulation.timeseries_thermal_efficiency.Data;
        total_power_MW_new      = simulation.timeseries_total_power_MW.Data;
        normalized_power_new    = simulation.timeseries_normalized_power.Data;
        power_SP_new            = simulation.timeseries_power_SP.Data;
        fuel_fraction_new       = simulation.timeseries_fuel_fraction.Data;
        n_inj_i_new             = simulation.timeseries_n_inj_i.Data;
        n_inj_noBias_i_new      = simulation.timeseries_n_inj_noBias_i.Data;
        
        % squeeze() removes all dimensions of length 1. CV has a bunch of length-1 dimensions
        CV_new = squeeze(CV_new);
        if length(SP)==1
            SP_new = SP_new .* ones(size(time_new));
        end
        
        % need to drop the first term becuase it is a dublicate
        time                = [time;                time_new(2:end)];
        CV                  = [CV;                  CV_new(2:end)];
        SP                  = [SP;                  SP_new(2:end)];
        thermal_efficiency  = [thermal_efficiency;  thermal_efficiency_new(2:end)];
        total_power_MW      = [total_power_MW;      total_power_MW_new(2:end)];
        normalized_power    = [normalized_power;    normalized_power_new(2:end)];
        power_SP            = [power_SP;            power_SP_new(2:end)];
        fuel_fraction       = [fuel_fraction;       fuel_fraction_new(2:end)];
        n_inj_i             = [n_inj_i;             n_inj_i_new(2:end,:)];
        n_inj_noBias_i      = [n_inj_noBias_i;      n_inj_noBias_i_new(2:end,:)];
    
    end
    
    % calculate bias percent
    bias_percent = (n_inj_i - n_inj_noBias_i)/injection_normalization_factor;
    % remove the two bias columns that aren't MWs (always zero):
    bias_percent(:,10) = [];
    bias_percent(:,7) = [];

%     disp(string(simulation_number)+" of "+string(max_index))

    data = [time, bias_percent, normalized_power, power_SP, SP, CV, total_power_MW, fuel_fraction, thermal_efficiency];

    % interpolate and downsample per minute. Do so for each offest second to maximize total training data
    for start_second = 0:max_offset_second
        disp("folder: "+sim_folder+', writing offset second: ' +string(start_second))
        time_max = time(end);
        data_clean = [];
        for t =start_second:60:time_max
            row = [];
            for r=1:size(data,2)
                row = [row, interp1(time,data(:,r),t)];
            end
            data_clean = [data_clean; row];
        end
    %     writematrix(data_clean,strcat(folder_name,'data_test.csv'))
        Table = array2table(data_clean);
    %     Table.Properties.VariableNames(:) = {'time','bias_percent_1','bias_percent_2','bias_percent_3','bias_percent_4','bias_percent_5','bias_percent_6','bias_percent_7','bias_percent_8','bias_percent_9','bias_percent_10','load_fraction','total_power_MW','thermal_efficiency'};
        Table.Properties.VariableNames(:) = {'time','bias_percent_1','bias_percent_2','bias_percent_3','bias_percent_4','bias_percent_5','bias_percent_6','bias_percent_8','bias_percent_9','normalized_power', 'power_SP','SP','CV','total_power_MW','fuel_fraction','thermal_efficiency'};
        file_name = strcat(write_folder_name,'data_closed_loop_',num2str(sim_folder_index),'_',string(start_second),'.csv');
        writetable(Table,file_name);
    end
    
end

