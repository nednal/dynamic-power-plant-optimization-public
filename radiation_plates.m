function Q_plate_1_to_2 = radiation_plates(T_1, T_2, epsilon_1, epsilon_2 , C_s_black_body, A_plate)

epsilon_effective = 1 / (1/epsilon_1 + 1/epsilon_2 - 1);
Q_plate_1_to_2 = A_plate .* C_s_black_body .* (T_1.^4 - T_2.^4) .* epsilon_effective;