% clear all;
% close all;
% clc;

% https://marcepinc.com/blog/coal-combustion-process-and-its-products

% note: to extract timeseries data of the final timestep, use [variable name].Data(:,:,end)
% add it to a temp variable: temp = [variable name].Data(:,:,end)
% use the variable explorer to copy the data, then paste it into the cmd
% window to get it in an array format; this can be copied into simulink

% % here are the commands needed (state-space blocks at bottom):
% temp_T_i = T_i_timeseries.Data(:,end)
% temp_T_pipe_i = T_pipe_i_timeseries.Data(end,:)'
% temp_T_wall_i = T_wall_i_timeseries.Data(end,:)'
% temp_n_ij = n_ij_timeseries.Data(:,:,end)
% temp_PID = PID_i_timeseries.Data(end)
% 
% temp_T_SH_gas_i = T_gas_SH_timeseries.Data(end,:)'
% temp_T_SH_pipe_i = T_pipe_SH_timeseries.Data(end,:)'
% temp_T_SH_steam_i = T_steam_SH_timeseries.Data(end,:)'
% temp_T_RH_gas_i = T_gas_RH_timeseries.Data(end,:)'
% temp_T_RH_pipe_i = T_pipe_RH_timeseries.Data(end,:)'
% temp_T_RH_steam_i = T_steam_RH_timeseries.Data(end,:)'
% 
% temp_ss_PID = PID_i_timeseries.Data(end)
% temp_ss_air_inj = air_inj_timeseries.Data(end)
% temp_ss_steam = m_dot_steam_timeseries.Data(end)








%% unit conversion factors:

pa_per_psi = 6894.76;
m_per_ft = 0.3048;
m_per_in = 0.0254;
J_per_kJ = 1000;
pa_per_bar = 1e5;
decC_to_k = 273.15;

%% array ordering for species:

% species: [O2 N2 CO2 CO C H2 H20]
is_gas_j = [1; 1; 1; 1; 0; 0; 1]; % [O2 N2 CO2 CO C H2 H20] % let's just say hydrogen isn't a gas since it's trapped in the coal. Also, that way the reaction expands instead of contracts mol-wise
O_MW = 15.999;
N_MW = 14.0067;
C_MW = 12.0107;
H_MW = 1.00784;
MW_j = [O_MW*2; N_MW*2; C_MW+O_MW*2; C_MW+O_MW; C_MW; 2*H_MW; 2*H_MW+O_MW];

%% state variables and misc.:

R = 8.314; % J/mol*K,      fixed,      Ideal gas constant
Pr_waterwall_water = 0.9349; % highest liquid temperature/pressure at https://www.engineeringtoolbox.com/water-steam-Prandtl-number-d_2059.html
x_steam_out = 0.8; % quality of steam exiting the LP turbine

%% heat capacities:

% Note: I wanted to replace usage of this in the boiler code with the
% tabulated ones, however, the tabulated heat capacites are too high to
% achieve the desired performance. Engineering toolbox has a note that
% the temperature range we are in is heavily affected by disassociation and
% that the tabulated values don't account for that, so I'm using the room
% temperature values since they provide physically consistent results.
% TODO: DELETE THESE GAS c_p VALUES HERE AND IN THE BOILER FUCNTION; NO LONGER
% USED
gas_c_p_j = [29.4071; 29.1335; 37.1444; 28.5702; 10.68]; % J/mol*K,  Gas molar heat capacity array, source is in kg, so covert to mols: https://www.engineeringtoolbox.com/specific-heat-capacity-gases-d_159.html
gas_c_p_j_mass = [919; 1040; 844; 1020; 710]; % J/mol*K, same source as above, but note that this is the carbon source for both: https://www.engineeringtoolbox.com/specific-heat-solids-d_154.html
% c_p_boiler_wall = 3.4e5; % J/K,          fixed,      Total heat capacity of the one section of boiler wall (not per mass or area). Based on above dimensions (25x9x9) and 6 BTU/ft2-degF from this site about COMMERCIAL buildings http://www.gobrick.com/docs/default-source/read-research-documents/technicalnotes/4b-energy-code-compliance-of-brick-masonry-walls.pdf?sfvrsn=0                           
c_p_boiler_wall = 1.05e3; % J/K,          fixed,      Heat capacity of the boiler wall. https://www.engineeringtoolbox.com/specific-heat-solids-d_154.html
c_p_waterwall = 490; % J/kg K %https://www.engineeringtoolbox.com/specific-heat-metals-d_152.html
c_p_pipe_SH = c_p_waterwall; % ASSUMPTION
c_p_pipe_RH = c_p_waterwall; % ASSUMPTION

% global tabulated_Cp_O2_mass tabulated_Cp_N2_mass tabulated_Cp_CO2_mass tabulated_Cp_CO_mass single_value_Cp_C_mass;
% global tabulated_Cp_O2_mol tabulated_Cp_N2_mol tabulated_Cp_CO2_mol tabulated_Cp_CO_mol single_value_Cp_C_mol;

tabulated_Cp_O2_mass = readmatrix('gas_properties/Cp_O2.csv'); % https://www.engineeringtoolbox.com/oxygen-d_978.html
tabulated_Cp_O2_mass(:,2) = tabulated_Cp_O2_mass(:,2) * J_per_kJ;
tabulated_Cp_O2_mol = tabulated_Cp_O2_mass;
tabulated_Cp_O2_mol(:,2) = tabulated_Cp_O2_mol(:,2) / MW_j(1);

tabulated_Cp_N2_mass = readmatrix('gas_properties/Cp_N2.csv'); % https://www.engineeringtoolbox.com/nitrogen-d_977.html
tabulated_Cp_N2_mass(:,2) = tabulated_Cp_N2_mass(:,2) * J_per_kJ;
tabulated_Cp_N2_mol = tabulated_Cp_N2_mass;
tabulated_Cp_N2_mol(:,2) = tabulated_Cp_N2_mass(:,2) / MW_j(2);

tabulated_Cp_CO2_mass = readmatrix('gas_properties/Cp_CO2.csv'); % https://www.engineeringtoolbox.com/carbon-dioxide-d_974.html
tabulated_Cp_CO2_mass(:,2) = tabulated_Cp_CO2_mass(:,2) * J_per_kJ;
tabulated_Cp_CO2_mol = tabulated_Cp_CO2_mass;
tabulated_Cp_CO2_mol(:,2) = tabulated_Cp_CO2_mass(:,2) / MW_j(3);

tabulated_Cp_CO_mass = readmatrix('gas_properties/Cp_CO.csv') ; % https://www.engineeringtoolbox.com/carbon-monoxide-d_975.html
tabulated_Cp_CO_mass(:,2) = tabulated_Cp_CO_mass(:,2) * J_per_kJ;
tabulated_Cp_CO_mol = tabulated_Cp_CO_mass;
tabulated_Cp_CO_mol(:,2) = tabulated_Cp_CO_mass(:,2) / MW_j(4);

single_value_Cp_C_mass = 1.38 * J_per_kJ; % Coal, bituminous, https://www.engineeringtoolbox.com/specific-heat-solids-d_154.html
single_value_Cp_C_mol = single_value_Cp_C_mass / MW_j(5);

%% thermal conductivities:

% global single_value_tc_O2 single_value_tc_N2 single_value_tc_CO2 single_value_tc_CO;

single_value_tc_O2 = 48.1e-3; % highest tabulated temperature is at 600 K and 1 bar, https://www.engineersedge.com/heat_transfer/thermal-conductivity-gases.htm
single_value_tc_N2 = 44.0e-3; % highest tabulated temperature is at 600 K and 1 bar, https://www.engineersedge.com/heat_transfer/thermal-conductivity-gases.htm
single_value_tc_CO2 = 41.6e-3; % highest tabulated temperature is at 600 K and 1 bar, https://www.engineersedge.com/heat_transfer/thermal-conductivity-gases.htm
single_value_tc_CO = 45.7e-3; % highest tabulated temperature is at 600 K and 1 bar, https://www.engineersedge.com/heat_transfer/thermal-conductivity-gases.htm
k_water_waterwall = 677.03e-3; % W/m K, highest tabulated temperature is 99.6 degC and 1 bara at https://www.engineeringtoolbox.com/water-liquid-gas-thermal-conductivity-temperature-pressure-d_2012.html 

%% dynamic viscocities:

% global single_value_mu_O2 single_value_mu_N2 single_value_mu_CO2 single_value_mu_CO;

single_value_mu_O2 = 49.12e-6; % highest tabulated temperature is at 1000 K and 1 bar, https://www.engineeringtoolbox.com/oxygen-O2-dynamic-kinematic-viscosity-temperature-pressure-d_2081.html?vA=3000&degree=C#
single_value_mu_N2 = 41.54e-6; % highest tabulated temperature is at 1000 K and 1 bar, https://www.engineeringtoolbox.com/nitrogen-N2-dynamic-kinematic-viscosity-temperature-pressure-d_2067.html
single_value_mu_CO2 = 42.69e-6; % highest tabulated temperature is at 1050 K and 1 bar, https://www.engineeringtoolbox.com/carbon-dioxide-dynamic-kinematic-viscosity-temperature-pressure-d_2074.html
single_value_mu_CO = 3.86e-5; % highest tabulated temperature is at 600 degC and 1 bar, https://www.engineeringtoolbox.com/gases-absolute-dynamic-viscosity-d_1888.html
mu_water_waterwall = 0.0000603; % Pa*a, highest tabulated temperature is at 360 degC and 18.7 MPa, https://www.engineeringtoolbox.com/water-dynamic-kinematic-viscosity-d_596.html


%% enthalpy of vaporization:

% H_evap_water = 2256.4e3; %J/kg  https://www.engineeringtoolbox.com/water-properties-d_1573.html?vA=100&units=C#
H_evap_water = 866e3; %J/kg @ 626 k,  https://www.engineeringtoolbox.com/water-properties-d_1573.html?vA=100&units=C#

%% densities:

density_boiler_wall = 2.0e3; % kg/m^3,    density of the refractory brick (also called fire brick) https://www.engineeringtoolbox.com/density-solids-d_1265.html
density_waterwall = 7850; % kg/m^3 https://www.engineeringtoolbox.com/metal-alloys-densities-d_50.html
density_SH = density_waterwall;
density_RH = density_waterwall;
density_water_waterwall = 786.7; % kg/m^3, highest temp/pressure at https://www.engineeringtoolbox.com/water-density-specific-weight-d_595.html

%% Pressures:

P_atm               = 100745.06;   % Pa, atmospheric pressure in Huntington (https://www.wunderground.com/weather/us/ut/huntington/84528)
P_boiler            = P_atm;   % Pa, Pressure of the reactor
P_SH_inlet          = P_atm + (2425 + 45) * pa_per_psi;   % Pa, atmospheric pressure plus 2425 psig plus 45 psig pressure drop
P_HP_turbine_inlet  = P_atm + (2425) * pa_per_psi;   % Pa, atmospheric pressure plus 2425 psig
P_RH_inlet          = P_atm + (253) * pa_per_psi;   % Pa, atmospheric pressure plus 253 psig
P_MP_turbine_inlet  = P_atm + (253 - 15) * pa_per_psi;   % Pa, atmospheric pressure plus 253 psig minus 15 psig pressure drop
P_LP_turbine_inlet  = P_atm + 1/5 * (253 - 15) * pa_per_psi;


%% measurements:

% boiler:
thickness_boiler_wall = 2.5 * m_per_in; % m, 2.5" seems typical https://insulation.org/io/articles/package-boilers-save-energy-with-brick-refractory-insulation-and-lagging/
Area_boiler_wall = (51 + 52)*2 * (201 + 5/12 - 119) * m_per_ft^2 * 0.1; % 51' x 52' x 82'5", total area of the walls in one section, converted to m^2, multiplied by 0.1 because there are 10 segments
V = 51 * 52 * (201 + 5/12 - 119) * m_per_ft^3 * 0.1;                            % m^3,          fixed,      Total volume of combustion section of the reactor (converted from feet and multiplied by 0.1 because there are 10 segments), taken from Claus's presentation (pg 4, https://byu.app.box.com/s/xc0t3toiemiml53pae9vgjia0ktz4a3x)
boiler_perimeter = (51 + 52)*2 * m_per_ft; % 51' x 52', converted to m

% waterwall:
outer_diameter_waterwall = 2.375 * m_per_in; % m, 2.375" converted to meters
thickness_waterwall = 0.220 * m_per_in; % m, 0.220" converted to meters
height_waterwall = (82 + 5/12) * m_per_ft * 0.1; % m, 82'5" converted to meters and multiplied by 0.1 because there are 10 segments

% super heater (SH):
n_pipes_SH = 12; % number of tubes in the tubebank of the superheater (4-6 = specs)
outer_diameter_SH = outer_diameter_waterwall; % ASSUMPTION
thickness_SH = thickness_waterwall; % ASSUMPTION
num_bends_SH = 8; % number of  bends in the tubebank of the superheater (5-10? = specs)
height_SH = 42.8828 * m_per_ft; % height of superheater, which is the length of a single switchback in the superheater pipe, 42.8828' calculated from "Hunter 3 Testing Summary" power point (pg 4 of https://byu.app.box.com/s/xc0t3toiemiml53pae9vgjia0ktz4a3x)
length_SH = 51 * m_per_ft; % length of super heater 
width_SH = 52 * m_per_ft; % width of super heater 
C_1_SH = 0.90; % (0.90 = ARBITRARY)
C_2_SH = 0.95; % (0.95 = ARBITRARY)

% reheater (RH):
n_pipes_RH = 8; % number of tubes in the tubebank of the superheater (9-33? = specs)
outer_diameter_RH = outer_diameter_waterwall; % ASSUMPTION
thickness_RH = thickness_waterwall; % ASSUMPTION
num_bends_RH = 4; % number of  bends in the tubebank of the superheater (10 = specs)
height_RH = 42.8828 * m_per_ft; % height of superheater, which is the length of a single switchback in the superheater pipe, 42.8828' calculated from "Hunter 3 Testing Summary" power point (pg 4 of https://byu.app.box.com/s/xc0t3toiemiml53pae9vgjia0ktz4a3x)
length_RH = 51 * m_per_ft; % length of super heater 
width_RH = 52 * m_per_ft; % width of super heater 
C_1_RH = 0.90; % (0.90 = ARBITRARY)
C_2_RH = 0.95; % (0.95 = ARBITRARY)

% unused:
% UA_wall_in  = 5e3; %5e4                     % W/delta_K,    fixed,      Overall heat transfer from the inside of the boiler to the boiler wall
% UA_wall_out = 5e1;                          % W/delta_K,    fixed,      Overall heat transfer from the boiler wall to the outside of the boiler, I simply made this 1% of the inner value
% T_ambient   = 298;                          % K,            fixed,      Temperature of ambient air around boiler

%% radiation parameters:

epsilon_inf         = (0.55+0.7)/2; % between 0.55 and 0.7 for hard coal, lignite, and peat
k_flame_parameter   = 0.75; % 0.75 = luminous flame, 0.5= blue flame
C_s_black_body      = 5.67e-8; % W/m^2*K^4
s_layer_thickness   = 0.49; % itterated on this to get the right value

% "Beer's law" is the formula we are using, and the book below recomended
% 3.6 * (V/A)
% https://books.google.com/books?id=b2238B-AsqcC&pg=PA712&lpg=PA712&dq=%22layer+thickness%22++of+gas+radiant+heat&source=bl&ots=-EU17cfor5&sig=ACfU3U0ANlAtt1f9uAQV1v2dNvEPU6oNuw&hl=en&sa=X&ved=2ahUKEwjh5Yusx47oAhU9GDQIHZeYAv4Q6AEwA3oECAoQAQ#v=onepage&q=%22layer%20thickness%22%20%20of%20gas%20radiant%20heat&f=false
epsilon_waterwall   = 0.6; % about the right value for steel
epsilon_boiler_wall = 0.38; %refractory brick, https://www.coleparmer.com/tech-article/emissivity-of-specific-materials#anchor7
epsilon_water       = 0.996; % https://www.engineeringtoolbox.com/radiation-heat-emissivity-d_432.html
epsilon_SH          = 0.6; % about the right value for steel
epsilon_RH          = 0.6; % about the right value for steel

% however, we do need that surface area for the transfer from the pipe to
% the steam
% https://www.metroboilertube.com/boiler-tube-stock.html 
% http://www.atc-mechanical.com/tube-pipe-101/tube-pipe-size-overview/


%% efficiencies:

% boiler_heat_transfer_efficiency = 0.75; % parameter from [0,1] that determines how much heat transfer from radiation is lost to the environment
heat_loss_constant_term_boiler = 15.98 * H_evap_water;
parasitic_load = 0.1; % fraction of total energy that is parasitic load

%% reactions:

% rxn_0 (direct reaction, used for testing before implementing the 2-step):
% C + O2 => CO2 (ues the reaction rate for CO2 since it's rate limitting)
% k = A * exp( - E/(R T) ), A = 2.5e12, E/R = 24000
% ideally, temperatures stay within 1500-3000 deg K, mostly on the lower end
% rxn_0 stoichiometry:
rxn_0_stoic_j           = [-1; 0; 1; 0; -1];                % [O2 N2 CO2 CO C]
rxn_0_order_j           = [1; 0; 0; 0; 1];                  % [O2 N2 CO2 CO C]
rxn_0_H                 = -393.5e3;                         % J/mol     source: https://opentextbc.ca/chemistry/chapter/5-3-enthalpy/
rxn_0_throttle_factor   = 0.000174 * 1e6;                   % ratio of the carbon that is effectively available to combust at a given moment
rxn_0_k_0               = 2.5e12 * rxn_0_throttle_factor;   % m^3/mol*s,    fixed,      Reaction rate preexponential term of rnx_1
rxn_0_E_a               = 24000*R;                          % J/mol,        fixed,      Reaction activation energy of rnx_1

energy_density_correction = 1.1097; % correction factor to make it coal energy density equal to the actual coal

% rxn_1:
% C + 1/2 O2 => CO
% k = A * T^B * exp( - E/(R T) ), A = ???, E/R = ???
% ideally, temperatures stay within 1500-3000 deg K, mostly on the lower end
% rxn_1 stoichiometry:
rxn_1_stoic_j           = [-0.5; 0; 0; 1; -1; 0; 0];   % [O2 N2 CO2 CO C H2 H20]
rxn_1_order_j           = [1; 0; 0; 0; 1; 0; 0];    % [O2 N2 CO2 CO C H2 H20]
rxn_1_H                 = -110.5e3 * energy_density_correction;     % J/mol     source: https://opentextbc.ca/chemistry/chapter/5-3-enthalpy/
rxn_1_throttle_factor   = 0.000174e-4;     % ratio of the carbon that is effectively available to combust at a given moment
rxn_1_k_0               = 6.3e11 * rxn_1_throttle_factor;       % m^3/mol*s,  Reaction rate preexponential term of rnx_1
rxn_1_E_a               = 0*R;      % J/mol,      Reaction activation energy of rnx_1
rxn_1_exponent          = 0.5;            % exponent of T in ahrenius equation


% rxn_2:
% CO + 1/2 O2 => CO2
% k = A * T^B * exp( - E/(R T) ), A = 2.5e12, E/R = 24000
% ideally, temperatures stay within 1500-3000 deg K, mostly on the lower end
% rxn_2 stoichiometry:
rxn_2_stoic_j           = [-0.5; 0; 1; -1; 0; 0; 0];   % [O2 N2 CO2 CO C H2 H20]
rxn_2_order_j           = [1; 0; 0; 1; 0; 0; 0];    % [O2 N2 CO2 CO C H2 H20]
rxn_2_H                 = -283.0e3 * energy_density_correction;     % J/mol     source: https://opentextbc.ca/chemistry/chapter/5-3-enthalpy/
% rxn_2_throttle_factor   = 0.000174e-2;     % ratio of oxygen radicals that are effectively available to combust at a given moment
rxn_2_k_0               = 2.5e12;           % m^3/mol*s,  Reaction rate preexponential term of rnx_1
rxn_2_E_a               = 24000*R;              % J/mol,      Reaction activation energy of rnx_1
rxn_2_exponent          = 0;              % exponent of T in ahrenius equation


% % rxn_3:
% % H2 + O2 => HO2
% % k = A * T^B * exp( - E/(R T) ), A = ?, E/R = ?
% % rxn_3 stoichiometry:
% rxn_3_gas_stoic         = [-0.5; 0; 1; -1; 0; 0];   % [O2 N2 CO2 CO H2 H20]
% rxn_3_solid_stoic       = [0];          % [C]
% rxn_3_gas_order         = [1; 0; 0; 1; 0; 0];    % [O2 N2 CO2 CO H2 H20]
% rxn_3_solid_order       = [0];          % [C]
% H_rxn_3                 = -285.8;     % J/mol     
% rxn_3_throttle_factor   = 1;     % ratio of oxygen radicals that are effectively available to combust at a given moment
% k_0_rxn_3               = 0;           % m^3/mol*s,  Reaction rate preexponential term of rnx_1
% E_a_rxn_3               = 0*R;              % J/mol,      Reaction activation energy of rnx_1
% rxn_exponent_3          = 0;              % exponent of T in ahrenius equation


% rxn_3:
% H2 + O2 => HO2
% k = A * T^B * exp( - E/(R T) ), A = ?, E/R = ? Just use the above
% reaction
% rxn_3 stoichiometry:
rxn_3_stoic_j           = [-0.5; 0; 0; 0; 0; -1; 1];   % [O2 N2 CO2 CO C H2 H20]
rxn_3_order_j           = [1; 0; 0; 0; 0; 1; 0];    % [O2 N2 CO2 CO C H2 H20]
rxn_3_H                 = rxn_1_H+rxn_2_H;     % J/mol     using the same value as the above reaction
rxn_3_throttle_factor   = rxn_1_throttle_factor;     % ratio of the hydrogen that is effectively available to combust at a given moment
rxn_3_k_0               = rxn_1_k_0;       % m^3/mol*s,  Reaction rate preexponential term of rnx_1
rxn_3_E_a               = rxn_1_E_a;      % J/mol,      Reaction activation energy of rnx_1
rxn_3_exponent          = rxn_1_exponent;            % exponent of T in ahrenius equation



% math and sources for throttle_factor:
% FDA says largest dust to create explosion hazard is 420 microns, so let's assume an even 400 microns for coal dust size (https://hughesenv.com/what-is-the-size-of-dust/)
% Density of carbon is 2.266 g/cm^3 (https://www.aqua-calc.com/page/density-table/substance/carbon-coma-and-blank-solid)
% That means a spherical carbon ball diameter 400 microns (400e-6 m) has a
% volume of 0.000034 cm^3
% this means is has a mass of 0.000076 gm
% molar weight of carbon is 12.0107 gm/mol
% this means the ball has 0.000006 mol or 3.81e18 atoms
% each atom gets 8.799 ang^3 volume
% apply a perfect spherical packing factor of 0.74, gives each atom 6.51 Ang^3
% back calculating the diameter of the spherical space occupied by the atom gives 2.32 Ang
% lastly, take a volume ratio of the outer shell that has a thickness of the calculated atom diameter and the total particle volume
% the ratio is 0.000174



%% calculations:

% waterwall measurements:
n_pipes_waterwall = floor( (boiler_perimeter - 2*outer_diameter_waterwall) / outer_diameter_waterwall ); % number of pipes in waterwall
outer_radius_waterwall = outer_diameter_waterwall/2;
inner_radius_waterwall = outer_radius_waterwall - thickness_waterwall;
inner_diameter_waterwall = inner_radius_waterwall / 2;
inner_area_waterwall = 2*pi*inner_radius_waterwall * height_waterwall;
SA_inner_total_waterwall = inner_area_waterwall * n_pipes_waterwall; % total internal surface area of pipe in a single section
mass_total_waterwall = pi*(outer_radius_waterwall^2 - inner_radius_waterwall^2) * height_waterwall * n_pipes_waterwall * density_waterwall; % total mass of the pipes in one section
mass_total_boiler_wall = Area_boiler_wall * thickness_boiler_wall * density_boiler_wall; % total mass of a single section of the boiler wall

% superheater measurements:
inner_diameter_SH = outer_diameter_SH - 2 * thickness_SH;
inner_radius_SH = inner_diameter_SH / 2;
outer_radius_SH = outer_diameter_SH / 2;
surface_area_inner_single_pipelength_SH = pi * inner_diameter_SH * height_SH;
surface_area_outer_single_pipelength_SH = pi * outer_diameter_SH * height_SH;
mass_pipe_SH = pi*(outer_radius_SH^2 - inner_radius_SH^2) * height_SH * n_pipes_SH * density_SH; % total mass of the pipes in one section, neglecting the bends in the pipes
cross_sectional_area_pipe_SH = pi * inner_radius_SH^2;
radiation_cross_sectional_area_SH = length_SH * width_SH; % cross sectional area that radiation passes through to get to the superheater
total_volume_SH = height_SH * length_SH * width_SH; % total volume taken up by the super heater area
internal_pipe_volume_SH_i = pi * inner_diameter_SH * height_SH * n_pipes_SH; % internal volume of pipes for a single section of the superheater (one bend)
external_pipe_volume_SH_i = pi * outer_diameter_SH * height_SH * n_pipes_SH; % internal volume of pipes for a single section of the superheater (one bend)
gas_volume_SH_i = total_volume_SH - external_pipe_volume_SH_i; % total volume of the gas occupied around the superheater in a single section (one bend)

% reheater measurements:
inner_diameter_RH = outer_diameter_RH - 2 * thickness_RH;
inner_radius_RH = inner_diameter_RH / 2;
outer_radius_RH = outer_diameter_RH / 2;
surface_area_inner_single_pipelength_RH = pi * inner_diameter_RH * height_RH;
surface_area_outer_single_pipelength_RH = pi * outer_diameter_RH * height_RH;
mass_pipe_RH = pi*(outer_radius_RH^2 - inner_radius_RH^2) * height_RH * n_pipes_RH * density_RH; % total mass of the pipes in one section, neglecting the bends in the pipes
cross_sectional_area_pipe_RH = pi * inner_radius_RH^2;
radiation_cross_sectional_area_RH = length_RH * width_RH; % cross sectional area that radiation passes through to get to the superheater
total_volume_RH = height_RH * length_RH * width_RH; % total volume taken up by the super heater area
internal_pipe_volume_RH_i = pi * inner_diameter_RH * height_RH * n_pipes_RH; % internal volume of pipes for a single section of the superheater (one bend)
external_pipe_volume_RH_i = pi * outer_diameter_RH * height_RH * n_pipes_RH; % internal volume of pipes for a single section of the superheater (one bend)
gas_volume_RH_i = total_volume_RH - external_pipe_volume_RH_i; % total volume of the gas occupied around the superheater in a single section (one bend)


% pressure values:
P_SH_avg = (P_SH_inlet + P_HP_turbine_inlet)/2; % average pressure in the superheater
P_RH_avg = (P_RH_inlet + P_MP_turbine_inlet)/2; % average pressure in the superheater


% temperature values:
T_water_waterwall = steam_property('Tsat_p',P_SH_inlet);
T_water_condenser = steam_property('Tsat_p',P_atm);

% c_p_feedwater
c_p_feedwater = steam_property('Cp_pt',P_atm, (T_water_waterwall+T_water_condenser)/2 );

% velocity values:
v_water_waterwall = 165.8 / density_water_waterwall / n_pipes_waterwall / (pi * inner_radius_waterwall ^ 2); % 165.8 kg_steam/s = maximum flowrate of water

% total energy density of coal (add both reactions)
energy_density = -rxn_1_H - rxn_2_H; % J/mol






