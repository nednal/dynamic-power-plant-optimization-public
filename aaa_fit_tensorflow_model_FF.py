# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 07:41:32 2020

@author: Landen
"""

print('starting tensorflow fitting script')

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM
import random
random.seed(12345)

# important notes: Tensorflow doesn't work with 32-bit python, which is the default
# not sure if Tensorflow works with Anaconda nowadays; didn't try
# there's a weird thing with numpy about to lose backward comparibility in October 2020, so don't update numpy, just let Tensorflow install an older version

# instructions for virtual environments: https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/

#     ***********************************************************
#     *    start the virtual environment and then start idle    *
#     *         C:\Users\Landen\python38\Scripts\activate       *
#     *         python -m idlelib                               *
#     ***********************************************************

plotting = True
#plot_range = None
plot_range = [250,500]
prediction_type_array = ['fuel','CV']
from_file = True

keras_file_array = ['model_fuel_ff_v7', 'model_CV_ff_v7']
num_csv_files = 64
num_closed_loop_csv_files = 93
num_offest_seconds = 1
shuffle_data = True
send_text = False


ML_folder = 'machine_learning_data/'
predict_column_units = ''

plot_name_dic = {'fuel_fraction':'Fuel Fraction','CV':'Excess O$_2$'}

for i, prediction_type in enumerate(prediction_type_array):
    print('\nRUNNING TENSORFLOW FIT WITH INDEX:',i,'\n')
    keras_file = keras_file_array[i]

    data_df_list = []
    for i in range(1,num_csv_files+1):
        try:
            print('reading dataframes index:',i)
            for j in range(num_offest_seconds): # read in each of the offset seconds
                data_df_list.append(pd.read_csv(ML_folder +'data_'+str(i)+'_'+str(j)+'.csv'))
        except:
            print('skipped number',i)

    
    # read in closed loop info
    for i in range(1,num_closed_loop_csv_files+1):
        try:
            print('reading closed-loop dataframes index:',i)
            for j in range(num_offest_seconds): # read in each of the offset seconds
                data_df_list.append(pd.read_csv(ML_folder +'data_closed_loop_'+str(i)+'_'+str(j)+'.csv'))
        except:
            print('skipped number',i)
    

    # shuffle the dataframes to break up similiear ones from testing/vallidaiton sets
    # I'm worried this has the effect of some of the testing data enter the training pool
    if shuffle_data:
        print('\nsuffling dataframe list')
        random.shuffle(data_df_list)
    
    if prediction_type == 'fuel':
        prediction_headers = ['fuel_fraction']
        headers_to_drop = ['time', 'thermal_efficiency', 'SP', 'CV',
                           'total_power_MW', 'normalized_power']
        loss_weights = [100]
        #LSTM_layers = 1
        hidden_layers = 1
        
    elif prediction_type == 'CV':
        prediction_headers = ['CV']
        headers_to_drop = ['time', 'thermal_efficiency', 'SP', 'fuel_fraction',
                           'total_power_MW', 'normalized_power']
        loss_weights = [10000]
        #LSTM_layers = 2
        hidden_layers = 2
        

    #num_predictions = 10 # starting with the next timestep
    #num_predicted_values = len(prediction_headers)
    #LSTM_size = 57 #57 is good for only kW prediction, and for only Excess O2 prediction
    #sample_size = 12 # Jake's paper recomends 12
    #sample_increment = 1
    num_predicted_values = 1
    epochs = 200
    use_early_stopping = True
    patience = 5
    batch_size = 18
    hidden_layer_size = 12

    samples_array = None
    data = []

    print('\nworking on formatting the data for FF-NN')
    for index, data_df in enumerate(data_df_list):
        if int(index/len(data_df_list)*10) != int((index-1)/len(data_df_list)*10):
            print('percent complete:',int(index/len(data_df_list)*100))
            
        #if len(data_df) < sample_size + num_predictions + sample_increment:
        #    continue
        
        # drop unecessary headers
        data_df = data_df.drop(headers_to_drop, axis=1)
        
        # add artificial noise
        for header in list(data_df):
            if header == 'total_power_MW':
                noise = np.random.normal(0, 5, len(data_df[header]))
            elif header == 'CV':
                noise = np.random.normal(0, 0.001, len(data_df[header]))
            elif header == 'fuel_fraction':
                noise = np.random.normal(0, 0.001, len(data_df[header]))
            else:
                noise = np.random.normal(0, 0.001, len(data_df[header]))
            data_df[header] += noise
        

        df_list = []
        #df_list.append(data_df[prediction_headers]) # current output
        #df_list.append(data_df[prediction_headers].shift(1)) # previous output COMMENTING THIS OUT TO TEST
        df_list.append(data_df.drop(prediction_headers, axis=1)) # current inputs (bias then load)
        #for i in range(0,num_predictions):
        #    df_list.append(data_df.shift(-(i+1)).drop(prediction_headers, axis=1)) # future inputs
        df_list.append(data_df[prediction_headers]) # current output
        # i in range(0,num_predictions):
        #    df_list.append(data_df[prediction_headers].shift(-(i+1))) # future outputs
        data_df = pd.concat(df_list, axis=1)
        
        data_df.dropna(inplace=True) # easy way to just get rid of all the groups at the ends with missing values
        if index==0:
            data = data_df.to_numpy()
        else:
            data = np.vstack([data,data_df.to_numpy()])
        

    print('data size:',data.shape)
    X = data[:,:-1]
    y = data[:,-1]

    # split train/validation/test:
    n = X.shape[0]
    train_cutoff = 0.7
    validation_cutoff = 0.9
    X_train = X[0:int(n*train_cutoff)]
    X_val = X[int(n*train_cutoff):int(n*validation_cutoff)]
    X_test = X[int(n*validation_cutoff):]

    y_train = y[0:int(n*train_cutoff)]
    y_val = y[int(n*train_cutoff):int(n*validation_cutoff)]
    y_test = y[int(n*validation_cutoff):]

    validation_data=(X_val, y_val)

    print('X shape:',X.shape)
    print('y shape:',y.shape)
    raise
    input_shape = X.shape[1:]

    if not from_file:
        # 1. define network
        model = Sequential()
        model.add(Dense(hidden_layer_size, input_shape=(X.shape[1],), activation='relu'))
        #model.add(LSTM(LSTM_size, input_shape=input_shape, return_sequences=True))
        for i in range(0,hidden_layers):
            model.add(Dense(hidden_layer_size, activation='relu'))
            #model.add(LSTM(LSTM_size, return_sequences=True))
        model.add(Dense(num_predicted_values))

        # 2. compile network
        model.compile(optimizer='adam', loss='mean_squared_error', loss_weights=loss_weights)

        # 3. fit network
        print('fitting network')

        callbacks=None
        if use_early_stopping:
            callbacks = [tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=patience)]
        history = model.fit(X_train, y_train, epochs=epochs, batch_size=batch_size, verbose=2, validation_data=validation_data, callbacks=callbacks)
        
        model.save(ML_folder + keras_file)
    else:
        model = tf.keras.models.load_model(ML_folder + keras_file)
        
    # 4. evaluate network
    print('evaluating network')
    loss_train = model.evaluate(X_train, y_train, verbose=0) # gives an array of loss and accuracy (accuracy is basically meaningless for regression)
    print('train loss:',loss_train)
    loss_test = model.evaluate(X_test, y_test, verbose=0) # gives an array of loss and accuracy (accuracy is basically meaningless for regression)
    print('test loss',loss_test)
    print('test RMSE',np.sqrt(loss_test))

    # 5. make predictions
    print('making predictions')

    predictions = model.predict(X_test, verbose=0)

    # plot multiple predictions
    n_test = len(y_test)
    increment = min(3*num_predicted_values, num_predicted_values)
    y_axis_labels = prediction_headers
    print('y_test shape:',np.shape(y_test))
    print('predictions shape:',np.shape(predictions))

    for i in np.linspace(0,num_predicted_values-1,increment).astype(int):
        plt.figure()
        if plot_range is None:
            #plt.plot(range(n_test),y_test[:,0,i],'b.-',label='actual')
            plt.plot(range(n_test),y_test,'b.-',label='actual',markersize=2, linewidth=1)
            #plt.plot(range(n_test),predictions[:,-1,i],'go',label='predicted')
            plt.plot(range(n_test),predictions,'go',label='predicted',markersize=2)
        else:
            plt.plot(range(n_test)[:plot_range[1]-plot_range[0]],y_test[plot_range[0]:plot_range[1]],'b.-',label='actual',markersize=2, linewidth=1)
            plt.plot(range(n_test)[:plot_range[1]-plot_range[0]],predictions[plot_range[0]:plot_range[1]],'go',label='predicted',markersize=2)
        plt.title('Prediction '+str(int(i/num_predicted_values+1))+' minute(s) ahead')
        plt.xlabel('Time (min)')
        plt.ylabel(plot_name_dic[y_axis_labels[i]])
        plt.legend()

        plt.savefig(ML_folder + 'forecast_'+str(i+1)+'_minutes_ahead_'+y_axis_labels[i]+keras_file+'.png', transparent=True)
        plt.savefig(ML_folder + 'forecast_'+str(i+1)+'_minutes_ahead_'+y_axis_labels[i]+keras_file+'.eps', format='eps',bbox_inches="tight")

        
    #parity_labels_array = []
    #parity_predictions_aray = []
    parity_labels = y_test
    parity_predictions = predictions[:,0]
    #for j in range(num_predicted_values):
    #    parity_labels = []
    #    parity_predictions = []
    #    for i in range(j,num_predicted_values,num_predicted_values):
    #        parity_labels = np.hstack([parity_labels, y_test[0,i]])
    #        parity_predictions = np.hstack([parity_predictions, predictions[:,i]])
    #    parity_labels_array.append(parity_labels)
    #    parity_predictions_aray.append(parity_predictions)

    def plot_parity_historgram(test_labels, test_predictions, title_add_on=''):
        
        plt.rcParams.update({'font.size': 14})
        plt.rc('axes', titlesize=14)
        
        plt.figure(figsize=(12.5,4.8)) #6.4,4.8 is default
        
        plt.subplot(121)
        plt.scatter(test_labels, test_predictions, s=5, linewidths = 0.1, edgecolors = 'darkblue',label='data')
        plt.xlabel('True values '+predict_column_units)
        plt.ylabel('Predicted values '+predict_column_units)
        plt.axis('equal')
        plt.axis('square')
        plt.xlim([0,plt.xlim()[1]])
        plt.ylim([0,plt.ylim()[1]])
        plt.plot([0,max(test_labels)],[0,max(test_labels)],'-g',label='parity')
        plt.title('Parity Plot of Testing Data ' + title_add_on)
        plt.legend()
        
        plt.subplot(122)
        
        error = test_predictions - test_labels
        plt.hist(error, bins = 25)
        plt.xlabel("Prediction error "+predict_column_units)
        plt.ylabel("Count")
        plt.title('Histogram Testing Data Error ' + title_add_on)
        
        plt.savefig(ML_folder + 'parity_histogram_'+keras_file+'.png', transparent=True)
        plt.savefig(ML_folder + 'parity_histogram_'+keras_file+'.eps', format='eps',bbox_inches="tight")
        
    if plotting:
        plot_parity_historgram(parity_labels, parity_predictions, plot_name_dic[prediction_headers[0]])
        #for i in range(num_predicted_values):
        #    plot_parity_historgram(parity_labels_array[i], parity_predictions_aray[i],prediction_headers[i])
        from send_text_message import send_message
        send_message("finished training nueral network(s).")
        plt.show()

if not plotting and send_text:
    from send_text_message import send_message
    send_message("finished training nueral network(s).")
print('complete')
