

% simulation_number = 1;

data = [];

max_index = 64;
% max_index = 1;
max_offset_second = 59;
offset_second = 0;

read_folder_name = 'simulation_results/';
write_folder_name = 'machine_learning_data/';

% hardcoded into the simulink model when we give inputs; don't change this
% value unless you change it there too.
injection_normalization_factor = 2000;

for i=1:max_index
    simulation_number = i;
    
    try
        load(strcat(read_folder_name,string(simulation_number)), 'simulation')

%         time                = simulation.tout;
        time                = simulation.timeseries_CV.time;
        CV                  = simulation.timeseries_CV.Data;
        SP                  = simulation.timeseries_SP.Data;
        thermal_efficiency  = simulation.timeseries_thermal_efficiency.Data;
        total_power_MW      = simulation.timeseries_total_power_MW.Data;
        normalized_power    = simulation.timeseries_normalized_power.Data;
        power_SP            = simulation.timeseries_power_SP.Data;
        fuel_fraction       = simulation.timeseries_fuel_fraction.Data;
        %load_fraction       = simulation.timeseries_load_fraction.Data;
        n_inj_i             = simulation.timeseries_n_inj_i.Data;
        n_inj_noBias_i      = simulation.timeseries_n_inj_noBias_i.Data;


        % squeeze() removes all dimensions of length 1. CV has a bunch of length-1 dimensions
        CV = squeeze(CV);
        if length(SP)==1
            SP = SP .* ones(size(time));
        end

        % calculate bias percent
        bias_percent = (n_inj_i - n_inj_noBias_i)/injection_normalization_factor;
        % remove the two bias columns that aren't MWs (always zero):
        bias_percent(:,10) = [];
        bias_percent(:,7) = [];

        disp(string(simulation_number)+" of "+string(max_index))

        data = [time, bias_percent, normalized_power, power_SP, SP, CV, total_power_MW, fuel_fraction, thermal_efficiency];

        % interpolate and downsample per minute. Do so for each offest second to maximize total training data
        for start_second = 0:max_offset_second
            time_max = time(end);
            data_clean = [];
            for t =start_second:60:time_max
                row = [];
                for r=1:size(data,2)
                    row = [row, interp1(time,data(:,r),t)];
                end
                data_clean = [data_clean; row];
            end
        %     writematrix(data_clean,strcat(folder_name,'data_test.csv'))
            Table = array2table(data_clean);
        %     Table.Properties.VariableNames(:) = {'time','bias_percent_1','bias_percent_2','bias_percent_3','bias_percent_4','bias_percent_5','bias_percent_6','bias_percent_7','bias_percent_8','bias_percent_9','bias_percent_10','load_fraction','total_power_MW','thermal_efficiency'};
            Table.Properties.VariableNames(:) = {'time','bias_percent_1','bias_percent_2','bias_percent_3','bias_percent_4','bias_percent_5','bias_percent_6','bias_percent_8','bias_percent_9','normalized_power', 'power_SP','SP','CV','total_power_MW','fuel_fraction','thermal_efficiency'};
            file_name = strcat(write_folder_name,'data_',string(i),'_',string(start_second),'.csv');
            writetable(Table,file_name);
        end
    catch
       disp("Failed to write simulation " + string(simulation_number))
    end
end




