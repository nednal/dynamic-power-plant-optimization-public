# writes and reads from the increment_counters text file

def write_increment_variables(closed_loop_counter, FFNN_version_counter, LSTM_version_counter, filename = 'increment_counters.txt'):
    f = open(filename, "w")
    lines = []
    lines = ['closed_loop_counter ',
             'FFNN_version_counter ',
             'LSTM_version_counter ']
    lines[0] += str(closed_loop_counter)
    lines[1] += str(FFNN_version_counter)
    lines[2] += str(LSTM_version_counter) 
    lines = [line +'\n' for line in lines]
    f.writelines(lines)
    f.close


def get_increment_variables(filename = 'increment_counters.txt'):
    f = open(filename, "r")
    lines = f.readlines()
    f.close()
    closed_loop_counter = lines[0].split()[1]
    FFNN_version_counter = lines[1].split()[1]
    LSTM_version_counter = lines[2].split()[1]
    return int(closed_loop_counter), int(FFNN_version_counter), int(LSTM_version_counter)


# quick test:

#write_increment_variables(100,6,12)
#print(get_increment_variables())
