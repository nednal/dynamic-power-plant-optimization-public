function [power, T_steam_out] = turbine(T_steam_in, P_steam_in, P_steam_out, m_dot_steam, efficiency)

entropy_steam_in            = steam_property('s_pt', P_steam_in, T_steam_in);
entropy_steam_out           = entropy_steam_in;

enthalpy_steam_in           = steam_property('h_pt', P_steam_in, T_steam_in);
enthalpy_steam_ideal_out    = steam_property('h_ps', P_steam_out, entropy_steam_out);
enthalpy_steam_out          = enthalpy_steam_in + efficiency * (enthalpy_steam_ideal_out - enthalpy_steam_in);

T_steam_out                 = steam_property('T_ph', P_steam_out, enthalpy_steam_out);

power = (enthalpy_steam_in - enthalpy_steam_out) * m_dot_steam;

% power = 0;
% T_steam_out = 0;
% P_steam_out = 0;