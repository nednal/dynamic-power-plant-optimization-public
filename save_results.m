
%% got halfway through making a function to save csv files and decided it was easier to save matlab objects. Leaving this here in case I change my mind

% index_number = 1
% simOut(1).timeseries_CV.time

% LastName = {'Smith';'Johnson';'Williams';'Jones';'Brown'};
% Age = [38;43;38;40;49];
% Height = [71;69;64;67;64];
% Weight = [176;163;131;133;119];
% BloodPressure = [124 93; 109 77; 125 83; 117 75; 122 80];
% 
% % T = table(Age,Height,Weight,BloodPressure,...
% %     'RowNames',LastName)
% 
% T = table(Age,Height,Weight,BloodPressure);
% 
% writetable(T,'simulation_results/myPatientData.csv','WriteRowNames',true,'Delimiter',',')  
% 

% % get time array
% time = simOut(1).tout;
% 
% % get data arrays
% CV = simOut(1).timeseries_CV.Data;
% HP_power_MW = simOut(1).timeseries_HP_power_MW.Data;
% LP_power_MW = simOut(1).timeseries_LP_power_MW.Data;
% MP_power_MW = simOut(1).timeseries_MP_power_MW.Data;
% SOFA_noDelay = simOut(1).timeseries_SOFA_noDelay.Data;
% SOFAs = simOut(1).timeseries_SOFAs.Data;
% SP = simOut(1).timeseries_SP.Data;
% T_boiler_i = simOut(1).timeseries_T_boiler_i.Data;
% T_boiler_wall_i = simOut(1).timeseries_T_boiler_wall_i.Data;
% T_gas_RH_i = simOut(1).timeseries_T_gas_RH_i.Data;
% T_gas_SH_i = simOut(1).timeseries_T_gas_SH_i.Data;
% T_pipe_RH_i = simOut(1).timeseries_T_pipe_RH_i.Data;
% T_pipe_SH_i = simOut(1).timeseries_T_pipe_SH_i.Data;
% T_steam_HP_out = simOut(1).timeseries_T_steam_HP_out.Data;
% T_steam_LP_out = simOut(1).timeseries_T_steam_LP_out.Data;
% T_steam_MP_out = simOut(1).timeseries_T_steam_MP_out.Data;
% T_steam_RH_i = simOut(1).timeseries_T_steam_RH_i.Data;
% T_steam_SH_i = simOut(1).timeseries_T_steam_SH_i.Data;
% T_waterwall_i = simOut(1).timeseries_T_waterwall_i.Data;
% air_inj = simOut(1).timeseries_air_inj.Data;
% air_inj_noDelay = simOut(1).timeseries_air_inj_noDelay.Data;
% fuel_inj = simOut(1).timeseries_fuel_inj.Data;
% load_fraction = simOut(1).timeseries_load_fraction.Data;
% m_dot_gas = simOut(1).timeseries_m_dot_gas.Data;
% m_dot_steam = simOut(1).timeseries_m_dot_steam.Data;
% m_dot_steam_noDelay = simOut(1).timeseries_m_dot_steam_noDelay.Data;
% n_ij = simOut(1).timeseries_n_ij.Data;
% n_inj_i = simOut(1).timeseries_n_inj_i.Data;
% n_out_i = simOut(1).timeseries_n_out_i.Data;
% r_rxn_1_i = simOut(1).timeseries_r_rxn_1_i.Data;
% r_rxn_2_i = simOut(1).timeseries_r_rxn_2_i.Data;
% thermal_efficiency = simOut(1).timeseries_thermal_efficiency.Data;
% total_fuel_inj = simOut(1).timeseries_total_fuel_inj.Data;
% total_power_MW = simOut(1).timeseries_total_power_MW.Data;



% T = table(time, CV, HP_power_MW, LP_power_MW, MP_power_MW, SOFA_noDelay, ...
%     SOFAs, SP, T_boiler_i, T_boiler_wall_i, T_gas_RH_i, T_gas_SH_i, T_pipe_RH_i, ...
%     T_pipe_SH_i, T_steam_HP_out, T_steam_LP_out, T_steam_MP_out, T_steam_RH_i, ...
%     T_steam_SH_i, T_waterwall_i, air_inj, air_inj_noDelay, fuel_inj, load_fraction, ...
%     m_dot_gas, m_dot_steam, m_dot_steam_noDelay, n_ij, n_inj_i, n_out_i, r_rxn_1_i, r_rxn_2_i, thermal_efficiency, total_fuel_inj, total_power_MW);
