
# inexplicably necessary; must be done before importing matlab.engine
from blackmagic import sorcery

# feed it doubles, not ints; it doesn't allow you to return int arrays
import matlab.engine
import matplotlib.pyplot as plt
#import pandas as pd
import numpy as np
from predict_horizon import make_prediction, make_prediction_FF
from plot_results import plot_results
import pickle
from tensorflow.keras.models import load_model
from pyswarm import pso


run_sims = True
use_constraints = False
use_custom_manual_inputs = False


if run_sims:
    eng = matlab.engine.start_matlab()


def advance_simulation(time_array, load_array, bias_matrix, simulink_model, is_first_time=True, use_full_load_switch=True, current_return_values = None, run_number = 0):
    # convert to matlab variable types
    time_array = matlab.double(time_array.astype('float64').tolist())
    load_array = matlab.double(load_array.astype('float64').tolist())
    bias_matrix = matlab.double(bias_matrix.astype('float64').tolist())
    if is_first_time:
        is_first_time = 1.0
    else:
        is_first_time = 0.0
        
    return_values = eng.python_matlab_closed_loop_control(time_array, load_array, bias_matrix, is_first_time, use_full_load_switch, simulink_model, run_number)

    if current_return_values is None:
        return np.array(return_values)
    else:
        return np.vstack((current_return_values, np.array(return_values)))

def get_past_inputs_outputs(return_values_np, num_timesteps, timestep_size):
    injection_normalization_factor = 2000
    time = return_values_np[:,0]
    CV = return_values_np[:,1]
    total_power_MW = return_values_np[:,4]
    fuel_fraction = return_values_np[:,5]
    n_inj_i = return_values_np[:,6:16]
    n_inj_noBias_i = return_values_np[:,16:26]
    normalized_power = return_values_np[:,26]
    load_fraction = return_values_np[:,27]
    
    bias_percent = (np.array(n_inj_i) - np.array(n_inj_noBias_i)) / injection_normalization_factor
    bias_filter = [0,1,2,3,4,5,7,8] # drop out the bias values that are always zero
    bias_percent = bias_percent[:,bias_filter]
    
    load_fraction = np.array(load_fraction).reshape([len(load_fraction),1])
    bias_and_load = np.hstack((bias_percent,load_fraction))
    bias_and_load = bias_and_load.tolist()
    
    # we want a total of num_timesteps values, separated by timestep_size indexes
    index_start = len(return_values_np) - timestep_size * (num_timesteps-1) - 1
    
    return bias_and_load[index_start::timestep_size], fuel_fraction[index_start::timestep_size], CV[index_start::timestep_size]


def start_up_simulation(num_minutes, simulink_model, use_full_load_switch=True, run_number = 0):
    t_f = num_minutes * 60 # convert from minutes to seconds
    if use_full_load_switch:
        load_init = 1
    else:
        load_init = 0.5
    time_array = np.array([0,t_f]).reshape((2,1))
    load_array = np.array([load_init,load_init]).reshape((2,1))
    bias_matrix = np.zeros([2,8])
    return_values_np = advance_simulation(time_array, load_array, bias_matrix, simulink_model, use_full_load_switch=use_full_load_switch, run_number = run_number)
    return t_f, return_values_np

def continue_simulation(t_0, num_minutes, current_load, current_bias, load_array, bias_matrix, return_values_np, simulink_model, use_full_load_switch=True, run_number = 0):
    t_f = t_0 + num_minutes * 60 # convert from minutes to seconds
    time_array = np.arange(t_0,t_f+0.1,60).reshape((num_minutes+1,1))
    load_array = np.insert(load_array, 0, current_load)
    load_array = np.array(load_array).reshape((num_minutes+1,1))
    bias_matrix = np.insert(bias_matrix, 0, current_bias, axis=0)
    return_values_np = advance_simulation(time_array, load_array, bias_matrix, simulink_model, is_first_time=False, use_full_load_switch=use_full_load_switch, current_return_values = return_values_np, run_number = run_number)
    return t_f, return_values_np

def optimize(duplicate_filer, num_predictions, decision_frequency, past_output_fuel, past_output_CV,
             past_inputs, future_load, keras_file_obj, keras_file_con, SP = 0.04, bias_limit = 0.25,
             dMax = 0.5, skip_optimization=False, closed_loop_vars=None):

    if SP is not 0.4:
        print('using SP constraint',SP)
    
    model_obj = load_model(keras_file_obj) # preload the model so it only does it once
    model_con = load_model(keras_file_con) # preload the model so it only does it once
    
    future_load = np.reshape(future_load, [len(future_load),1])
    past_output_fuel = np.reshape(past_output_fuel, [len(past_output_fuel),1])
    
    matrix_shape = (int(num_predictions/decision_frequency),8) # 8 bias values
    array_shape = matrix_shape[0] * matrix_shape[1]
    num_timesteps = matrix_shape[0]

    lb = np.ones(array_shape) * -bias_limit
    ub = np.ones(array_shape) * bias_limit
    lb_array = lb.reshape(matrix_shape)
    ub_array = ub.reshape(matrix_shape)

    current_bias = np.array(past_inputs)[-1,:-1]


    def scale_dMax(bias_matrix):
        # scale from (0-1) to the desired range given dMax and ub/lb
        # row = time, col = air level
        previous_timestep = current_bias
        for t in range(num_timesteps):
            low = np.maximum(lb_array[t,:], previous_timestep - dMax)
            high = np.minimum(ub_array[t,:], previous_timestep + dMax)
            #print('previous_timestep',previous_timestep)
            #print('high',high)
            #print('low',low)
            #print(previous_timestep * (high - low) + low)
            bias_matrix[t,:] = bias_matrix[t,:] * (high - low) + low
            previous_timestep = bias_matrix[t,:]
        return bias_matrix
    
    def obj_function(bias_array_guess_raw):
        # make a copy. This is super important, or it actually messes with the PSO
        bias_matrix_guess = np.copy(bias_array_guess_raw)
        #print('bias_array_guess_raw',bias_array_guess_raw)
        #print('initial size:', np.shape(bias_array_guess))
        bias_matrix_guess = bias_matrix_guess.reshape(matrix_shape)
        if use_constraints:
            bias_matrix_guess = scale_dMax(bias_matrix_guess)
        # apply the duplication filter to allow for variable step-size
        future_inputs_guess = bias_matrix_guess[duplicate_filer]
        future_inputs_guess = np.hstack([future_inputs_guess, future_load])
        # sum of predicted fuel
        predicted_fuel_sum = np.sum(make_prediction(past_output_fuel, past_inputs, future_inputs_guess, keras_file = keras_file_obj, model=model_obj))
        # array of predicted CV values
        predicted_CV_array = make_prediction(past_output_CV, past_inputs, future_inputs_guess, keras_file = keras_file_con, model=model_con)
        # create linear pentalty for going below SP. Positive penalty is bad, otherwise zero pentalty.
        CV_penalty = -np.sum(np.minimum(predicted_CV_array-SP,0) * 10000)
        # minimize predicted fuel plus the constraint penalty
        #print('bias_array_guess_raw (again)',bias_array_guess_raw)
        return predicted_fuel_sum + CV_penalty

    
    #def constraint_function(bias_array_guess):
    #    #print('initial size:', np.shape(bias_array_guess))
    #    bias_matrix_guess = bias_array_guess.reshape(matrix_shape)
    #    answer = []
    #    #  apply dMax constraint (also constrains against current values)
    #    for timestep_index in range(len(bias_matrix_guess)):
    #        for bias_index in range(len(bias_matrix_guess[0])):
    #            if timestep_index == 0:
    #                answer.append(dMax - np.abs(bias_matrix_guess[timestep_index,bias_index]
    #                                          - current_bias[bias_index]))
    #            else:
    #                answer.append(dMax - np.abs(bias_matrix_guess[timestep_index,bias_index]
    #                                          - bias_matrix_guess[timestep_index-1,bias_index]))
    #    return answer

    #if use_constraints:
    #    lb = np.ones(matrix_shape) * -bias_limit
    #    ub = np.ones(matrix_shape) * bias_limit
    #    for timestep_index in range(len(lb)):
    #        for bias_index in range(len(lb[0])):
    #            lb_2 = current_bias[bias_index] - dMax * (timestep_index+1)
    #            ub_2 = current_bias[bias_index] + dMax * (timestep_index+1)
    #            lb[timestep_index,bias_index] = max(lb[timestep_index,bias_index], lb_2)
    #            ub[timestep_index,bias_index] = min(ub[timestep_index,bias_index], ub_2)
    #    lb = lb.reshape(array_shape)
    #    ub = ub.reshape(array_shape)
    #else:
    print('dMax =',dMax)
    
    #print('lb',lb_array)
    #print('ub',ub_array)

    if closed_loop_vars is not None:

        print('beginning optimization using given optimizer/model')

        def obj_function_wrapper(bias_array_guess_raw):
            # inputs are from 0-1, need to rescale to given upper/lower bounds
            return obj_function(bias_array_guess_raw * (ub - lb) + lb)
            
        # perform optimization
        xopt = closed_loop_vars.perform_optimization(obj_function_wrapper)

##        rescale xopt
        xopt = xopt * (ub - lb) + lb
        
##        future_inputs = xopt.reshape(matrix_shape)[duplicate_filer]
##        future_inputs = np.hstack([future_inputs, future_load])
##        
##        predicted_fuel_array = make_prediction(past_output_fuel, past_inputs, future_inputs,
##                                               keras_file = keras_file_obj, model=model_obj)
##        predicted_CV_array = make_prediction(past_output_CV, past_inputs, future_inputs,
##                                             keras_file = keras_file_con, model=model_con)
##        
##        return future_inputs, predicted_fuel_array, predicted_CV_array
    
    elif skip_optimization:
        print('skipping optimization')
        if use_constraints:
            xopt = 0.5 * np.ones(array_shape)
        else:
            if use_custom_manual_inputs:
                 single_row = np.reshape(np.linspace(0.25,-0.25,8),[1,8])
                 xopt_mat = np.tile(single_row,(matrix_shape[0],1))
                 #print('xopt',xopt_mat)
                 xopt = np.reshape(xopt_mat,array_shape)
            else:
                xopt = np.zeros(array_shape)
        print('all biases set to zero')
    else:
        print('beginning optimization')
        #xopt, fopt = pso(obj_function, lb, ub, debug = True, maxiter=2)
        #xopt, fopt = pso(obj_function, lb, ub, debug = False)
        # PSO parameters https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.298.4359&rep=rep1&type=pdf
        #if use_constraints:
        #    cons = constraint_function
        #else:
        #    cons = None

        lb_pso = lb
        ub_pso = ub
        if use_constraints:
            print('using constraints')
            lb_pso = np.zeros(array_shape)
            ub_pso = np.ones(array_shape)

        #print('pso lb = ',lb_pso)
        #print('pso ub = ',ub_pso)
        xopt, fopt = pso(obj_function, lb_pso, ub_pso,
                         swarmsize=100, #161
                         omega=-0.2089,
                         phip=-0.0787,
                         phig=3.7637,
                         maxiter=10,#100, #1200
                         debug=False)
        
        print('optimization complete')
        #print('raw xopt',xopt)
    if use_constraints:
        #print('future inputs without scale_dMax:')
        #print(xopt.reshape(matrix_shape)[duplicate_filer])
        future_inputs = scale_dMax(xopt.reshape(matrix_shape))[duplicate_filer]
        #print('future inputs:')
        #print(future_inputs)
        #print('TEST')
        #test = np.random.rand(num_timesteps,8)
        #print(test)
        #print(scale_dMax(test))
    else:
        future_inputs = xopt.reshape(matrix_shape)[duplicate_filer]
    future_inputs = np.hstack([future_inputs, future_load])

    predicted_fuel_array = make_prediction(past_output_fuel, past_inputs, future_inputs, keras_file = keras_file_obj, model=model_obj)
    predicted_CV_array = make_prediction(past_output_CV, past_inputs, future_inputs, keras_file = keras_file_con, model=model_con)

    #print('current predicted CV:',predicted_CV_array)
    #print('penalty array:',np.minimum(predicted_CV_array-SP,0) * 10000)
    #print('CV_penalty:', -np.sum(np.minimum(predicted_CV_array-SP,0) * 10000))
    #print('predicted fuel sum:',np.sum(predicted_fuel_array))
    
    return future_inputs, predicted_fuel_array, predicted_CV_array

def optimize_SS(previous_fuel, previous_CV, current_load, keras_file_obj, keras_file_con, SP = 0.04, bias_limit = 0.25, dMax = 0.01, timesteps=10):
    model_obj = load_model(keras_file_obj) # preload the model so it only does it once
    model_con = load_model(keras_file_con) # preload the model so it only does it once

    lb = np.ones(8) * -bias_limit
    ub = np.ones(8) * bias_limit
    
    #if use_constraints:
    #    pass
    
    def obj_function(bias_array_guess):
        #print('previous_fuel',previous_fuel)
        #print('previous_CV',previous_CV)
        #print('bias_array_guess',bias_array_guess)
        #print('current_load',current_load)
        
        #stacked_inputs = [list(np.concatenate([[previous_fuel], bias_array_guess, [current_load]],axis=0))]
        stacked_inputs = [list(np.concatenate([[previous_fuel], bias_array_guess],axis=0))]
        
        #print('stacked_inputs',stacked_inputs)
        predicted_fuel = make_prediction_FF(stacked_inputs, model = model_obj)[0][0]
        
        #stacked_inputs = [list(np.concatenate([[previous_CV], bias_array_guess, [current_load]],axis=0))]
        stacked_inputs = [list(np.concatenate([[previous_CV], bias_array_guess],axis=0))]
        
        #print('stacked_inputs',stacked_inputs)
        #CV_penalty = make_prediction_FF(stacked_inputs, model = model_con)[0][0] * -10000
        return predicted_fuel #+ CV_penalty

    xopt, fopt = pso(obj_function, lb, ub,
                     swarmsize=100, #161
                     omega=-0.2089,
                     phip=-0.0787,
                     phig=3.7637,
                     maxiter=10,#100, #1200
                     debug=False)
    future_biases = np.tile(xopt,[timesteps,1])
    
    #stacked_inputs = [list(np.concatenate([[previous_fuel], xopt, [current_load]],axis=0))]
    stacked_inputs = [list(np.concatenate([[previous_fuel], xopt],axis=0))]
    
    predicted_fuel = make_prediction_FF(stacked_inputs, model = model_obj)[0][0]
    predicted_fuel_array = np.tile(predicted_fuel,[timesteps])
    
    #stacked_inputs = [list(np.concatenate([[previous_CV], xopt, [current_load]],axis=0))]
    stacked_inputs = [list(np.concatenate([[previous_CV], xopt],axis=0))]
    
    predicted_CV = make_prediction_FF(stacked_inputs, model = model_con)[0][0]
    predicted_CV_array = np.tile(predicted_CV,[timesteps])
    
    return future_biases, predicted_fuel_array, predicted_CV_array
    

def get_load_profile(sample_size, approx_minutes=7*24*60, set_profile=None, closed_loop_vars = None):

    if closed_loop_vars is not None:
        np.random.seed(closed_loop_vars.random_seed)
        approx_minutes = closed_loop_vars.simulation_length

    if set_profile is not None:
        print('using the set load profile:',set_profile)
    else:
        print('generating random load profile approx.',approx_minutes,'minutes')
    # max ramp rate is 1=>0.5 over 24 minutes (or dLoad/dt <= 1/48)
    
    if set_profile=='W':
        load_profile = np.ones(sample_size)
        load_profile = np.hstack([load_profile, np.linspace(1,0.5,24)])
        load_profile = np.hstack([load_profile, np.ones(5)*0.5])
        load_profile = np.hstack([load_profile, np.linspace(0.5,1,24)])
        load_profile = np.hstack([load_profile, np.ones(5)])
        load_profile = np.hstack([load_profile, np.linspace(1,0.5,24)])
        load_profile = np.hstack([load_profile, np.linspace(0.5,1,24)])
        load_profile = np.hstack([load_profile, np.ones(12)])
        return load_profile
    
    if set_profile=='down fast':
        load_profile = np.ones(sample_size)
        load_profile = np.hstack([load_profile, np.linspace(1,0.5,24)])
        load_profile = np.hstack([load_profile, np.ones(5)*0.5])
        return load_profile
    
    if set_profile=='down slow':
        load_profile = np.ones(sample_size)
        load_profile = np.hstack([load_profile, np.linspace(1,0.5,48)])
        load_profile = np.hstack([load_profile, np.ones(5)*0.5])
        return load_profile
    
    if set_profile=='up fast':
        load_profile = 0.5 * np.ones(sample_size)
        load_profile = np.hstack([load_profile, np.linspace(0.5,1,24)])
        load_profile = np.hstack([load_profile, np.ones(5)])
        return load_profile
    
    if set_profile=='up slow':
        load_profile = 0.5 * np.ones(sample_size)
        load_profile = np.hstack([load_profile, np.linspace(0.5,1,48)])
        load_profile = np.hstack([load_profile, np.ones(5)])
        return load_profile
    
    load_profile = np.ones(sample_size)
    while len(load_profile) < approx_minutes:
        current_load = load_profile[-1]
        new_load = np.random.rand()
        if new_load < 0.5:
            # hold current load for 5-15 minutes
            load_profile = np.hstack([load_profile, np.ones(np.random.randint(5,16))*current_load])
        else:
            min_minutes_for_ramp = int(np.abs(current_load - new_load) * 48)
            ramp_length = np.random.randint(min_minutes_for_ramp, min_minutes_for_ramp*2+1)
            load_profile = np.hstack([load_profile, np.linspace(current_load,new_load,ramp_length)[1:]])
    return load_profile

def Master_driver(closed_loop_counter = None, FFNN_version_counter = None, LSTM_version_counter = None,
                  text_and_plot_when_complete = False, run_on_Watson = True, closed_loop_vars = None):
    if closed_loop_counter is not None:
        closed_loop_folder = 'run' + str(closed_loop_counter)
        run_number = closed_loop_counter
        # make directory:
        import os
        folder1 = "machine_learning_data/" + closed_loop_folder
        folder2 = "simulation_results/" + closed_loop_folder
        if not os.path.exists(folder1):
            os.makedirs(folder1)
        if not os.path.exists(folder2):
            os.makedirs(folder2)
        # add on the slash to make it plug and play for later
        closed_loop_folder += '/'
    else:
        closed_loop_folder = ''
        run_number = 0
    
    #try:
    if True:
        if closed_loop_vars is None:
            num_predictions = 10 # should be based on the ML model inputs
            decision_frequency = 2 # should evenly divide into num_predictions
            sample_size = 12 # based on the ML model; how far back the model can look in time (in minutes)
        else:
            num_predictions = closed_loop_vars.num_predictions
            decision_frequency = closed_loop_vars.decision_frequency 
            sample_size = closed_loop_vars.sample_size
            
        skip_optimization = False # make sure steady_state_optimization is False
        steady_state_optimization = False # make sure skip_optimization is False
         # run on the campus computer "Watson"
        use_SS_SP_penalty = False # use the steady-state output as the SP penalty term
        
        set_profile = None
        #set_profile = 'down fast'
        #set_profile = 'down slow'
        #set_profile = 'W'
        use_full_load_switch = True
        
        #set_profile = 'up fast'
        #set_profile = 'up slow'
        #use_full_load_switch = False
        
        
        if closed_loop_vars is not None:
            keras_file_obj = closed_loop_vars.keras_file_obj
            keras_file_con = closed_loop_vars.keras_file_con
        elif steady_state_optimization:
            if FFNN_version_counter is not None:
                keras_file_obj = 'machine_learning_data/' + 'model_fuel_ff_v' + str(FFNN_version_counter)
                keras_file_con = 'machine_learning_data/' + 'model_CV_ff_v' + str(FFNN_version_counter)
            else:
                keras_file_obj = 'machine_learning_data/' + 'model_fuel_ff_v6'
                keras_file_con = 'machine_learning_data/' + 'model_CV_ff_v6'
        else:
            if LSTM_version_counter is not None:
                keras_file_obj = 'machine_learning_data/' + 'model_fuel_10_v' + str(LSTM_version_counter)
                keras_file_con = 'machine_learning_data/' + 'model_CV_10_v' + str(LSTM_version_counter)
            else:
                keras_file_obj = 'machine_learning_data/' + 'model_fuel_10_v12'
                keras_file_con = 'machine_learning_data/' + 'model_CV_10_v12'
        
        print('Using ML models:')
        print(keras_file_obj)
        print(keras_file_con)
        
        if run_on_Watson:
            simulink_model = "Simple_plant_2019b"
        else:
            simulink_model = "Simple_plant"

        print('Running Simulink model:',simulink_model)

        print('run_sims',run_sims)
        print('use_constraints',use_constraints)
        print('use_custom_manual_inputs',use_custom_manual_inputs)
        print('use_SS_SP_penalty',use_SS_SP_penalty)

        duplicate_filer = []
        for i in range(int(num_predictions/decision_frequency)):
            for j in range(decision_frequency):
                duplicate_filer.append(i)

        # create load profile (start, ramp down, hold, ramp up, hold, ramp down, ramp up, hold)
        # ramp rate should be 100% to 50% in 16-24 minutes. Jake recomends the slower end of this range ~10 MW/min.
        if run_on_Watson:
            load_profile = get_load_profile(sample_size, set_profile=set_profile, closed_loop_vars=closed_loop_vars) # defaults to a ~week of simulation time; great for Watson since he can do 10 sim-minutes in 20 real minutes.
        else: # run on my laptop
            load_profile = get_load_profile(sample_size, approx_minutes=200, set_profile=set_profile, closed_loop_vars=closed_loop_vars) # takes about 12 hours on my laptop to plow through. Any longer, and it starts to lag terribly.
        t_final = len(load_profile)

        #plt.plot(load_profile)
        #plt.show()

        if run_sims:
            print('starting up simulation')
            current_time, return_values_np = start_up_simulation(sample_size, simulink_model, use_full_load_switch=use_full_load_switch, run_number=run_number)
            current_time_min = int(current_time/60)
            print('finished start-up. Time (min):',current_time_min)
            #past_inputs, past_output_power, past_output_CV = get_past_inputs_outputs(return_values_np, sample_size, 60)
            save_values = [current_time, return_values_np]
            print('pickling values')
            pickle.dump( save_values, open( "temp_values.p", "wb" ) )
        else:
            print('skipping simulation')
            print('unpickling values')
            load_values = pickle.load( open( "temp_values.p", "rb" ) )
            current_time, return_values_np = load_values
            current_time_min = int(current_time/60)
        
        # fill predicted arrays with nans because there are no predictions during the start-up
        predicted_fuel_array = np.zeros(sample_size) * np.nan
        predicted_CV_array = np.zeros(sample_size) * np.nan
        
        while current_time_min <= t_final - num_predictions:
            future_load = load_profile[current_time_min:current_time_min+num_predictions]
            past_inputs, past_output_fuel, past_output_CV = get_past_inputs_outputs(return_values_np, sample_size, 60)

            print('performing optimization')
            if steady_state_optimization:
                future_biases, new_predicted_fuel_array, new_predicted_CV_array = optimize_SS(past_output_fuel[-1], past_output_CV[-1], future_load[0], keras_file_obj, keras_file_con, timesteps=num_predictions)
                future_inputs = np.hstack([future_biases, future_load.reshape([len(future_load),1]) ])
            else:
                if use_SS_SP_penalty:
                    if set_profile == 'up fast':
                        CV_file = "machine_learning_data/8_best_fast_up_SS/aaa_most_recent_results_32.p"
                        return_values_np_temp, _, _ = pickle.load( open( CV_file, "rb" ) )
                        CV_SS_array = return_values_np_temp[:,1]
                        CV_SS_average = np.average(CV_SS_array[719:])
                        CV_SS = CV_SS_average * np.ones(np.shape(CV_SS_array))
                    elif set_profile == 'up slow':
                        CV_file = "machine_learning_data/11_best_slow_up_SS/aaa_most_recent_results_62.p"
                        return_values_np_temp, _, _ = pickle.load( open( CV_file, "rb" ) )
                        CV_SS_array = return_values_np_temp[:,1]
                        CV_SS_average = np.average(CV_SS_array[719:])
                        CV_SS = CV_SS_average * np.ones(np.shape(CV_SS_array))
                    else:
                        CV_SS_average = 0.4
                        CV_SS = CV_SS_average * np.ones(np.shape(load_profile))
                    print('CV_SS_average',CV_SS_average)
                    
                    print('CV_SS',CV_SS)
                    future_inputs, new_predicted_fuel_array, new_predicted_CV_array = optimize(duplicate_filer, num_predictions, decision_frequency, past_output_fuel, past_output_CV, past_inputs,
                                                                                               future_load, keras_file_obj, keras_file_con, skip_optimization=skip_optimization, SP = CV_SS[current_time_min:current_time_min+num_predictions], closed_loop_vars=closed_loop_vars)
                else:
                    future_inputs, new_predicted_fuel_array, new_predicted_CV_array = optimize(duplicate_filer, num_predictions, decision_frequency, past_output_fuel, past_output_CV, past_inputs,
                                                                                               future_load, keras_file_obj, keras_file_con, skip_optimization=skip_optimization, closed_loop_vars=closed_loop_vars)
            
            predicted_fuel_array = np.hstack([predicted_fuel_array,new_predicted_fuel_array])
            predicted_CV_array = np.hstack([predicted_CV_array,new_predicted_CV_array])
            
            #print('future_inputs')
            #print(future_inputs)
            
            if run_sims:
                print('running simulation from minute',current_time_min,'to',current_time_min+num_predictions)
                current_load = np.array(past_inputs)[-1,-1]
                current_bias = np.array(past_inputs)[-1,:-1]
                load_array = np.array(future_inputs)[:,-1]
                bias_matrix = np.array(future_inputs)[:,:-1]
                current_time, return_values_np = continue_simulation(current_time, num_predictions, current_load, current_bias, load_array, bias_matrix, return_values_np, simulink_model, use_full_load_switch=use_full_load_switch, run_number=run_number)
                current_time_min = int(current_time/60)
                print('finished current simulation. Time (min):',current_time_min,'of',t_final)
                print('pickling values just in case')
                save_values = [return_values_np, predicted_fuel_array, predicted_CV_array]
                pickle.dump( save_values, open( "machine_learning_data/"+closed_loop_folder+"aaa_most_recent_results_"+str(current_time_min)+".p", "wb" ) )
            else:
                print('skipping the remaining simulations')
                break
        
        #calculate efficiency and save run information in text file
        parastic_load = 0.1
        thermal_efficiency = return_values_np[:,3] * (1-parastic_load)
        thermal_efficiency_average = np.round(np.average(thermal_efficiency),6)
        filename = "machine_learning_data/"+closed_loop_folder+"aaa_efficiency_" + str(thermal_efficiency_average) + ".txt"
        f = open(filename, "w")
        opt_type = 'dynamic'
        if steady_state_optimization:
            opt_type = 'steady-state'
        elif skip_optimization:
            opt_type = 'baseline'
        f.write("opt_type: " + str(opt_type) +"\n")
        f.write("set_profile: " + str(set_profile) +"\n")
        f.write("run_on_Watson: " + str(run_on_Watson) +"\n")
        f.write("use_full_load_switch: " + str(use_full_load_switch) +"\n")
        f.write("NN_versions: " + keras_file_obj + " and " + keras_file_con +"\n")
        f.write("use_SS_SP_penalty: " + str(use_SS_SP_penalty) +"\n")
        if closed_loop_vars is not None:
            f.write("ML_type used: " + closed_loop_vars.ML_type +"\n")
            f.write("optimizer used: " + closed_loop_vars.optimizer +"\n")
            f.write("refined metaoptimiation index used: " + str(closed_loop_vars.df_index) +"\n")
            f.write("total function evaluations: " + str(np.sum(np.array(closed_loop_vars.function_evals_array))) +"\n")
            pickle.dump( closed_loop_vars, open( "machine_learning_data/"+str(closed_loop_folder)+"closed_loop_vars.p", "wb" ) )
        f.close()

        if text_and_plot_when_complete:
            from send_text_message import send_message
            if run_on_Watson:
                send_message("finished running closed-loop simulation on Watson.")
            else:
                send_message("finished running closed-loop simulation on laptop.")

            print('plotting and saving figure')
            predicted_time_array = np.arange(1,len(predicted_fuel_array)+1) * 60 # convert to seconds
            plot_results(return_values_np, predicted_time_array, predicted_fuel_array, predicted_CV_array)
'''
    except Exception as e:
        import sys
        from send_text_message import send_message
        if run_on_Watson:
            send_message("closed-loop simulation crashed on Watson.")
        else:
            send_message("closed-loop simulation crashed on laptop.")
        print(repr(e))
        return "Error"
'''


if __name__ == "__main__":
    # test
    #Master_driver(101, 6, 12, False)
    Master_driver(101, 1, 7, text_and_plot_when_complete = False, run_on_Watson = False )


