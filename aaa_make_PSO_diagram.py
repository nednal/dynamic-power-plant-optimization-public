
import numpy as np
import matplotlib.pyplot as plt

# workaround from github issue to fix the arrowheads being dashed
# source: https://matplotlib.org/stable/gallery/lines_bars_and_markers/linestyles.html
def draw_arrow(arrowStart, arrowStop, arrowColor, linestyle):
    plt.annotate("",arrowStop,xytext=arrowStart,arrowprops=dict(arrowstyle='-',shrinkA=0,shrinkB=5,edgecolor=arrowColor,facecolor="none",linestyle=linestyle), label='test')
    plt.annotate("",arrowStop,xytext=arrowStart,arrowprops=dict(linewidth=0,arrowstyle="-|>",shrinkA=0,shrinkB=0,edgecolor="none",facecolor=arrowColor,linestyle="solid"))

x               = np.array([0.75,0])
x_best          = np.array([0.2,-0.6])
x_global_best   = np.array([-0.75,.1])
x_v             = np.array([0.8,0.5])
c1 = 0.5
c2 = 0.25

delta_v = x_v-x
delta_best = c1 * (x_best-x)
delta_global_best = c2 * (x_global_best-x)
x_new = x + delta_v + delta_best + delta_global_best

plt.figure()

plt.plot(x[0],x[1],'.k',label='particle',markersize=10)
plt.plot(x_best[0],x_best[1],'^k',label='particle best',markersize=10)
plt.plot(x_global_best[0],x_global_best[1],'*k',label='global best',markersize=10)
plt.scatter(x_new[0], x_new[1], s=25, facecolors='none', edgecolors='k',label='new particle')

draw_arrow(x, x + delta_v, 'black','dashed')
draw_arrow(x, x + delta_best, 'black','dotted')
draw_arrow(x, x + delta_global_best, 'black','dashdot')
draw_arrow(x, x_new, 'black','solid')

# hack: draw some lines off screen to fill out the legend
plt.plot([100,101],[100,101],'--k',label='velocity component')
plt.plot([100,101],[100,101],':k',label='particle best component')
plt.plot([100,101],[100,101],'-.k',label='swarm best component')
plt.plot([100,101],[100,101],'-k',label='new velocity')

plt.legend(loc='lower left')
plt.xlim([-1,1])
plt.ylim([-1,1])

ax = plt.gca()
ax.axes.xaxis.set_visible(False)
ax.axes.yaxis.set_visible(False)

plt.savefig('PHD_figures/PSO_diagram.png', transparent=True)
plt.savefig('PHD_figures/PSO_diagram.eps', format='eps',bbox_inches="tight")
plt.show()
