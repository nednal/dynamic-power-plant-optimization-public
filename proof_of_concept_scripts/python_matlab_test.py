
import matlab.engine
import matplotlib.pyplot as plt
#import pandas as pd
import numpy as np

# feed it doubles, not ints; it doesn't allow you to return int arrays


eng = matlab.engine.start_matlab()

time_array = []
output_array = []

time, output = eng.python_matlab_test(0.0, 0.0, 15.0, 1.0)
time_array += time
output_array += output

time, output = eng.python_matlab_test(10.0, 15.0, 30.0, 0.0)
time_array += time
output_array += output

time, output = eng.python_matlab_test(-5.0, 30.0, 45.0, 0.0)
time_array += time
output_array += output


plt.plot(time_array, output_array)
plt.show()

#print(eng.python_matlab_test())
