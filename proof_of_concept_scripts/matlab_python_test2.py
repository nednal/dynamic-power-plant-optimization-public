#import matlab.engine
# eng = matlab.engine.start_matlab()

import numpy as np

def test2(word):
    print("did the second one work?",word)
    word.append('feedback2')
    return word

def test3(word):
    print("did the third one work?",word)
    word.append('feedback3')
    return word

def test4(word):
    print("did the fourth one work?",word)
    word.append(np.array([1,2,3]))
    return word

def test5(word):
    print("did the fifth one work?",word)
    import sys
    print("Python version")
    print (sys.version)
    print("Version info.")
    print (sys.version_info)
    import tensorflow as tf
    from tensorflow.keras.models import Sequential
    from tensorflow.keras.layers import Dense
    from tensorflow.keras.layers import LSTM
    from predict_horizon import test_prediction 
    word.append(test_prediction([1]))
    return word
