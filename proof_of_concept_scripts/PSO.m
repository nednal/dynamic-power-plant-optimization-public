
rng(12345)
% source: http://www.cleveralgorithms.com/nature-inspired/swarm/pso.html
% also useful https://pdfs.semanticscholar.org/11e9/a02f79ed0ea2a521e5222890ce0689c2fc6e.pdf


% row = particle index
% col = dimension index

% function inputs
num_inputs = 5;
lb = 0;
ub = 10;

% calculate range
range = ub - lb;

% tuning parameters 
num_particles = 30;
max_iterations = 1000;
convergence_spread = range * 0.01; % fraction of range
max_velocity = range * 0.05; % fraction of range
neighborhood_radius = range * 0.3; % fraction of range
global_learning_factor = 2; % recomend that learning factors are between 0-4, usually 2.
personal_learning_factor = 2;
use_neighborhood = true;
neighborhood_learning_factor = 2;
boundary_type = 'stick'; % options: 'reflect', 'stick';
%'reflect' = particles 'bounce' back into the domain
%'stick' = particles stick to the edge (unless pulled away next timestep by a change in velocity); 

max_velocity_array = ones(num_particles,1) * max_velocity;
particle_positions = rand(num_particles,num_inputs) * range + lb;
particle_velocities = 2*(rand(num_particles,num_inputs) - 0.5) .* max_velocity;
global_best_input = zeros(1,num_inputs);
global_best_output = Inf;
personal_best_input = zeros(num_particles,num_inputs);
personal_best_output = zeros(num_particles,1);

particle_fitnesses = zeros(num_particles,1);
parfor row = 1:num_particles
    % function evaluations in parallel
    particle_fitnesses(row,1) = f(particle_positions(row,:));
end

% initialize best inputs and outputs
% find best in swarm
[global_best_output, global_best_index] = min(particle_fitnesses);
global_best_input(1,:) = particle_positions(global_best_index,:);
personal_best_input(:,:) = particle_positions(:,:);
personal_best_output(:,1) = particle_fitnesses(:,1);

% initialize particle distance matrix
particle_distances = zeros(num_particles,num_particles);
max_particle_distance = inf;

tic
% run the algorithm
keep_going = true;
num_iterations = 1;
while keep_going 
    disp('iteration ' + string(num_iterations)+' of '+string(max_iterations)+'. Spread is '+string(max_particle_distance)+' targetting '+string(convergence_spread))
    
    %TODO: implement neighborhood velocity bias
    for i=1:num_particles
        for j=(i+1):num_particles
            particle_distances(i,j) = norm( particle_positions(i,:) - particle_positions(j,:) );
            particle_distances(j,i) = particle_distances(i,j);
        end
    end
    
    particle_velocities(:,:) =  particle_velocities + ...
                                personal_learning_factor * rand(num_particles,num_inputs) .* (personal_best_input - particle_positions) + ...
                                global_learning_factor * rand(num_particles,num_inputs) .* (global_best_input - particle_positions);
    
    
    
    particle_speeds = vecnorm(particle_velocities')';
    % multiply the velocities by a factor less than 1 to cap the velocities (only if the speed is too high)
    particle_velocities = particle_velocities .* min( cat(2,ones(num_particles,1),max_velocity_array ./ particle_speeds), [], 2);
    
    particle_positions = particle_positions + particle_velocities;
    % ensure particles stay within bounds:
    if strcmp(boundary_type,'stick')
        particle_positions = max( particle_positions, lb );
        particle_positions = min( particle_positions, ub );
        not_at_lb = particle_positions ~= lb;
        not_at_ub = particle_positions ~= ub;
        particle_velocities = particle_velocities .* not_at_lb .* not_at_ub;
    elseif strcmp(boundary_type,'reflect')
        below_lb = particle_positions - lb;
        is_below_lb = below_lb < 0;
        below_lb = below_lb .* is_below_lb;
        above_ub = particle_positions - ub;
        is_above_ub = above_ub > 0;
        above_ub = above_ub .* is_above_ub;
        
        is_past_boundary = is_below_lb | is_above_ub;
        amount_past_boundary = below_lb + above_ub;
        reflect_velocity_factor = -2*is_past_boundary + 1;
        
        particle_positions = particle_positions - 2*amount_past_boundary;
        particle_velocities = particle_velocities .* reflect_velocity_factor;
    end
        
    parfor row = 1:num_particles
        % function evaluations in parallel
        particle_fitnesses(row,1) = f(particle_positions(row,:));
    end
    
    [current_best_output, current_best_index] = min(particle_fitnesses);
    if current_best_output < global_best_output
        global_best_input(1,:) = particle_positions(current_best_index,:);
        global_best_output = current_best_output;
    end
    for i = 1:num_particles
        if particle_fitnesses(i) < personal_best_output(i)
            personal_best_input(i,:) = particle_positions(i,:);
            personal_best_output(i,1) = particle_fitnesses(i,1);
        end
    end
    
    max_particle_distance = max(max(particle_distances));
    
    keep_going = num_iterations < max_iterations && convergence_spread < max_particle_distance;
    num_iterations = num_iterations + 1;
end

disp('Final particle convergence is '+string(max_particle_distance));

global_best_output
global_best_input
calculated_output = f(global_best_input)

toc

% optimal_output = f([2;4;6;8;10])
% local_optimal_output = f([5;5;5;5;5;])

% function y = f(x)
%     x = reshape(x,[length(x),1]);
%     solution = [2;4;6;8;10];
%     solution_value = 2;
%     y = sum((x-solution).^2) + solution_value;
% end

% [X,Y] = meshgrid(1:0.1:10,1:0.1:10);
% Z = zeros(length(X),length(Y));
% for i=1:length(X)
%     for j=1:length(Y)
%         Z(i,j) = f([X(i,j);Y(i,j)]);
%     end
% end
% surf(X,Y,Z);

% f([2;4;6;8;10])
% f([5;5;5;5;5;])

function y = f(x)
    x = reshape(x,[length(x),1]);
    global_solution = [2;4;6;8;10];
    local_solution = [10;8;6;4;2];
%     global_solution = [8;8];
%     local_solution = [2;2];
    global_solution_value = 2;
    local_solution_value = 3;
    delta_solution_value = local_solution_value - global_solution_value;
    
    % these first two are smooth, but result in additional local minimums:
%     quint = sum( (x-global_solution).^2 .* (x-local_solution).^2 ) + global_solution_value;
%     quad = sum( delta_solution_value ./ (local_solution-global_solution).^2 .* (x-global_solution).^2) / length(x);
    
    quint =  sum(abs(x-global_solution)).^2 .* sum(abs(x-local_solution)).^2 + global_solution_value;
    quad = delta_solution_value ./ sum(abs(local_solution-global_solution)).^2 .* sum(abs(x-global_solution)).^2 ;
    
    y = quint + quad;
end


