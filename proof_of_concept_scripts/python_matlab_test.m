
% function c = python_matlab_test(a,b)
% c = a + b;

% assertion simulink block:
% https://www.mathworks.com/help/simulink/ug/controlling-execution-of-a-simulation.html
% info about the set_param(...'pause'/'continue') command
% https://www.mathworks.com/help/simulink/ug/using-the-sim-command.html

% matlab Q&A:
% https://www.mathworks.com/matlabcentral/answers/45077-run-a-simulink-model-for-a-certain-amount-of-time-and-pause
% good example:
% https://www.mathworks.com/help/simulink/ug/saving-and-restoring-simulation-operating-point.html

% https://www.mathworks.com/help/simulink/ug/load-data-to-root-level-input-ports-1.html#bsuwm6b
% https://www.mathworks.com/help/simulink/ug/configuration-parameters-dialog-box-overview.html


% %% current function
% 
% function return_vals = python_matlab_test(external_input, t_start, t_end, is_first_time)
% 
% % this is very important; when calling from python, the base workspace and
% % current workspace are not the same, but simulink defaults to the base
% % workspace. use sim('modelname',[],options) -- https://www.mathworks.com/matlabcentral/answers/92888-how-do-i-run-a-model-in-simulink-from-my-matlab-function-where-my-simulink-parameters-are-defined-in
% options = simset('SrcWorkspace','current');
% 
% t_range = [t_start, t_end];
% 
% model_name = 'python_matlab_simulink_test';
% 
% if is_first_time
%     open_system(model_name); 
%     set_param(model_name, 'LoadInitialState', 'off')
%     set_param(model_name, 'SaveFinalState', 'on', 'FinalStateName',...
%      'save_state_end','SaveCompleteFinalSimState', 'on')
% else
%     load('workspace2','save_state_previous_end')
%     set_param(model_name, 'LoadInitialState', 'on', 'InitialState',...
%               'save_state_previous_end','SaveFinalState', 'on', 'FinalStateName',...
%               'save_state_end','SaveCompleteFinalSimState', 'on');
% end
% 
% set_param(strcat(model_name,'/external input'), 'Value', string(external_input))
% 
% % my_input.time = [t_start:0.1:t_end]';
% % my_input.signals(1).values = sin(my_input.time);
% % my_input.signals(1).dimensions = 1;
% t = [t_start:0.1:t_end]'
% u = [sin(t)]
% 
% simOut = sim(model_name, t_range, options);
% time = simOut.simout.time;
% output = simOut.simout.data;
% 
% % save state for beginning of next run
% save_state_previous_end = simOut.save_state_end;
% save('workspace2','save_state_previous_end')
% 
% return_vals = [time'; output'];
% 
% end

%% another experimental

% function return_vals = python_matlab_test(external_input, t_start, t_end, is_first_time)

external_input = 0.0
t_start = 0.0
t_end = 15.0
is_first_time = 1.0


% this is very important; when calling from python, the base workspace and
% current workspace are not the same, but simulink defaults to the base
% workspace. use sim('modelname',[],options) -- https://www.mathworks.com/matlabcentral/answers/92888-how-do-i-run-a-model-in-simulink-from-my-matlab-function-where-my-simulink-parameters-are-defined-in
options = simset('SrcWorkspace','current');

t_range = [t_start, t_end];

model_name = 'python_matlab_simulink_test';

if is_first_time
    open_system(model_name); 
    set_param(model_name, 'LoadInitialState', 'off')
    set_param(model_name, 'SaveFinalState', 'on', 'FinalStateName',...
     'save_state_end','SaveCompleteFinalSimState', 'on')
else
    load('workspace2','save_state_previous_end')
    set_param(model_name, 'LoadInitialState', 'on', 'InitialState',...
              'save_state_previous_end','SaveFinalState', 'on', 'FinalStateName',...
              'save_state_end','SaveCompleteFinalSimState', 'on');
end

set_param(strcat(model_name,'/external input'), 'Value', string(external_input))

% my_input.time = [t_start:0.1:t_end]';
% my_input.signals(1).values = sin(my_input.time);
% my_input.signals(1).dimensions = 1;
t = [t_start:0.1:t_end]'
u = [sin(t)]


sim(model_name, t_range, options);
% simOut = sim(model_name, t_range, options);
% time = simOut.simout.time;
% output = simOut.simout.data;
% 
% % save state for beginning of next run
% save_state_previous_end = simOut.save_state_end;
% save('workspace2','save_state_previous_end')
% 
% return_vals = [time'; output'];

% end



%% experimental

% function return_vals = python_matlab_test()
% 
% model_name = 'python_matlab_simulink_test';
% t1 = 0
% t2 = 10
% 
% % this is very important; when calling from python, the base workspace and
% % current workspace are not the same, but simulink defaults to the base
% % workspace https://www.mathworks.com/matlabcentral/answers/92888-how-do-i-run-a-model-in-simulink-from-my-matlab-function-where-my-simulink-parameters-are-defined-in
% options = simset('SrcWorkspace','current');
% % sim('modelname',[],options)
% 
% % first run
% open_system(model_name);
% set_param(model_name, 'LoadInitialState', 'off')
% set_param(model_name, 'SaveFinalState', 'on', 'FinalStateName',...
%  'my_save_state1','SaveCompleteFinalSimState', 'on')
% external_input = 10;
% set_param(strcat(model_name,'/external input'), 'Value', string(external_input))
% % simOut = sim(model_name, [t1 t2]);
% simOut = sim(model_name, [t1 t2], options);
% time = simOut.simout.time;
% output = simOut.simout.data;
% my_save_state1 = simOut.my_save_state1;
% plot(time,output,'b');
% 
% my_save_state1_namechange = my_save_state1;
% % save('workspace','my_save_state1_namechange')
% % 
% % clear all
% % close all
% % 
% % load('workspace','my_save_state1_namechange')
% % model_name = 'python_matlab_simulink_test';
% % open_system(model_name);
% t2 = 10
% t3 = 20
% 
% 
% % second run
% % set_param(model_name, 'LoadInitialState', 'off')
% % set_param(model_name,'SaveFinalState', 'on', 'FinalStateName',...
% %  'my_save_state2','SaveCompleteFinalSimState', 'on');
% 
% % set_param(model_name, 'LoadInitialState', 'on', 'InitialState', 'my_save_state1_namechange')
% set_param(model_name, 'LoadInitialState', 'on', 'InitialState', 'my_save_state1')
% set_param(model_name, 'SaveFinalState', 'on', 'FinalStateName', 'my_save_state2','SaveCompleteFinalSimState', 'on');
% 
% % set_param(model_name, 'LoadInitialState', 'on', 'InitialState',...
% % 'my_save_state1_namechange','SaveFinalState', 'on', 'FinalStateName',...
% %  'my_save_state2','SaveCompleteFinalSimState', 'on');
% external_input = 20;
% set_param(strcat(model_name,'/external input'), 'Value', string(external_input))
% % simOut = sim(model_name, [t2 t3]);
% simOut = sim(model_name, [t2 t3], options);
% time2 = simOut.simout.time;
% output2 = simOut.simout.data;
% my_save_state2 = simOut.my_save_state2;
% hold on; 
% plot(time2,output2,'g');
% 
% return_vals = 'it worked!';
% disp(return_vals)
% 
% end

%% working

% model_name = 'python_matlab_simulink_test';
% save_State_name1 = 'my_save_state1';
% save_State_name2 = 'my_save_state2';
% t1 = 0;
% t2 = 10;
% t3 = 20;
% t4 = 30;
% 
% % first run
% open_system(model_name);
% set_param(model_name, 'LoadInitialState', 'off')
% set_param(model_name, 'SaveFinalState', 'on', 'FinalStateName',...
%  save_State_name1,'SaveCompleteFinalSimState', 'on')
% external_input = 0;
% set_param(strcat(model_name,'/external input'), 'Value', string(external_input))
% simOut = sim(model_name, [t1 t2]);
% time = simOut.simout.time;
% output = simOut.simout.data;
% my_save_state1 = simOut.my_save_state1;
% plot(time,output,'b');
% 
% % second run
% set_param(model_name, 'LoadInitialState', 'on', 'InitialState',...
% save_State_name1,'SaveFinalState', 'on', 'FinalStateName',...
%  save_State_name2,'SaveCompleteFinalSimState', 'on');
% external_input = -5;
% set_param(strcat(model_name,'/external input'), 'Value', string(external_input))
% simOut = sim(model_name, [t2 t3]);
% time2 = simOut.simout.time;
% output2 = simOut.simout.data;
% my_save_state2 = simOut.my_save_state2;
% hold on; 
% plot(time2,output2,'g');
% 
% % third run
% set_param(model_name, 'LoadInitialState', 'on', 'InitialState',...
% save_State_name2,'SaveFinalState', 'on', 'FinalStateName',...
%  save_State_name1,'SaveCompleteFinalSimState', 'on');
% external_input = 5;
% set_param(strcat(model_name,'/external input'), 'Value', string(external_input))
% simOut = sim(model_name, [t3 t4]);
% time3 = simOut.simout.time;
% output3 = simOut.simout.data;
% hold on; 
% plot(time3,output3,'c');
% 
% 
% 
% set_param(model_name, 'LoadInitialState', 'off');





