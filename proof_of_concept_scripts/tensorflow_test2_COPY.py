# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 07:41:32 2020

@author: Landen
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import LSTM
#import keras2onnx # save model as onnx
#import pickle # pickle model to be used in python again

# important notes: Tensorflow doesn't work with 32-bit python, which is the default
# not sure if Tensorflow works with Anaconda nowadays; didn't try
# there's a weird thing with numpy about to lose backward comparibility in October 2020, so don't update numpy, just let Tensorflow install an older version


# instructions for virtual environments: https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/

#     ***********************************************************
#     *    start the virtual environment and then start idle    *
#     *         C:\Users\Landen\python38\Scripts\activate       *
#     *         python -m idlelib                               *
#     ***********************************************************

# much easier example:

#https://machinelearningmastery.com/5-step-life-cycle-long-short-term-memory-models-keras/
#https://machinelearningmastery.com/reshape-input-data-long-short-term-memory-networks-keras/
#https://machinelearningmastery.com/prepare-univariate-time-series-data-long-short-term-memory-networks/
#https://machinelearningmastery.com/handle-missing-timesteps-sequence-prediction-problems-python/

# 'keras2onnx' best bet for converting to ONNX:
# https://github.com/onnx/keras-onnx

# well, maybe not. Here is how to save it as a .h5 keras format:
# https://www.tensorflow.org/guide/keras/save_and_serialize#keras_h5_format

# axes of variable X:
# [samples, timesteps, features]

# if we need to use python in real-time
# https://www.mathworks.com/matlabcentral/answers/458589-can-i-interact-with-a-simulink-real-time-simulation-from-python
# https://www.mathworks.com/help/matlab/matlab-engine-for-python.html

from_file = True
# pickle_file = 'model.p'
keras_file = 'model_2'
keras_file_h5 = 'model_2.h5';
#onnx_model_file = 'model_1.onnx'
predict_column_units = '(MW)'

#data_df = pd.read_csv('data_1.csv')  # 10000
#data_df = pd.read_csv('test_data2.csv')  # 10000
#data_df = pd.read_csv('test_data3.csv')  # 1000
data_df_list = []
for i in range(1,65):
    data_df_list.append(pd.read_csv('data_'+str(i)+'.csv'))

#prediction_header = 'output'
prediction_header = 'total_power_MW'
num_predictions = 2 # starting with the next timestep
LSTM_size = 57
LSTM_layers = 1
sample_size = 12
sample_increment = 1
epochs = 200
use_early_stopping = True
patience = 5
prediction_normalize_factor = 1
batch_size = 18

samples_array = None

for index, data_df in enumerate(data_df_list):
    if len(data_df) < sample_size + num_predictions + sample_increment:
        continue
    print('reading dataframe number:',index+1)
#    print('df head pre-processing:')
#    print(data_df.head())
    data_df = data_df.drop(['time'], axis=1)
    data_df = data_df.drop(['thermal_efficiency'], axis=1)
    # normalize output
    data_df[prediction_header] = data_df[prediction_header] / prediction_normalize_factor

    # add artificial noise
    for header in list(data_df):
        if header==prediction_header:
            noise = np.random.normal(0, 5, len(data_df[header]))
        else:
            noise = np.random.normal(0, 0.05, len(data_df[header]))
        data_df[header] += noise
    
    df_list = []
    #df_list.append(data_df) # current timestep
    df_list.append(data_df[prediction_header]) # current output
    for i in range(0,num_predictions):
        df_list.append(data_df.shift(-(i+1)).drop([prediction_header], axis=1)) # future inputs
    for i in range(0,num_predictions):
        df_list.append(data_df[prediction_header].shift(-(i+1))) # future outputs
    data_df = pd.concat(df_list, axis=1)
    
    data_df.dropna(inplace=True) # easy way to just get rid of all the groups at the ends with missing values
#    print('df head post-processing:')
#    print(data_df.head())
    data = data_df.to_numpy()
#    print('np array head:')
#    print(data[:5,:])
    
    n = data.shape[0]
    samples_list = [data[i:i+sample_size] for i in range(0,n-sample_size, sample_increment)]
    if samples_array is not None:
        samples_array = np.concatenate([samples_array, np.array(samples_list)],axis=0)
    else:
        samples_array = np.array(samples_list)
#    print('samples shape:',samples_array.shape)

X = samples_array[:,:,:-num_predictions]
y = samples_array[:,-1:,-num_predictions:]

# split train/validation/test:
n = X.shape[0]
train_cutoff = 0.7
validation_cutoff = 0.9
X_train = X[0:int(n*train_cutoff)]
X_val = X[int(n*train_cutoff):int(n*validation_cutoff)]
X_test = X[int(n*validation_cutoff):]

y_train = y[0:int(n*train_cutoff)]
y_val = y[int(n*train_cutoff):int(n*validation_cutoff)]
y_test = y[int(n*validation_cutoff):]

validation_data=(X_val, y_val)

print('X shape:',X.shape)
print('y shape:',y.shape)

input_shape = X.shape[1:]

if not from_file:
    # 1. define network
    model = Sequential()
    model.add(LSTM(LSTM_size, input_shape=input_shape, return_sequences=True))
    for i in range(1,LSTM_layers):
        model.add(LSTM(LSTM_size, return_sequences=True))
    model.add(Dense(num_predictions))

    # 2. compile network
    model.compile(optimizer='adam', loss='mean_squared_error')

    # 3. fit network
    print('fitting network')

    callbacks=None
    if use_early_stopping:
        callbacks = [tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=patience)]
    history = model.fit(X_train, y_train, epochs=epochs, batch_size=batch_size, verbose=2, validation_data=validation_data, callbacks=callbacks)
    
    # pickle the model
    # pickle.dump( model, open( pickle_file, "wb" ) )
    model.save(keras_file)
    model.save(keras_file_h5)
else:
    # unpickle model
    # model = pickle.load( open( pickle_file, "rb" ) )
    model = tf.keras.models.load_model(keras_file)
    
# 4. evaluate network
print('evaluating network')
loss = model.evaluate(X_train, y_train, verbose=0)
print('train loss:',loss)
loss = model.evaluate(X_test, y_test, verbose=0)
print('test loss',loss)

# 5. make predictions
print('making predictions')

predictions = model.predict(X_test, verbose=0)
print('test this:',X_test[0])
print('\ntest results:',y_test[0:12])

# plot multiple predictions
n_test = len(y_test)
increment = min(5,num_predictions)
for i in np.linspace(0,num_predictions-1,increment).astype(int):
    plt.figure()
    plt.plot(range(n_test),y_test[:,0,i] * prediction_normalize_factor,'b.-',label='actual')
    plt.plot(range(n_test),predictions[:,-1,i] * prediction_normalize_factor,'go',label='predicted')
    plt.title('Prediction '+str(i+1)+' minute(s) ahead')
    plt.xlabel('Time (min)')
    plt.ylabel('Power Generated (MW)')
    plt.legend()
    
#plt.show()

# save model as onnx
#onnx_model = keras2onnx.convert_keras(model, model.name)
#keras2onnx.save_model(onnx_model, onnx_model_file)

parity_labels = []
parity_predictions = []
for i in range(num_predictions):
    parity_labels = np.hstack([parity_labels, y_test[:,0,i]])
    parity_predictions = np.hstack([parity_predictions, predictions[:,-1,i] * prediction_normalize_factor])
    # parity_labels.append(y_test[:,0,i])
    # parity_predictions.append(predictions[:,-1,i] * prediction_normalize_factor)

def plot_parity_historgram(test_labels, test_predictions):
    
    plt.rcParams.update({'font.size': 14})
    plt.rc('axes', titlesize=14)
#    plt.figure()
    plt.figure(figsize=(11,4.8)) #6.4,4.8 is default
    
    plt.subplot(121)
    plt.scatter(test_labels, test_predictions, s=5, linewidths = 0.1, edgecolors = 'darkblue')
    plt.xlabel('True values '+predict_column_units)
    plt.ylabel('Predictions '+predict_column_units)
    plt.axis('equal')
    plt.axis('square')
    plt.xlim([0,plt.xlim()[1]])
    plt.ylim([0,plt.ylim()[1]])
    plt.plot([0,max(test_labels)],[0,max(test_labels)],'-g')
    plt.title('Parity Plot of Testing Data')
    
    plt.subplot(122)
#    plt.figure()
    error = test_predictions - test_labels
    plt.hist(error, bins = 25)
    plt.xlabel("Prediction error "+predict_column_units)
    plt.ylabel("Count")
    plt.title('Histogram Testing Data Error')
    
    plt.savefig('temp.png', transparent=True)
    #plt.show()
    
    

plot_parity_historgram(parity_labels, parity_predictions)
plt.show()
