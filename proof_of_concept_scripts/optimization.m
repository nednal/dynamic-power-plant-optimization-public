

% https://www.mathworks.com/matlabcentral/answers/1015-pso-in-simulink#answer_1604
% http://www.cleveralgorithms.com/nature-inspired/swarm/pso.html


% parpool('AttachedFiles',{'load_parameters.m','Simple_plant.slx'}); % to add more files, use a comma inside the brackets


lb = [0];
ub = [10];
rng default  % For reproducibility
nvars = 1;
% options = optimoptions('particleswarm','UseParallel',true);
% options = optimoptions('particleswarm','SwarmSize',4,'MaxTime',2*60,'UseParallel',true);
options = optimoptions('particleswarm','SwarmSize',4,'MaxTime',2*60);
% options = optimoptions('particleswarm','SwarmSize',5,'HybridFcn',@fmincon);
% answer = particleswarm(@obj_function,nvars,lb,ub)
answer = particleswarm(@obj_function,nvars,lb,ub,options)



function value = obj_function(x)

simIn = Simulink.SimulationInput('Simple_plant');
simIn.setBlockParameter('Simple_plant/SOFA2', 'Value', x);
simOut = sim(simIn);
final_temperature = simOut.yout{4}.Values.Data(end);
value = -final_temperature;

end
