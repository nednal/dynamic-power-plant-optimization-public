clear all;
clc

% useful links:
% https://www.mathworks.com/matlabcentral/answers/477196-how-can-i-avoid-transferring-all-the-workspace-variables-to-the-workers-when-using-parsim
% https://www.mathworks.com/help/simulink/slref/rapid-accelerator-simulations-using-parsim.html
% https://www.mathworks.com/matlabcentral/answers/458511-setexternalinput-the-number-of-external-inputs-must-be-equal-to-the-number-of-root-level-input-port

% run with inputs:
data = 2:2:20;
data2 = 1:10;
data_combined = [data; data2]';
time = 1:10;
time = time';
numSims = 4;
mdl = 'parsim_test_model';

numInputs = 2;
% create empty cell for the imports
inports_cellArray = cell(numSims);

for i=1:numSims
    % initialize each cell with an inputDataSet
    inports_cellArray{i} = createInputDataset(mdl);
    for j=1:numInputs
        % the +i makes sure each of the inputs is different
        % .Name retrieves the actual name of the input ports
        inports_cellArray{i}{j}=timeseries(data_combined(:,j)+i,time,'Name',inports_cellArray{i}{j}.Name);
    end
end

% creat array in simulation models
simIn(1:numSims) = Simulink.SimulationInput(mdl);

for i = 1:numSims
    % set external inputs
    simIn(i) = simIn(i).setExternalInput(inports_cellArray{i});
end

% run simulations in parallel
simOut = parsim(simIn);

% plot results to show the parsim took different inputs
figure()
hold on
for i = 1:numSims
    simOut_i = simOut(i);
    t = simOut_i.output_name.Time;
    y = simOut_i.output_name.Data;
    plot(t, y)
end
hold off

figure()
hold on
for i = 1:numSims
    simOut_i = simOut(i);
    t = simOut_i.output_name2.Time;
    y = simOut_i.output_name2.Data;
    plot(t, y)
end
hold off
