
% https://www.mathworks.com/help/matlab/matlab_external/call-user-defined-custom-module.html

% matlab command to change to the version with tensorflow (must run this before the first instance of py is launched)
% pyenv('Version','C:\Users\Landen\python38\Scripts\python.exe')
% run pyversion command to verify that the executable is as shown above
% at the bottom, contains some cmd commands that I used: https://www.tensorflow.org/install/gpu#windows_setup

% had version 2.10.0
% trying version 2.8.0 https://github.com/h5py/h5py/issues/1151
% pip install h5py==2.8.0

word = py.list({'huzzah!'});
result = py.matlab_python_test.test(word)
result = py.matlab_python_test2.test2(word)
result = py.matlab_python_test2.test3(word)
result = py.matlab_python_test2.test4(word)
result = py.matlab_python_test2.test5(word)

%input1 = py.list({1});
%result = py.predict_horizon.test_prediction(input1)

%input2 = py.list({2});
%result = py.predict_horizon.test_prediction(input2)
