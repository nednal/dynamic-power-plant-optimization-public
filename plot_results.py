
import matplotlib.pyplot as plt
import numpy as np

# helper function for plotting optimized results
# use aaa_plot_optimization_results.py for paper figures

def plot_results(return_values_np, predicted_time_array, predicted_fuel_array, predicted_CV_array):
    
    time = return_values_np[:,0]
    CV = return_values_np[:,1]
    SP = return_values_np[:,2]
    thermal_efficiency = return_values_np[:,3]
    total_power_MW = return_values_np[:,4]
    fuel_fraction = return_values_np[:,5]
    n_inj_i = return_values_np[:,6:16]
    n_inj_noBias_i = return_values_np[:,16:26]
    normalized_power = return_values_np[:,26]
    power_SP = return_values_np[:,27]
    
    x_axis_max = max(predicted_time_array)

    plt.subplot(5,1,1)
    plt.plot(time,CV,label='CV')
    plt.plot(time,SP,label='SP')
    plt.plot(predicted_time_array,predicted_CV_array,'--',label='CV predicted')
    plt.xlim([0, x_axis_max])
    plt.legend()

    plt.subplot(5,1,2)
    plt.plot(time,thermal_efficiency,label='thermal_efficiency')
    plt.xlim([0, x_axis_max])
    plt.legend()

    plt.subplot(5,1,3)
    plt.plot(time,fuel_fraction,label='fuel fraction')
    plt.plot(predicted_time_array,predicted_fuel_array,'--',label='fuel fraction predicted')
    plt.xlim([0, x_axis_max])
    plt.legend()
    
    plt.subplot(5,1,4)
    #plt.plot(time,total_power_MW,label='total power')
    plt.plot(time,normalized_power,label='total power')
    plt.plot(time,power_SP,'--',label='load SP')
    plt.xlim([0, x_axis_max])
    plt.legend()

    plt.subplot(5,1,5); 
    for i in range(9):
        if i < 6:
            plt.plot(time,n_inj_i[:,i],'-',label='air inj level #'+str(i+1))
        elif i > 6:
            plt.plot(time,n_inj_i[:,i],'--',label='air inj level #'+str(i+1))
    plt.plot(time,n_inj_noBias_i[:,0],'-',label='unbiased air inj #1-6',linewidth=2)
    plt.plot(time,n_inj_noBias_i[:,7],'--',label='unbiased air inj #8-9',linewidth=2)
    plt.xlim([0, x_axis_max])
    plt.legend()
    plt.savefig('machine_learning_data/aaa_most_recent_results.png', transparent=True)
    plt.show()
