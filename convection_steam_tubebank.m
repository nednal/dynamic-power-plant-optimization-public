function Q_tube_to_steam_i = convection_steam_tubebank(surface_area_i, D, ...
                                                       T_steam_i, T_pipe_i, Pr_steam, ...
                                                       rho_steam_i, v_steam_i, k_steam, mu_steam_i)
% % Fundamentals of Heat and Mass Transfer 7th edition (Bergman, Lavine, Incopera, Dewitt)
% % Equation 8.60

Re_D_i = rho_steam_i .* v_steam_i .* D ./ mu_steam_i; % note: mu = Dynamic Viscosity
Nu_i = 0.023 .* Re_D_i .^ (4/5) .* Pr_steam .^ 0.3; % note: using 0.3 because the pipes should usually be hotter than the steam. 0.4 would be used for the opposite
h_i = Nu_i .* k_steam / D;
Q_tube_to_steam_i = h_i .* surface_area_i .* (T_pipe_i - T_steam_i);
% x = 1; %TEMP

