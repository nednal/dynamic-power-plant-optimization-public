
# from MASTER_python_matlab import plot_results
import pickle
import numpy as np
import matplotlib.pyplot as plt

#file_dynamic = 'machine_learning_data/run_default_load_dynamic_1/aaa_most_recent_results_122.p'
#file_SS      = 'machine_learning_data/run_default_load_SS_1/aaa_most_recent_results_122.p'
#file_base    = 'machine_learning_data/run_default_load_base_1/aaa_most_recent_results_122.p'

move_legend = False

'''
profile = 'fast_down'
title = 'Type I Comparison'
index_D = 1
time_final = '32'
file_dynamic = 'machine_learning_data/'+str(index_D+0)+'_best_'+profile+'_D/aaa_most_recent_results_'+time_final+'.p'
file_SS      = 'machine_learning_data/'+str(index_D+1)+'_best_'+profile+'_SS/aaa_most_recent_results_'+time_final+'.p'
file_base    = 'machine_learning_data/'+str(index_D+2)+'_best_'+profile+'_base/aaa_most_recent_results_'+time_final+'.p'
figure_name = 'O2_comparison_'+profile
'''

'''
profile = 'slow_down'
title = 'Type II Comparison'
index_D = 4
time_final = '62'
file_dynamic = 'machine_learning_data/'+str(index_D+0)+'_best_'+profile+'_D/aaa_most_recent_results_'+time_final+'.p'
file_SS      = 'machine_learning_data/'+str(index_D+1)+'_best_'+profile+'_SS/aaa_most_recent_results_'+time_final+'.p'
file_base    = 'machine_learning_data/'+str(index_D+2)+'_best_'+profile+'_base/aaa_most_recent_results_'+time_final+'.p'
figure_name = 'O2_comparison_'+profile
'''

'''
profile = 'fast_up'
title = 'Type III Comparison'
index_D = 7
time_final = '32'
file_dynamic = 'machine_learning_data/'+str(index_D+0)+'_best_'+profile+'_D/aaa_most_recent_results_'+time_final+'.p'
file_SS      = 'machine_learning_data/'+str(index_D+1)+'_best_'+profile+'_SS/aaa_most_recent_results_'+time_final+'.p'
file_base    = 'machine_learning_data/'+str(index_D+2)+'_best_'+profile+'_base/aaa_most_recent_results_'+time_final+'.p'
figure_name = 'O2_comparison_'+profile
'''


profile = 'slow_up'
title = 'Type IV Comparison'
index_D = 10
time_final = '62'
file_dynamic = 'machine_learning_data/'+str(index_D+0)+'_best_'+profile+'_D/aaa_most_recent_results_'+time_final+'.p'
file_SS      = 'machine_learning_data/'+str(index_D+1)+'_best_'+profile+'_SS/aaa_most_recent_results_'+time_final+'.p'
file_base    = 'machine_learning_data/'+str(index_D+2)+'_best_'+profile+'_base/aaa_most_recent_results_'+time_final+'.p'
figure_name = 'O2_comparison_'+profile


'''
profile = 'W'
title = 'Type V Comparison'
index_D = 13
time_final = '122'
file_dynamic = 'machine_learning_data/'+str(index_D+0)+'_best_'+profile+'_D/aaa_most_recent_results_'+time_final+'.p'
file_SS      = 'machine_learning_data/'+str(index_D+1)+'_best_'+profile+'_SS/aaa_most_recent_results_'+time_final+'.p'
file_base    = 'machine_learning_data/'+str(index_D+2)+'_best_'+profile+'_base/aaa_most_recent_results_'+time_final+'.p'
figure_name = 'O2_comparison_'+profile
move_legend = True
'''


x_loc_legend = 1.025
window_resize = 0.65

parastic_load = 0.1

#def plot_results(return_values_np, predicted_time_array, predicted_fuel_array, predicted_CV_array):
def plot_results(return_values_np_dynamic, return_values_np_SS, return_values_np_base,
                 predicted_time_array_dynamic, predicted_time_array_SS, predicted_time_array_base,
                 predicted_fuel_array_dynamic, predicted_fuel_array_SS, predicted_fuel_array_base,
                 predicted_CV_array_dynamic, predicted_CV_array_SS, predicted_CV_array_base, figure_name):

    time_dynamic = return_values_np_dynamic[:,0]
    CV_dynamic = return_values_np_dynamic[:,1]
    SP_dynamic = return_values_np_dynamic[:,2]
    thermal_efficiency_dynamic = return_values_np_dynamic[:,3] * (1-parastic_load)
    fuel_fraction_dynamic = return_values_np_dynamic[:,5]

    time_SS = return_values_np_SS[:,0]
    CV_SS = return_values_np_SS[:,1]
    thermal_efficiency_SS = return_values_np_SS[:,3] * (1-parastic_load)
    fuel_fraction_SS = return_values_np_SS[:,5]

    time_base = return_values_np_base[:,0]
    CV_base = return_values_np_base[:,1]
    thermal_efficiency_base = return_values_np_base[:,3] * (1-parastic_load)
    fuel_fraction_base = return_values_np_base[:,5]

    print('average Dynamic efficiency:',np.average(thermal_efficiency_dynamic))
    print('average SS efficiency:',np.average(thermal_efficiency_SS))
    print('average Base efficiency:',np.average(thermal_efficiency_base))

    #print('average Dynamic fuel:',np.average(fuel_fraction_dynamic))
    #print('average SS fuel:',np.average(fuel_fraction_SS))
    #print('average Base fuel:',np.average(fuel_fraction_base))

    #def rmse(CV_array):
    #    return np.sqrt(((CV_array - 0.04) ** 2).mean())
    #
    #print('rmse Dynamic CV:',rmse(CV_dynamic))
    #print('rmse SS CV:',rmse(CV_SS))
    #print('rmse Base CV:',rmse(CV_base))

    fig = plt.figure(figsize=(10, 5))
    
    x_axis_max = max(predicted_time_array_dynamic)
    #move_legend
    plt.subplot(2,1,1)
    plt.plot(time_dynamic, CV_dynamic,'b-',label='dynamic')
    plt.plot(time_SS, CV_SS,'g-',label='steady-state')
    plt.plot(time_base, CV_base,'m-',label='baseline')
    plt.plot(predicted_time_array_dynamic, predicted_CV_array_dynamic,'b--',label='dynamic predicted')
    plt.plot(time_dynamic,SP_dynamic,label='SP')
    #plt.plot(predicted_time_array_SS, predicted_fuel_array_SS,'g--',label='steady-state predicted')
    #plt.plot(predicted_time_array_base, predicted_fuel_array_base,'m--',label='baseline predicted')
    plt.xlim([0, x_axis_max])
    plt.ylabel('Excess O$_2$')
    #plt.title('')
    if move_legend:
        plt.subplots_adjust(right=window_resize)
        plt.legend(bbox_to_anchor=(x_loc_legend, 1))
    else:
        plt.legend()
        
    plt.title(title)
    
    plt.subplot(2,1,2)
    plt.plot(time_dynamic, thermal_efficiency_dynamic,'b-',label='dynamic')
    plt.plot(time_SS, thermal_efficiency_SS,'g-',label='steady-state')
    plt.plot(time_base, thermal_efficiency_base,'m-',label='baseline')
    plt.xlim([0, x_axis_max])
    plt.ylabel('thermal efficiency')
    #plt.title('')
    if move_legend:
        plt.subplots_adjust(right=window_resize)
        plt.legend(bbox_to_anchor=(x_loc_legend, 1))
    else:
        plt.legend()

    
    
    plt.savefig('machine_learning_data/'+figure_name+'.png', transparent=True)
    
    plt.show()


print('unpickling dynamic values')
load_values_dynamic = pickle.load( open( file_dynamic, "rb" ) )
return_values_np_dynamic, predicted_fuel_array_dynamic, predicted_CV_array_dynamic = load_values_dynamic
predicted_time_array_dynamic = np.arange(1,len(predicted_fuel_array_dynamic)+1) * 60 # convert to seconds

print('unpickling values SS')
load_values_SS = pickle.load( open( file_SS, "rb" ) )
return_values_np_SS, predicted_fuel_array_SS, predicted_CV_array_SS = load_values_SS
predicted_time_array_SS = np.arange(1,len(predicted_fuel_array_SS)+1) * 60 # convert to seconds

print('unpickling values base')
load_values_base = pickle.load( open( file_base, "rb" ) )
return_values_np_base, predicted_fuel_array_base, predicted_CV_array_base = load_values_base
predicted_time_array_base = np.arange(1,len(predicted_fuel_array_base)+1) * 60 # convert to seconds

print('plotting and saving figure')

plot_results(return_values_np_dynamic, return_values_np_SS, return_values_np_base,
             predicted_time_array_dynamic, predicted_time_array_SS, predicted_time_array_base,
             predicted_fuel_array_dynamic, predicted_fuel_array_SS, predicted_fuel_array_base,
             predicted_CV_array_dynamic, predicted_CV_array_SS, predicted_CV_array_base, figure_name)
    
