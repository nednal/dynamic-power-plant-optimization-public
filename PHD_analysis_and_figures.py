
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

class Analysis:
    '''
    class that handles tearing apart a single metaoptimization csv, all analysis and plots
    
    need to:
    - read in dataframe
    - filter down to the instances that are close to the best objective values
    - further filter down to the instances that are n-best from a paratofront sweep (see excel doc)
    - plot filtered objective values vs iteration counts, with those from the second filter in a different color
    '''
    
    def __init__(self, ML_type, optimizer, objective_cuttoff = 1.0, pareto_cuttoff = 1, start_index_array = None, consolidation_type = 'median', normalization_array = []):
        
        if start_index_array is None:
            file = 'machine_learning_data/metaoptimization/' + optimizer + '_' + ML_type + '_paramater_sweep.csv'
            self.df = pd.read_csv(file)
            
        else: # if start_array is given, average values down
            file = 'machine_learning_data/refined_metaoptimization/' + optimizer + '_' + ML_type + '_refined_paramater_sweep.csv'
            self.df_raw = pd.read_csv(file)
            
            # first find and apply normalization factors (one for each start_index):
            using_given_normalization_array = normalization_array != []
            df_raw_normalized_array = []
            self.obj_best_global_low_array = []
            for index, start_index in enumerate(start_index_array):
                df_subset = self.df_raw[self.df_raw['start_index'] == start_index].copy()
                df_subset = df_subset.reset_index(drop=True)
                np_array_obj_best_global = df_subset['obj_best_global'].to_numpy()
                obj_best_global_low = np.min(np_array_obj_best_global)
                if not using_given_normalization_array:
                    self.obj_best_global_low_array.append(obj_best_global_low)
                else:
                    self.obj_best_global_low_array.append(normalization_array[index])
                df_subset.loc[:,'obj_best_global'] = df_subset.loc[:,'obj_best_global'] / self.obj_best_global_low_array[index]
                df_raw_normalized_array.append(df_subset)
            self.df_raw_normalized = pd.concat(df_raw_normalized_array)
            
            self.df_raw_normalized['parameter_ID'] = ''
            for index, row in self.df_raw_normalized.iterrows():
                if optimizer=='PSO':
                    id_array = ['w', 'c1', 'c2', 'i_max']
                if optimizer=='GA':
                    id_array = ['bit_count', 'm', 'c', 'tournament_size', 'i_max']
                if optimizer=='ACO':
                    id_array = ['Q', 'rho', 'discretizations', 'i_max'] 
                parameter_ID = ''
                for i, id_value in enumerate(id_array):
                    if i != 0:
                        parameter_ID += '-'
                    parameter_ID += str(row[id_value])
                self.df_raw_normalized.loc[index,'parameter_ID'] = parameter_ID
            
            # next consilidate runs using median values for identical sets of parameters:
            parameter_ID_array = self.df_raw_normalized['parameter_ID'].unique()
            df_array = []
            for parameter_ID in parameter_ID_array:
                df_subset = self.df_raw_normalized[self.df_raw_normalized['parameter_ID']==parameter_ID].copy()
                df_subset = df_subset.reset_index(drop=True)
                np_array_obj_best_global = df_subset['obj_best_global'].to_numpy()
                np_array_function_evals = df_subset['function_evals'].to_numpy()

                # find best, worst, and average performance
                obj_best_global_low = np.min(np_array_obj_best_global) 
                obj_best_global_high = np.max(np_array_obj_best_global) 
                obj_best_global_average = np.average(np_array_obj_best_global)
                obj_best_global_median = np.median(np_array_obj_best_global)
                
                function_evals_low = np.min(np_array_function_evals)
                function_evals_high = np.max(np_array_function_evals)
                function_evals_average = np.average(np_array_function_evals)
                function_evals_median = np.median(np_array_function_evals)

                # .loc is end inclusive!!!
                df_row = df_subset.loc[0:0,:].copy()

                # add columns for best performances
                df_row.loc[0:0,'obj_best_global_high'] = obj_best_global_high
                df_row.loc[0:0,'function_evals_high'] = function_evals_high
                # add column for worst performances
                df_row.loc[0:0,'obj_best_global_low'] = obj_best_global_low
                df_row.loc[0:0,'function_evals_low'] = function_evals_low
                # add column for median performances
                df_row.loc[0:0,'obj_best_global_median'] = obj_best_global_median
                df_row.loc[0:0,'function_evals_median'] = function_evals_median
                # add column for average performances
                df_row.loc[0:0,'obj_best_global_average'] = obj_best_global_average
                df_row.loc[0:0,'function_evals_average'] = function_evals_average

                df_array.append(df_row)

            self.df = pd.concat(df_array)
            self.df = self.df.reset_index(drop=True)
            
        self.ML_type = ML_type
        self.optimizer = optimizer
        self.start_index_array = start_index_array
        
        # find first filter
        function_evals = self.df['function_evals'].to_numpy()
        obj_best_global = self.df['obj_best_global'].to_numpy()
        min_obj_value = np.min(obj_best_global)

        filter_1 = obj_best_global <= min_obj_value + objective_cuttoff
        
        # find second filter (use nans for values from the first filter so the dimensions still match up)
        max_filtered_function_evals = np.max(function_evals[filter_1])
        max_filtered_obj_best_global = np.max(obj_best_global[filter_1])
        # normalize by the highest value
        function_evals_normalized = function_evals / max_filtered_function_evals
        if start_index_array is None: #only normalize if it isn't already
            obj_best_global_normalized = obj_best_global / max_filtered_obj_best_global
        else:
            obj_best_global_normalized = obj_best_global
        # nans for the first filter
        function_evals_normalized[np.logical_not(filter_1)] = np.nan
        obj_best_global_normalized[np.logical_not(filter_1)] = np.nan
        # create matrix of weighted sums
        stepsize = 0.0001
        weight = np.tile(np.arange(0,1+stepsize,stepsize),[len(function_evals_normalized),1])
        weight_cols = np.shape(weight)[1]
        weighted_sums = (np.transpose(np.tile(function_evals_normalized,[weight_cols,1])) * weight
                       + np.transpose(np.tile(obj_best_global_normalized,[weight_cols,1])) * (1-weight) )
        
        pareto_1_maxtix = np.zeros_like(weighted_sums,dtype=bool)
        pareto_n_maxtix = np.zeros_like(weighted_sums,dtype=bool)
        for col in range(weight_cols):
            for n in range(pareto_cuttoff):
                row = np.argpartition(weighted_sums[:,col],n)[n]
                if n == 0:
                    pareto_1_maxtix[row,col] = True
                pareto_n_maxtix[row,col] = True
        filter_2 = np.any(pareto_n_maxtix, axis=1)
        filter_3 = np.any(pareto_1_maxtix, axis=1)
        
        # add filters as columns
        print('\nMachine Learning Model:', self.ML_type)
        print('Optimizer:', self.optimizer)
        print('Total entries:', len(self.df))
        print('Entries that hit the mark:', np.count_nonzero(filter_1))
        print('With depth of',pareto_cuttoff,'entries in pareto front:', np.count_nonzero(filter_2))
        print('With depth of',1,'entries in pareto front:', np.count_nonzero(filter_3))

##        for parameter_ID in parameter_ID_array:
##                print(parameter_ID)
        
        self.df['filter_1'] = filter_1
        self.df['filter_2'] = filter_2
        self.df['filter_3'] = filter_3

    def get_pareto_front_parameters(self, output=True):
        df_copy = self.df.copy()
        df_copy = df_copy.drop(columns=['filter_1','filter_2','filter_3'])
        if self.start_index_array is not None:
            df_copy = df_copy.drop(columns=['run_counter','w','c1','c2','i_max','start_index',
                                            'iterations','obj_best_global_median','function_evals_median'])
            df_copy = df_copy.drop(columns=['function_evals_average','function_evals_low',
                                            'function_evals_high'])
        indexes_in_pareto_1 = []
        indexes_in_pareto_n = []
        for i in range(len(self.df)):
            if self.df['filter_2'][i]:
                indexes_in_pareto_n.append(i)
            if self.df['filter_3'][i]:
                indexes_in_pareto_1.append(i)
        filtered_and_sorted_1_df = df_copy.loc[indexes_in_pareto_1].sort_values('obj_best_global')
        filtered_and_sorted_n_df = df_copy.loc[indexes_in_pareto_n].sort_values('obj_best_global')
        if output:
            print('Pareto with depth n:')
            print(filtered_and_sorted_n_df)
            print('Pareto with depth 1:')
            print(filtered_and_sorted_1_df)
        return filtered_and_sorted_n_df, filtered_and_sorted_1_df

    def get_plot_values(self):
        function_evals = self.df['function_evals'].to_numpy()
        obj_best_global = self.df['obj_best_global'].to_numpy()
        filter_1 = self.df['filter_1'].to_numpy()
        filter_2 = self.df['filter_2'].to_numpy()
        filter_3 = self.df['filter_3'].to_numpy()
##        print('pareto front size:',np.sum(filter_3))
        
        filter_1_not2 = np.logical_and(filter_1, np.logical_not(filter_2))
        filter_2_not3 = np.logical_and(filter_2, np.logical_not(filter_3))

        function_evals_pareto_n = function_evals[filter_2_not3]
        obj_best_global_pareto_n = obj_best_global[filter_2_not3]

        function_evals_pareto_1 = function_evals[filter_3]
        obj_best_global_pareto_1 = obj_best_global[filter_3]

        function_evals_interior = function_evals[filter_1_not2]
        obj_best_global_interior = obj_best_global[filter_1_not2]

        # sort the pareto front by obj_best_global
        sort_indexes = np.argsort(obj_best_global_pareto_1)
        function_evals_pareto_1 = function_evals_pareto_1[sort_indexes]
        obj_best_global_pareto_1 = obj_best_global_pareto_1[sort_indexes]
        
        return function_evals_pareto_1, obj_best_global_pareto_1, function_evals_pareto_n, obj_best_global_pareto_n, function_evals_interior, obj_best_global_interior

    def plot(self, connect_pareto_front = True, show_plot=False, filename=None, x_limit = None, y_limit = None,title_add_on=''):
        function_evals_pareto_1, obj_best_global_pareto_1, function_evals_pareto_n, obj_best_global_pareto_n, function_evals_interior, obj_best_global_interior = self.get_plot_values()
        
##        plt.figure()
        plt.figure(figsize=(3, 3)) # measured in inches
        plt.title(self.ML_type + ' with ' + self.optimizer + title_add_on)
        plt.semilogy(obj_best_global_interior, function_evals_interior, 'b.',label='low performance')
        plt.semilogy(obj_best_global_pareto_n, function_evals_pareto_n, 'g.',label='high performance')
        if connect_pareto_front:
            style = 'g-o'
        else:
            style = 'g.'
        plt.semilogy(obj_best_global_pareto_1, function_evals_pareto_1, style,label='Pareto front')
        plt.xlabel('objective function value')
        plt.ylabel('function evaluations (log scale)')
        plt.tight_layout()
##        plt.legend()
        if x_limit is not None:
            plt.xlim(x_limit)
            x_plot_values = np.concatenate((obj_best_global_interior,
                                            obj_best_global_pareto_n,
                                            obj_best_global_pareto_1),axis=0)
            if np.any(x_plot_values < x_limit[0]):
                print('>>>> x lower limit of', x_limit[0],'failed to capture',np.min(x_plot_values))
            if np.any(x_plot_values > x_limit[1]):
                print('>>>> x upper limit of', x_limit[1],'failed to capture',np.max(x_plot_values))
        if y_limit is not None:
            plt.ylim(y_limit)
            y_plot_values = np.concatenate((function_evals_interior,
                                            function_evals_pareto_n,
                                            function_evals_pareto_1),axis=0)
            if np.any(y_plot_values < y_limit[0]):
                print('>>>> y lower limit of', y_limit[0],'failed to capture',np.min(y_plot_values))
            if np.any(y_plot_values > y_limit[1]):
                print('>>>> y upper limit of', y_limit[1],'failed to capture',np.max(y_plot_values))
        if filename is not None:
            plt.savefig('PHD_figures/'+filename, transparent=True)
            plt.savefig('PHD_figures/'+filename+'.eps', format='eps',bbox_inches="tight")
        if show_plot:
            plt.show()

def compare_pareto_fronts(analysis_array, label_array, show_plot=False, title_add_on='', filename = 'pareto_front_comparison', x_limit = None, y_limit = None):
    color_array = ['b-*','g-*','c-*',
                   'b--^','g--^','c--^',
                   'b-.o','g-.o','c-.o']
    plt.figure()
    plt.title('Comparison of Pareto Fronts'+title_add_on)
    for i, analysis in enumerate(analysis_array):
        function_evals_pareto, obj_best_global_pareto, _, _, _, _ = analysis.get_plot_values()
        plt.semilogy(obj_best_global_pareto, function_evals_pareto, color_array[i], label=label_array[i])
    plt.xlabel('objective function value')
    plt.ylabel('function evaluations')
    plt.legend()
    plt.savefig('PHD_figures/'+filename, transparent=True)
    plt.savefig('PHD_figures/'+filename+'.eps', format='eps',bbox_inches="tight")
    if show_plot:
        plt.show()

def write_refined_pareto_CSV(analysis_array):
    '''
    creates CSV file for the combined pareto fronts, only need to do this once
    '''
    # parings of parameters (PSO,GA,ACO): (w,bit_count,Q); (c1,m,rho); (c2,c,discretizations); (-,tournament_size,-); (i_max,i_max,i_max)

    # create headers
    file = 'machine_learning_data/refined_metaoptimization/combined_pareto_fronts.csv'
    with open(file,'w') as fd:
        fd.write('ML_type,optimizer,w-bit_count-Q,c1-m-rho,c2-c-discretizations,N/A-tournament_size-N/A,i_max,obj_best_global_median,function_evals_median\n')

    for analysis in analysis_array:
        for index, row in analysis.df.iterrows():
            if row['filter_3']:
                myCsvRow_array = []
                myCsvRow_array.append(analysis.ML_type)
                myCsvRow_array.append(analysis.optimizer)
                if analysis.optimizer=='PSO':
                    myCsvRow_array.append(row['w'])
                    myCsvRow_array.append(row['c1'])
                    myCsvRow_array.append(row['c2'])
                    myCsvRow_array.append('')
                elif analysis.optimizer=='GA':
                    myCsvRow_array.append(row['bit_count'])
                    myCsvRow_array.append(row['m'])
                    myCsvRow_array.append(row['c'])
                    myCsvRow_array.append(row['tournament_size'])
                elif analysis.optimizer=='ACO':
                    myCsvRow_array.append(row['Q'])
                    myCsvRow_array.append(row['rho'])
                    myCsvRow_array.append(row['discretizations'])
                    myCsvRow_array.append('')
                myCsvRow_array.append(row['i_max'])
                myCsvRow_array.append(row['obj_best_global'])
                myCsvRow_array.append(row['function_evals'])

                # build CSV row string
                myCsvRow = ''
                for CSV_col_i, CSV_col in enumerate(myCsvRow_array):
                    if CSV_col_i != 0:
                        myCsvRow += ","
                    myCsvRow += str(CSV_col)
                myCsvRow += "\n"
                # write sring, append
                with open(file,'a') as fd:
                    fd.write(myCsvRow)
##                print(myCsvRow)

def plot_closed_loop_pareto_front(model_array, optimizer_array, show_plot=False, filename='Closed_loop_pareto_front', include_numbering = False):
##    color_array = ['b-*','g-*','c-*',
##                   'b--^','g--^','c--^',
##                   'b-.o','g-.o','c-.o']
    color_array = ['b*','g*','c*',
                   'b^','g^','c^',
                   'bo','go','co']
    file = 'machine_learning_data/refined_metaoptimization/combined_pareto_fronts_analysis.csv'
    df = pd.read_csv(file)
    plt.figure()
    plt.title('Comparison of Closed-loop Performance')
    color_index = 0
    function_evals_combined_array = []
    thermal_efficiency_combined_array = []
    for model in model_array:
        for optimizer in optimizer_array:
            df_subset = df[(df['ML_type'] == model) & (df['optimizer'] == optimizer)]
            function_evals_array = df_subset['function evals closed-loop'].to_numpy()
            thermal_efficiency_array = df_subset['thermal efficiency'].to_numpy()
            sort_indexes = np.argsort(thermal_efficiency_array)
            function_evals_array = function_evals_array[sort_indexes]
            thermal_efficiency_array = thermal_efficiency_array[sort_indexes]
            model_ = model
            if model=='SimpleRNN':
                model_ = 'RNN'
            
            plt.semilogy(thermal_efficiency_array, function_evals_array, color_array[color_index], label=model_+'-'+optimizer)
            color_index += 1
            function_evals_combined_array += list(function_evals_array)
            thermal_efficiency_combined_array += list(thermal_efficiency_array)
    # find combined pareto front
    function_evals_combined_array = np.array(function_evals_combined_array)
    thermal_efficiency_combined_array = np.array(thermal_efficiency_combined_array)
    sort_indexes = np.argsort(thermal_efficiency_combined_array)
    function_evals_combined_array = function_evals_combined_array[sort_indexes]
    thermal_efficiency_combined_array = thermal_efficiency_combined_array[sort_indexes]

    function_evals_combined_array_log = np.log(function_evals_combined_array)
    thermal_efficiency_combined_array_negative = -1 * thermal_efficiency_combined_array # since we want the maximum
    
    stepsize = 0.0001
    weight = np.tile(np.arange(0,1+stepsize,stepsize),[len(function_evals_combined_array_log),1])
    weight_cols = np.shape(weight)[1]
    weighted_sums = (np.transpose(np.tile(function_evals_combined_array_log,[weight_cols,1])) * weight
                   + np.transpose(np.tile(thermal_efficiency_combined_array_negative,[weight_cols,1])) * (1-weight) )
    
    pareto_maxtix = np.zeros_like(weighted_sums,dtype=bool)
    for col in range(weight_cols):
        row = np.argpartition(weighted_sums[:,col],0)[0]
        pareto_maxtix[row,col] = True
    pareto_filter = np.any(pareto_maxtix, axis=1)
    print('members of pareto:',np.sum(pareto_filter))
    print('members of function_evals_combined_array:',len(function_evals_combined_array))

    plt.semilogy(thermal_efficiency_combined_array[pareto_filter],
                 function_evals_combined_array[pareto_filter],
                 'k-', label='combined Pareto')
    
    if include_numbering:
        numbering_x_array = thermal_efficiency_combined_array[pareto_filter]
        numbering_y_array = function_evals_combined_array[pareto_filter]
        for i in range(len(numbering_x_array)):
            plt.text(numbering_x_array[i] * 1.0005, numbering_y_array[i] *0.95, chr(i+ord('A')),
                     fontsize=10, horizontalalignment='left', verticalalignment='top')
        
    plt.xlabel('Average thermal efficiency')
    plt.ylabel('total function evaluations')
    plt.legend()
    plt.savefig('PHD_figures/'+filename, transparent=True)
    plt.savefig('PHD_figures/'+filename+'.eps', format='eps',bbox_inches="tight")
    if show_plot:
        plt.show()
    

########################################################################
########################## parameters ##################################
########################################################################

##plot_type = 'individual pareto fronts'
plot_type = 'plot combined pareto fronts'
##plot_type = 'plot individual refined pareto fronts'
##plot_type = 'plot combined refined pareto fronts'
##plot_type = 'plot closed-loop pareto fronts' # remember these values must be manually entered into the excel doc and saved as a csv

start_index_array = [200, 300, 400, 500, 600, 700, 800, 900, 1000]
normalization_array = [6.8885527, 8.972397, 7.708514699999999, 7.631056, 4.9661446, 9.159432, 6.022367, 8.599263, 9.136154]

##consolidation_type = 'min'
##objective_cuttoff = 0.2

##consolidation_type = 'average'
##objective_cuttoff = 2.0

consolidation_type = 'median'
objective_cuttoff = 0.2

print_analysis_parameters = False

generate_normalization_array = False # only works for the 'plot combined refined pareto fronts' case
write_refined_pareto_CSV_file = False # only works for the 'plot combined refined pareto fronts' case
use_same_axis_limits = False

LSTM_pareto_depth_dic      = {'PSO':5,'GA':5,'ACO':7}
GRU_pareto_depth_dic       = {'PSO':4,'GA':7,'ACO':8}
SimpleRNN_pareto_depth_dic = {'PSO':4,'GA':6,'ACO':9}


include_numbering = True # only for 'plot closed-loop pareto fronts', adds numbering to points

# define necessary variables:
pareto_depth_dic_dic = {'LSTM':LSTM_pareto_depth_dic,
                        'GRU':GRU_pareto_depth_dic,
                        'SimpleRNN':SimpleRNN_pareto_depth_dic}
model_array     = ['GRU', 'LSTM', 'SimpleRNN']
optimizer_array = ['PSO', 'GA', 'ACO']
x_limit = None
y_limit = None

########################################################################
#################### plot individual pareto fronts #####################
########################################################################

if plot_type == 'individual pareto fronts':
    if use_same_axis_limits:
        x_limit = [7.5,9.5]
        y_limit = [10**1,10**5]
    for model in model_array:
        for optimizer in optimizer_array:
            analysis = Analysis(model, optimizer, pareto_cuttoff = pareto_depth_dic_dic[model][optimizer])
            if print_analysis_parameters:
                analysis.get_pareto_front_parameters()
            analysis.plot(filename='pareto_front_'+model+'_'+optimizer,x_limit = x_limit, y_limit = y_limit)

########################################################################
################## plot combined pareto fronts #########################
########################################################################

if plot_type == 'plot combined pareto fronts':
##    pareto_cuttoff = 1
    analysis_array = []
    label_array = []
    for model in model_array:
        for optimizer in optimizer_array:
            analysis_array.append(Analysis(model, optimizer))
            model_ = model
            if model=='SimpleRNN':
                model_ = 'RNN'
            label_array.append(model_ + '-' + optimizer)
    compare_pareto_fronts(analysis_array, label_array)

########################################################################
################# plot individual refined pareto fronts ################
########################################################################

if plot_type == 'plot individual refined pareto fronts':
    if use_same_axis_limits:
        x_limit = [1,1.15]
        y_limit = [80,1500]
    for model in model_array:
        for optimizer in optimizer_array:
            analysis = Analysis(model, optimizer, pareto_cuttoff = 1,
                                objective_cuttoff = objective_cuttoff,
                                start_index_array = start_index_array,
                                consolidation_type = consolidation_type,
                                normalization_array = normalization_array)
            if print_analysis_parameters:
                analysis.get_pareto_front_parameters()
            analysis.plot(filename='pareto_front_'+model+'_'+optimizer+'_refined',
                          x_limit = x_limit, y_limit = y_limit,title_add_on=', Median')

########################################################################
############### plot combined refined pareto fronts ####################
########################################################################

if plot_type == 'plot combined refined pareto fronts':
    
    if generate_normalization_array:
        normalization_array = []
    
    pareto_cuttoff = 1
    analysis_array = []
    label_array = []
    for model in model_array:
        for optimizer in optimizer_array:
            analysis_array.append(Analysis(model, optimizer, pareto_cuttoff = 1,
                                           objective_cuttoff = objective_cuttoff,
                                           start_index_array = start_index_array,
                                           consolidation_type = consolidation_type,
                                           normalization_array = normalization_array))
            model_ = model
            if model=='SimpleRNN':
                model_ = 'RNN'
            label_array.append(model_ + '-' + optimizer)

    if generate_normalization_array:
        # get normalization array (make sure to provide None as the input)
        obj_best_global_low_array_array = []
        print('')
        for analysis in analysis_array:
            print('For',analysis.ML_type,'and',analysis.optimizer)
            print(analysis.obj_best_global_low_array)
            obj_best_global_low_array_array.append(analysis.obj_best_global_low_array)
        np_obj_best_global_low_array_array = np.array(obj_best_global_low_array_array)
        normalization_array = []
        for col in range(np.shape(np_obj_best_global_low_array_array)[1]):
            normalization_array.append(np.min(np_obj_best_global_low_array_array[:,col]))
        print('copy this into the code:',normalization_array)

    compare_pareto_fronts(analysis_array, label_array,
                          filename = 'pareto_front_comparison_refined',
                          title_add_on=', Median Performance')
##    # quick check
##    total_entries = 0
##    entries_array = []
##    total_median_entries = 0
##    median_entries_array = []
##    expected_median_entries_array = []
##    for analysis in analysis_array:
##        total_entries += len(analysis.df_raw)
##        entries_array.append(len(analysis.df_raw))
##        total_median_entries += len(analysis.df)
##        median_entries_array.append(len(analysis.df))
##        expected_median_entries_array.append(int(len(analysis.df_raw) / 9))
##        print(analysis.ML_type,analysis.optimizer)
##    print('entries array:',entries_array)
##    print('median entries array:',median_entries_array)
##    print('expected median entries:',expected_median_entries_array)
##    print('total entires:',total_entries)
##    print('total median entries:',total_median_entries)


    if write_refined_pareto_CSV_file:
        # only need to run this once
        write_refined_pareto_CSV(analysis_array)

if plot_type == 'plot closed-loop pareto fronts':
    plot_closed_loop_pareto_front(model_array, optimizer_array, include_numbering=include_numbering)

plt.show()


