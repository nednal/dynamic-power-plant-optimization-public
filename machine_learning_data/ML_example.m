load JapaneseVowelsNet

net.Layers

[XTest,YTest] = japaneseVowelsTestData;

X = XTest{94};
numTimeSteps = size(X,2);
for i = 1:numTimeSteps
    v = X(:,i);
    [net,score] = predictAndUpdateState(net,v);
    scores(:,i) = score;
end

classNames = string(net.Layers(end).Classes);
figure
lines = plot(scores');
xlim([1 numTimeSteps])
legend("Class " + classNames,'Location','northwest')
xlabel("Time Step")
ylabel("Score")
title("Prediction Scores Over Time Steps")

trueLabel = YTest(94)

lines(trueLabel).LineWidth = 3;

figure
bar(score)
title("Final Prediction Scores")
xlabel("Class")
ylabel("Score")

