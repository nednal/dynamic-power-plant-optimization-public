
# important notes: Tensorflow doesn't work with 32-bit python, which is the default
# not sure if Tensorflow works with Anaconda nowadays; didn't try
# there's a weird thing with numpy about to lose backward comparibility in October 2020, so don't update numpy, just let Tensorflow install an older version


# instructions for virtual environments: https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/

#     ***********************************************************
#     *    start the virtual environment and then start idle    *
#     *         C:\Users\Landen\python38\Scripts\activate       *
#     *         python -m idlelib                               *
#     ***********************************************************

# tutorial: https://www.tensorflow.org/tutorials/structured_data/time_series#single_step_models


print('hello world')

import os
import datetime

import matplotlib.pyplot as plt
print('matplotlib')
import numpy as np
print('numpy')
import pandas as pd
print('pandas')
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
print('tensorflow')


 
class WindowGenerator():
    def __init__(self, input_width, label_width, shift,
                train_df, val_df, test_df,
                label_columns=None):
        # Store the raw data.
        self.train_df = train_df
        self.val_df = val_df
        self.test_df = test_df
        
        # Work out the label column indices.
        self.label_columns = label_columns
        
        if label_columns is not None:
          self.label_columns_indices = {name: i for i, name in
                                        enumerate(label_columns)}
        self.column_indices = {name: i for i, name in
                               enumerate(train_df.columns)}
        
        # Work out the window parameters.
        self.input_width = input_width
        self.label_width = label_width
        self.shift = shift
        
        self.total_window_size = input_width + shift
        
        self.input_slice = slice(0, input_width)
        self.input_indices = np.arange(self.total_window_size)[self.input_slice]
        
        self.label_start = self.total_window_size - self.label_width
        self.labels_slice = slice(self.label_start, None)
        self.label_indices = np.arange(self.total_window_size)[self.labels_slice]
    
    def split_window(self, features):
        inputs = features[:, self.input_slice, :]
        labels = features[:, self.labels_slice, :]
        if self.label_columns is not None:
            labels = tf.stack(
                        [labels[:, :, self.column_indices[name]] for name in self.label_columns],
                        axis=-1)
    
        # Slicing doesn't preserve static shape information, so set the shapes
        # manually. This way the `tf.data.Datasets` are easier to inspect.
        inputs.set_shape([None, self.input_width, None])
        labels.set_shape([None, self.label_width, None])
        
        return inputs, labels
    
    def make_dataset(self, data):
        data = np.array(data, dtype=np.float32)
        ds = tf.keras.preprocessing.timeseries_dataset_from_array(
              data=data,
              targets=None,
              sequence_length=self.total_window_size,
              sequence_stride=1,
              shuffle=True,
              batch_size=32,)
    
        ds = ds.map(self.split_window)
        return ds
        
    @property
    def train(self):
        return self.make_dataset(self.train_df)
    
    @property
    def val(self):
        return self.make_dataset(self.val_df)
    
    @property
    def test(self):
        return self.make_dataset(self.test_df)
    
    @property
    def example(self):
        """Get and cache an example batch of `inputs, labels` for plotting."""
        result = getattr(self, '_example', None)
        if result is None:
            # No example batch was found, so get one from the `.train` dataset
            result = next(iter(self.train))
            # And cache it for next time
            self._example = result
        return result

    def plot(self, model=None, plot_col='output', max_subplots=3):
        inputs, labels = self.example
        plt.figure(figsize=(12, 8))
        plot_col_index = self.column_indices[plot_col]
        max_n = min(max_subplots, len(inputs))
        for n in range(max_n):
            plt.subplot(3, 1, n+1)
            plt.ylabel(f'{plot_col} [normed]')
            plt.plot(self.input_indices, inputs[n, :, plot_col_index],
                     label='Inputs', marker='.', zorder=-10)
            
            if self.label_columns:
                label_col_index = self.label_columns_indices.get(plot_col, None)
            else:
                label_col_index = plot_col_index
            if label_col_index is None:
                continue
    
            plt.scatter(self.label_indices, labels[n, :, label_col_index],
                        edgecolors='k', label='Labels', c='#2ca02c', s=64)
            if model is not None:
              predictions = model(inputs)
              plt.scatter(self.label_indices, predictions[n, :, label_col_index],
                          marker='X', edgecolors='k', label='Predictions',
                          c='#ff7f0e', s=64)
        
            if n == 0:
                plt.legend()
    
        plt.xlabel('Time [h]')
        plt.show()

    def __repr__(self):
        return '\n'.join([
            f'Total window size: {self.total_window_size}',
            f'Input indices: {self.input_indices}',
            f'Label indices: {self.label_indices}',
            f'Label column name(s): {self.label_columns}'])


def split_dataset(df, train_cutoff = 0.7, validation_cutoff = 0.9):
    n = len(df)
    train_df = df[0:int(n*train_cutoff)]
    val_df = df[int(n*train_cutoff):int(n*validation_cutoff)]
    test_df = df[int(n*validation_cutoff):]
    return train_df, val_df, test_df

MAX_EPOCHS = 20
def compile_and_fit(model, window, patience=2):
#    early_stopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss',
#                                                      patience=patience,
#                                                      mode='min')

    model.compile(loss=tf.losses.MeanSquaredError(),
                  optimizer=tf.optimizers.Adam(),
                  metrics=[tf.metrics.MeanAbsoluteError()])

    history = model.fit(window.train, epochs=MAX_EPOCHS,
                        validation_data=window.val) #, callbacks=[early_stopping]
    return history


#zip_path = tf.keras.utils.get_file(
#    origin='https://storage.googleapis.com/tensorflow/tf-keras-datasets/jena_climate_2009_2016.csv.zip',
#    fname='jena_climate_2009_2016.csv.zip',
#    extract=True)
#csv_path, _ = os.path.splitext(zip_path)
#df = pd.read_csv(csv_path)
## slice [start:stop:step], starting from index 5 take every 6th record.
#df = df[5::6]
#
#date_time = pd.to_datetime(df.pop('Date Time'), format='%d.%m.%Y %H:%M:%S')
#data_df = df
#data_df['output'] = data_df['T (degC)']


#data_df = pd.read_csv('test_data2.csv')  
data_df = pd.read_csv('test_data3.csv')  




#inputs_array = data_df.to_numpy()[:,1:-1]
#labels_array = data_df.to_numpy()[:,-1]
#num_inputs = 2
#
#
#w1 = WindowGenerator(input_width=10, label_width=1, shift=5, train_df = data_df,
#                     label_columns=['output'])
#
##print(w1)

train_df, val_df, test_df = split_dataset(data_df)

#print('-'*50)
#print(train_df)
#print('-'*50)
#print(val_df)
#print('-'*50)
#print(test_df)


#single_step_window = WindowGenerator(
#    input_width=1, label_width=1, shift=1, train_df=train_df, val_df=val_df, test_df=test_df,
#    label_columns=['output'])

#print(single_step_window)

#for example_inputs, example_labels in single_step_window.train.take(1):
#    print(f'Inputs shape (batch, time, features): {example_inputs.shape}')
#    print(f'Labels shape (batch, time, features): {example_labels.shape}')

wide_window = WindowGenerator(
    input_width=24, label_width=24, shift=1, train_df=train_df, val_df=val_df, test_df=test_df,
    label_columns=['output'])

print(wide_window)

#linear = tf.keras.Sequential([
#    tf.keras.layers.Dense(units=1)
#])


#print('Input shape:', single_step_window.example[0].shape)
#print('Output shape:', linear(single_step_window.example[0]).shape)


#history = compile_and_fit(linear, single_step_window)
#print('linear:', linear.evaluate(single_step_window.val))
#print('linear:', linear.evaluate(single_step_window.test, verbose=0))


#dense = tf.keras.Sequential([
#    tf.keras.layers.Dense(units=64, activation='relu'),
#    tf.keras.layers.Dense(units=64, activation='relu'),
#    tf.keras.layers.Dense(units=1)
#])

    
#history = compile_and_fit(dense, single_step_window)
#print('dense:', dense.evaluate(single_step_window.val))
#print('dense:', dense.evaluate(single_step_window.test, verbose=0))


#CONV_WIDTH = 3
#conv_window = WindowGenerator(
#    input_width=CONV_WIDTH,
#    label_width=1,
#    shift=1,
#    train_df=train_df, val_df=val_df, test_df=test_df,
#    label_columns=['output'])
#
#print(conv_window)

#conv_model = tf.keras.Sequential([
#    tf.keras.layers.Conv1D(filters=32,
#                           kernel_size=(CONV_WIDTH,),
#                           activation='relu'),
#    tf.keras.layers.Dense(units=32, activation='relu'),
#    tf.keras.layers.Dense(units=1),
#])
    
#print("Conv model on 'conv_window'")
#print('Input shape:', conv_window.example[0].shape)
#print('Output shape:', conv_model(conv_window.example[0]).shape)

#history = compile_and_fit(conv_model, conv_window)
#print('Conv:', conv_model.evaluate(conv_window.val))
#print('Conv:', conv_model.evaluate(conv_window.test, verbose=0))


#LABEL_WIDTH = 24
#INPUT_WIDTH = LABEL_WIDTH + (CONV_WIDTH - 1)
#wide_conv_window = WindowGenerator(
#    input_width=INPUT_WIDTH,
#    label_width=LABEL_WIDTH,
#    shift=1,
#    train_df=train_df, val_df=val_df, test_df=test_df,
#    label_columns=['output'])


#wide_conv_window.plot(conv_model)

very_wide_window = WindowGenerator(
    input_width=100, label_width=100, shift=1, 
    train_df=train_df, val_df=val_df, test_df=test_df,
    label_columns=['output'])


lstm_model = tf.keras.models.Sequential([
    # Shape [batch, time, features] => [batch, time, lstm_units]
#    tf.keras.layers.Dense(64),
#    tf.keras.layers.LSTM(32, return_sequences=True),
    tf.keras.layers.LSTM(150, return_sequences=True),
#    tf.keras.layers.LSTM(32, return_sequences=True),
#    tf.keras.layers.Dense(64),
    # Shape => [batch, time, features]
    tf.keras.layers.Dense(units=1)
])

#history = compile_and_fit(lstm_model, wide_window)
#print('LSTM:', lstm_model.evaluate(wide_window.val))
#print('LSTM:', lstm_model.evaluate(wide_window.test, verbose=0))
#wide_window.plot(lstm_model)

#history = compile_and_fit(lstm_model, very_wide_window)
#print('LSTM:', lstm_model.evaluate(very_wide_window.val))
#print('LSTM:', lstm_model.evaluate(very_wide_window.test, verbose=0))
#very_wide_window.plot(lstm_model)
#very_wide_window.plot(lstm_model, plot_col='input_1')



    
    
    

single_step_window = WindowGenerator(
    # `WindowGenerator` returns all features as labels if you 
    # don't set the `label_columns` argument.
    input_width=1, label_width=1, shift=1,
    train_df=train_df, val_df=val_df, test_df=test_df)

wide_window = WindowGenerator(
    input_width=24, label_width=24, shift=1,
    train_df=train_df, val_df=val_df, test_df=test_df)

for example_inputs, example_labels in wide_window.train.take(1):
    print(f'Inputs shape (batch, time, features): {example_inputs.shape}')
    print(f'Labels shape (batch, time, features): {example_labels.shape}')











#
#
##help(layers.LSTM)
#
#
#def build_model():
#    
#    model = tf.keras.models.Sequential([
#        # Shape [batch, time, features] => [batch, time, lstm_units]
#        tf.keras.layers.LSTM(10, return_sequences=True),
#        # Shape => [batch, time, features]
#        tf.keras.layers.Dense(units=1)
#    ])
#    
#    optimizer = tf.keras.optimizers.RMSprop(0.001)
#    
#    model.compile(loss='mean_squared_error',
#        optimizer=optimizer,
#        metrics=['mean_absolute_error', 'mean_squared_error']
#    )
#    
#    
#    return model
#
#model = build_model()
#
#model.summary()
#
#EPOCHS = 1000


#print('built lstm_model')



#print('build second lstm_model')

print('complete')

