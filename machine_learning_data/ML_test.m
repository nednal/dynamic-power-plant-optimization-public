
% https://www.mathworks.com/matlabcentral/fileexchange/67296-deep-learning-toolbox-converter-for-onnx-model-format
% https://www.mathworks.com/help/deeplearning/ref/importonnxnetwork.html
% https://www.mathworks.com/help/deeplearning/ref/importkeraslayers.html
% https://www.mathworks.com/help/deeplearning/ref/predict.html

% ONNX format:
% modelfile = 'model_1.onnx';
% 
% % [samples, timesteps, features]
% % net = importONNXNetwork(modelfile,'OutputLayerType','regression');
% lgraph = importONNXLayers(modelfile,'OutputLayerType','regression');
% indPlaceholderLayers = findPlaceholderLayers(lgraph);
% numel(indPlaceholderLayers)
% params = importONNXFunction(modelfile,'shufflenetFcn');

% h5 keras format:
modelfile_h5 = 'model_1.h5';

% net = importKerasLayers(modelfile_h5);
mynet = importKerasLayers(modelfile_h5,'ImportWeights',true);

% mynet2 = importKerasNetwork(modelfile_h5) 
%If a network in modelfile has multiple inputs, then you cannot specify the input sizes using this argument. Use importKerasLayers instead. importKerasLayers inserts placeholder layers for the inputs. After importing, you can find and replace the placeholder layers by using findPlaceholderLayers and replaceLayer, respectively. 
% https://www.mathworks.com/help/deeplearning/ref/importkerasnetwork.html


% mynet = assembleNetwork(mynet) %https://www.mathworks.com/help/deeplearning/ref/assemblenetwork.html

sample_size = 12;
num_predictions = 1;
sample_increment = 1;

data = csvread('data_64.csv',1,0);

inputs = data(:,2:12);
outputs = data(:,13);

raw_timestep_count = size(inputs,1);
raw_feature_count = size(inputs,2);
timesteps = raw_timestep_count - num_predictions;
features = raw_feature_count * num_predictions + 1;

X = zeros(timesteps, sample_size, features);
y = zeros(timesteps, 1, num_predictions);

for timestep_i=1:raw_timestep_count-sample_size
    for sample_i = 1:sample_size
        % current output
        X(timestep_i,sample_i,1) = outputs(timestep_i + sample_size-1);
        for prediction_i=1:num_predictions
            % future inputs
            start_index = 1+(num_predictions-1)*raw_feature_count;
            end_index = start_index + raw_feature_count - 1;
            X(timestep_i,sample_i,start_index:end_index) = inputs(timestep_i+prediction_i,:);
        end
    end
end

% future outputs
for prediction_i=1:num_predictions
    y(:,1,prediction_i) = outputs(prediction_i:prediction_i-1+timesteps);
end

% convert to cell arrays for matlab
X_cell = cell(timesteps,1);
for i=1:timesteps
    X_cell(i) = {squeeze(X(i,:,:))};
end

% X_predict = X_cell{1};
% numTimeSteps = size(X_predict,2);
% for i = 1:numTimeSteps
%     v = X_predict(:,i);
%     [mynet,score] = predictAndUpdateState(mynet,v);
%     scores(:,i) = score;
% end

% predict(params, X_cell)
% predictAndUpdateState(net, X_cell)
% net = resetState(net)