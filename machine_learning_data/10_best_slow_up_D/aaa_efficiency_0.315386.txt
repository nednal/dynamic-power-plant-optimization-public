opt_type: dynamic
set_profile: up slow
run_on_Watson: True
use_full_load_switch: False
NN_versions: machine_learning_data/model_fuel_10_v12 and machine_learning_data/model_CV_10_v12
use_SS_SP_penalty: True
