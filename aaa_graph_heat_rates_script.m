
% save('aaa_heat_rate_comparison_data_1_to_8',    'time_1_array',...
%                                             'time_2_array',...
%                                             'time_3_array',...
%                                             'time_4_array',...
%                                             'time_5_array',...
%                                             'time_6_array',...
%                                             'time_7_array',...
%                                             'time_8_array',...
%                                             'HR_1_array',...
%                                             'HR_2_array',...
%                                             'HR_3_array',...
%                                             'HR_4_array',...
%                                             'HR_5_array',...
%                                             'HR_6_array',...
%                                             'HR_7_array',...
%                                             'HR_8_array',...
%                                             'CV_1_array',...
%                                             'CV_2_array',...
%                                             'CV_3_array',...
%                                             'CV_4_array',...
%                                             'CV_5_array',...
%                                             'CV_6_array',...
%                                             'CV_7_array',...
%                                             'CV_8_array') 

% save('aaa_heat_rate_comparison_data_9_to_11',   'time_9_array',...
%                                             'time_10_array',...
%                                             'time_11_array',...
%                                             'HR_9_array',...
%                                             'HR_10_array',...
%                                             'HR_11_array',...
%                                             'CV_9_array',...
%                                             'CV_10_array',...
%                                             'CV_11_array') 

% load('aaa_heat_rate_comparison_data_1_to_8',    'time_1_array',...
%                                             'time_2_array',...
%                                             'time_3_array',...
%                                             'time_4_array',...
%                                             'time_5_array',...
%                                             'time_6_array',...
%                                             'time_7_array',...
%                                             'time_8_array',...
%                                             'HR_1_array',...
%                                             'HR_2_array',...
%                                             'HR_3_array',...
%                                             'HR_4_array',...
%                                             'HR_5_array',...
%                                             'HR_6_array',...
%                                             'HR_7_array',...
%                                             'HR_8_array',...
%                                             'CV_1_array',...
%                                             'CV_2_array',...
%                                             'CV_3_array',...
%                                             'CV_4_array',...
%                                             'CV_5_array',...
%                                             'CV_6_array',...
%                                             'CV_7_array',...
%                                             'CV_8_array') 

                                        
% load('aaa_heat_rate_comparison_data_9_to_11',   'time_9_array',...
%                                             'time_10_array',...
%                                             'time_11_array',...
%                                             'HR_9_array',...
%                                             'HR_10_array',...
%                                             'HR_11_array',...
%                                             'CV_9_array',...
%                                             'CV_10_array',...
%                                             'CV_11_array') 

%% extra data and rename it:
% time_1_array = timeseries_SOFAs.time(:);
% HR_1_array = timeseries_heat_rate.data(:);
% CV_1_array = timeseries_CV.data(:);
% 
% time_2_array = timeseries_SOFAs.time(:);
% HR_2_array = timeseries_heat_rate.data(:);
% CV_2_array = timeseries_CV.data(:);

% time_3_array = timeseries_SOFAs.time(:);
% HR_3_array = timeseries_heat_rate.data(:);
% CV_3_array = timeseries_CV.data(:);
% 
% time_4_array = timeseries_SOFAs.time(:);
% HR_4_array = timeseries_heat_rate.data(:);
% CV_4_array = timeseries_CV.data(:);
% 
% time_5_array = timeseries_SOFAs.time(:);
% HR_5_array = timeseries_heat_rate.data(:);
% CV_5_array = timeseries_CV.data(:);
% 
% time_6_array = timeseries_SOFAs.time(:);
% HR_6_array = timeseries_heat_rate.data(:);
% CV_6_array = timeseries_CV.data(:);
% 
% time_7_array = timeseries_SOFAs.time(:);
% HR_7_array = timeseries_heat_rate.data(:);
% CV_7_array = timeseries_CV.data(:);

% time_8_array = timeseries_SOFAs.time(:);
% HR_8_array = timeseries_heat_rate.data(:);
% CV_8_array = timeseries_CV.data(:);

% time_9_array = timeseries_SOFAs.time(:);
% HR_9_array = timeseries_heat_rate.data(:);
% CV_9_array = timeseries_CV.data(:);

% ramping for 1-9:
% time	coal_signal
% 0	0.5
% 2000	0.5
% 4000	1
% 4001	1
% 8000	1
% 8001	1
% 10000	0.5
% 10001	0.5
% 14000	0.5

time_10_array = timeseries_SOFAs.time(:);
HR_10_array = timeseries_heat_rate.data(:);
CV_10_array = timeseries_CV.data(:);

% time_11_array = timeseries_SOFAs.time(:);
% HR_11_array = timeseries_heat_rate.data(:);
% CV_11_array = timeseries_CV.data(:);

% ramping for 10-11:
% time	coal_signal
% 0	0.5
% 2000	0.5
% 3000	1
% 3001	1
% 8000	1
% 8001	1
% 9000	0.5
% 9001	0.5
% 14000	0.5


%% key
% 1: normal heat rate ramp. no changes
% 2: heat rate ramp. added oscillating heat loss terms
% 3: heat rate ramp. added oscillating heat loss terms and freezing the term after at its peak
% 4: heat rate ramp. added oscillating heat loss terms and freezing the term after at its valley
% 5: heat rate ramp. added oscillating heat loss terms with higher range and freezing the term after at it's valley
% 6: normal heat rate ramp but excess O2 has been set to 0.06
% 7: normal heat rate ramp but excess O2 has been set to 0.02
% 8: normal heat rate ramp. near perfect control (removed delays from air but not steam
% 9: normal heat rate ramp. added SP 1% increase during ramp up
% 10: normal heat rate ramp at double speed. near perfrect control
% 11: normal heat rate ramp at double speed. added SP 2% increase during ramp up

figure()
subplot(2,1,1);
hold on 
plot(time_1_array,HR_1_array,'DisplayName','case 1')
% plot(time_2_array,HR_2_array,'DisplayName','case 2')
% plot(time_3_array,HR_3_array,'DisplayName','case 3')
% plot(time_4_array,HR_4_array,'DisplayName','case 4')
% plot(time_5_array,HR_5_array,'DisplayName','case 5')
% plot(time_6_array,HR_6_array,'DisplayName','case 6')
% plot(time_7_array,HR_7_array,'DisplayName','case 7')
plot(time_8_array,HR_8_array,'DisplayName','case 8')
plot(time_9_array,HR_9_array,'DisplayName','case 9')
% plot(timeseries_SOFAs.time(:),timeseries_heat_rate.data(:),'DisplayName','case current')
legend()
hold off 

subplot(2,1,2); 
hold on 
plot(time_1_array,CV_1_array,'DisplayName','case 1')
% plot(time_2_array,CV_2_array,'DisplayName','case 2')
% plot(time_3_array,CV_3_array,'DisplayName','case 3')
% plot(time_4_array,CV_4_array,'DisplayName','case 4')
% plot(time_5_array,CV_5_array,'DisplayName','case 5')
% plot(time_6_array,CV_6_array,'DisplayName','case 6')
% plot(time_7_array,CV_7_array,'DisplayName','case 7')
plot(time_8_array,CV_8_array,'DisplayName','case 8')
plot(time_9_array,CV_9_array,'DisplayName','case 9')
% plot(timeseries_SOFAs.time(:),timeseries_CV.data(:),'DisplayName','case current')
legend()
hold off

figure()
subplot(2,1,1);
hold on 
plot(time_10_array,HR_10_array,'DisplayName','case 10')
plot(time_11_array,HR_11_array,'DisplayName','case 11')
% plot(timeseries_SOFAs.time(:),timeseries_heat_rate.data(:),'DisplayName','case current')
legend()
hold off 

subplot(2,1,2); 
hold on 
plot(time_10_array,CV_10_array,'DisplayName','case 10')
plot(time_11_array,CV_11_array,'DisplayName','case 11')
% plot(timeseries_SOFAs.time(:),timeseries_CV.data(:),'DisplayName','case current')
legend()
hold off

% integrate heat rate during ramp down

% HR_integral_1_ramp_down = trapz(time_1_array(181:229),HR_1_array(181:229))
% HR_integral_8_ramp_down = trapz(time_8_array(200:244),HR_8_array(200:244))
% HR_avg_1_ramp_down = HR_integral_1_ramp_down / (time_1_array(229) - time_1_array(181))
% HR_avg_8_ramp_down = HR_integral_8_ramp_down / (time_8_array(244) - time_8_array(200))
% difference = HR_avg_8_ramp_down - HR_avg_1_ramp_down