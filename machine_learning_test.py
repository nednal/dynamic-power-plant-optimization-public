
# instructions for virtual environments: https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/

# remember, Anaconda doesn't play nice with TensorFlow, so use a separate virtual environment:
# in anaconda prompt to: 

# run the following:
#    C:\Users\Landen\Desktop\python_virtual_environment\mypython\Scripts\activate
# to deactivate, just run:
#    deactivate

# this script is located in the following folder:
#   cd C:\Users\Landen\Google_Drive\Personal_Files\Research_UofU\Pacificorp_Dynamic_Optimization\Dyn_Opt_repo


#from __future__ import absolute_import, division, print_function, unicode_literals

#import pathlib

#https://machinelearningmastery.com/handle-missing-timesteps-sequence-prediction-problems-python/
#https://www.programcreek.com/python/example/127073/tensorflow.keras.layers.GRU
#https://www.tensorflow.org/api_docs/python/tf/keras/layers/GRU

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
#import seaborn as sns

#https://www.tensorflow.org/guide/keras/rnn
#https://www.tensorflow.org/api_docs/python/tf/keras/layers/GRU
#https://www.programcreek.com/python/example/127073/tensorflow.keras.layers.GRU
#https://machinelearningmastery.com/handle-missing-timesteps-sequence-prediction-problems-python/
 
#import tensorflow as tf
#from tensorflow import keras
#from tensorflow.keras import layers

#print(tf.__version__)

dataset_path = 'machine_learning_data/test_data.csv'

column_names = ['time','input_1','input_2','output']

raw_dataset = pd.read_csv(dataset_path, skipinitialspace=True)

#data = raw_dataset.values
data = raw_dataset.to_numpy()


def build_model():
    model = keras.Sequential([
        layers.Dense(64, activation=tf.nn.relu, input_shape=[len(train_dataset.keys())]),
        layers.Dense(64, activation=tf.nn.relu),
        layers.Dense(1)
        ])
    
    lstm_model = tf.keras.models.Sequential([
        # Shape [batch, time, features] => [batch, time, lstm_units]
        tf.keras.layers.LSTM(32, return_sequences=True),
        # Shape => [batch, time, features]
        tf.keras.layers.Dense(units=1)
        ])

  optimizer = tf.keras.optimizers.RMSprop(0.001)

  model.compile(loss='mean_squared_error',
                optimizer=optimizer,
                metrics=['mean_absolute_error', 'mean_squared_error'])
  return model

model = build_model()

model.summary()





print('complete!')





