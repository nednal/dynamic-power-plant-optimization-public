import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

plant_file = 'machine_learning_data/dynamic_vallidation_plant.csv'
simulation_file = 'machine_learning_data/dynamic_vallidation_simulated.csv'


plant_data = pd.read_csv(plant_file)
simulation_data = pd.read_csv(simulation_file)

plt.figure()
plt.subplot(2,1,1)
plt.plot(plant_data['time'], plant_data['fuel'],'k-',label='fuel')
plt.plot(plant_data['time'], plant_data['power'],'g-',label='power')
##plt.xlabel('time (min)')
plt.ylabel('Normalized Plant Data')
plt.xlim([0,40])
plt.ylim([0.5,1.15])
plt.legend()
plt.title('Plant Data vs Simulation Data during ramp up')

plt.subplot(2,1,2)
plt.plot(simulation_data['time'], simulation_data['fuel'],'k-',label='fuel')
plt.plot(simulation_data['time'], simulation_data['power'],'g-',label='power')
##plt.plot(simulation_data['time'], simulation_data['power'],'g-',label='power')
plt.xlabel('time (min)')
plt.ylabel('Normalized Simulation Data')
plt.xlim([0,40])
plt.ylim([0.5,1.15])
plt.legend()

plt.savefig('vallidation_comparison.png', transparent=True)
plt.savefig('vallidation_comparison.eps', format='eps',bbox_inches="tight")


plt.show()


##################################################
# added another version for a paper revision

plt.figure()
plt.subplot(2,1,1)
plt.plot(plant_data['time'], plant_data['fuel'],'k-',label='plant')
plt.plot(simulation_data['time'], simulation_data['fuel'],'k--',label='simulation')
##plt.xlabel('time (min)')
plt.ylabel('Normalized Fuel Usage Data')
plt.xlim([0,40])
plt.ylim([0.5,1.15])
plt.legend()
plt.title('Plant Data vs Simulation Data during ramp up')

plt.subplot(2,1,2)
plt.plot(plant_data['time'], plant_data['power'],'g-',label='plant')
plt.plot(simulation_data['time'], simulation_data['power'],'g--',label='simulation')
##plt.plot(simulation_data['time'], simulation_data['power'],'g-',label='power')
plt.xlabel('time (min)')
plt.ylabel('Normalized Plant Power Data')
plt.xlim([0,40])
plt.ylim([0.5,1.15])
plt.legend()

plt.savefig('vallidation_comparison2.png', transparent=True)
plt.savefig('vallidation_comparison2.eps', format='eps',bbox_inches="tight")


plt.show()