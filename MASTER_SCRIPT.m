%% helpful links:
% timeout and StopTime (simulation time) options: https://www.mathworks.com/matlabcentral/answers/373719-is-there-a-timeout-option-with-parsim
% timout option and CaptureErrors options (for sim) https://www.mathworks.com/help/simulink/slref/sim.html

clear;
load_parameters;

tic

%% can be changed:
begin_index = 1; % can increase this to skip ones that have been completed
end_index = 64;
num_sims_per_batch = 8;

%% do not change:
rng(12345) % seed random number generator so this data is reproducable
model_name = 'Simple_plant_2019b';
% create array of simulation models
simIn(1:num_sims_per_batch) = Simulink.SimulationInput(model_name);
current_batch_size = 0;
num_inputs = 9;

% loop through every index each time so RNG is the same. Just skip the completed simulations based on begin_index
for current_index = 1:end_index
    
    % initialize time and input cell arrays
    time_array_cellArray = cell(num_inputs);
    model_input_array_cellArray = cell(num_inputs);
    
    %% define air biases and load parameters for the time range (air bias ~> -1; 0.25 ~< load <= 1)
    % this seciton will probably be a bunch of if statments for different index ranges
    
%     if mod(current_index,3) == 1
%         load_time_array = 60*[0;1;10;20;30;40;50;60];
%         load_array = [1;1;0.5;0.5;1;1;0.5;0.5];
%         bias_time_array = [0,1];
%         bias_array = [0,0];
%         time_array_cellArray{1} = load_time_array;
%         model_input_array_cellArray{1} = load_array;
%         for i=2:9
%             time_array_cellArray{i} = bias_time_array;
%             model_input_array_cellArray{i} = bias_array;
%         end
%         use_full_load_switch = 1; % must be 0 for half load or 1 for full load to start. Should also start with all biases at 0.
%         StopTime = 60; % number of simulation seconds
%         TimeOut = 60^2; % maximum number of real world seconds for running the simulation
%     
%     elseif mod(current_index,3) == 2
%         load_time_array = 60*[0;1;10;20;30;40;50;60;70;80];
%         load_array = [1;1;0.5;0.5;1;1;0.5;0.5;1;1];
%         bias_time_array = [0,1];
%         bias_array = [0,0];
%         time_array_cellArray{1} = load_time_array;
%         model_input_array_cellArray{1} = load_array;
%         for i=2:9
%             time_array_cellArray{i} = bias_time_array;
%             model_input_array_cellArray{i} = bias_array;
%         end
%         use_full_load_switch = 1; % must be 0 for half load or 1 for full load to start. Should also start with all biases at 0.
%         StopTime = 80; % number of simulation seconds
%         TimeOut = 60^2; % maximum number of real world seconds for running the simulation
% 
%     elseif mod(current_index,3) == 0
%         load_time_array = 60*[0;1;10;20;30;40;50;60];
%         load_array = [1;1;0.5;0.5;1;1;0.5;0.5];
%         bias_time_array = load_time_array;
%         bias_array_2 = [0,0,-0.1,-0.1,0.1,0.1,-0.1,-0.1];
%         bias_array_3 = [0,0,0.1,0.1,-0.1,-0.1,0.1,0.1];
%         bias_array_4 = [0,0,-0.2,-0.2,0.2,0.2,-0.2,-0.2];
%         bias_array_5 = [0,0,0.2,0.2,-0.2,-0.2,0.2,0.2];
%         bias_array_6789 = [0,0,0,0,0,0,0,0];
%         
%         time_array_cellArray{1} = load_time_array;
%         model_input_array_cellArray{1} = load_array;
%         time_array_cellArray{2} = bias_time_array;
%         model_input_array_cellArray{2} = bias_array_2;
%         time_array_cellArray{3} = bias_time_array;
%         model_input_array_cellArray{3} = bias_array_3;
%         time_array_cellArray{4} = bias_time_array;
%         model_input_array_cellArray{4} = bias_array_4;
%         time_array_cellArray{5} = bias_time_array;
%         model_input_array_cellArray{5} = bias_array_5;
%         
%         for i=6:9
%             time_array_cellArray{i} = bias_time_array;
%             model_input_array_cellArray{i} = bias_array_6789;
%         end
%         
%         use_full_load_switch = 1; % must be 0 for half load or 1 for full load to start. Should also start with all biases at 0.
%         StopTime = 60; % number of simulation seconds
%         TimeOut = 60^2; % maximum number of real world seconds for running the simulation
%     end


    if current_index <= inf
        % rules: changes occur every 15 minutes (15*60 seconds)
        % time: 15min,  30min,  45min,  60min,  75min,  90min,  105min, 120min, 135min, 150min
        % load: value1, (skip), value2, (skip), (skip), value2, (skip), value3, (skip), (skip), value3, (skip), value4, (skip), (skip), value4
        % air:  value1, value1, value2, value2, value3, value3, value4, value4, value5, value6, ... 
        num_steps = 12*8; % nuumber of steps to go through above pattern (12 = 3 hrs, 12*8 = 24 hrs)
        timestep_size = 15*60;
        StopTime = num_steps*timestep_size; % number of simulation seconds
        use_full_load_switch = 1; % must be 0 for half load or 1 for full load to start. Should also start with all biases at 0.
        TimeOut = 6*60^2; % maximum number of real world seconds for running the simulation
        
        % start at full load, zero offset and give simulation 1 minute to find it's bearings
        % note: columns in bias_matrix indicate different air injectors
        load_time_array = 60*[0;1];
        load_array = [1;1];
        bias_time_array = load_time_array;
        bias_matrix = zeros([2,8]);
        load_ub = 1;
        load_lb = 0.25;
        bias_ub = 0.5;
        bias_lb = -0.5;
        load_range = load_ub - load_lb;
        bias_range = bias_ub - bias_lb;
        
        % get inital random values for step 1 before starting pattern.
        load_time_array = vertcat([load_time_array;timestep_size]);
        load_array = vertcat([load_array; load_lb + load_range * rand ]);
        bias_time_array = vertcat([bias_time_array;timestep_size]);
        bias_matrix = vertcat([bias_matrix; bias_lb + bias_range .* rand([1,8]) ]);
        
        for timestep=2:num_steps-2 % first step is defined above, last two steps will be to reach a steady state
            % load is on 5-step repeat (see pattern above):
            if mod(timestep,5)==4
                % new value
                load_time_array = vertcat([load_time_array;timestep*timestep_size]);
                load_array = vertcat([load_array; load_lb + load_range * rand ]);
            elseif mod(timestep,5)==2
                % hold previous value
                load_time_array = vertcat([load_time_array;timestep*timestep_size]);
                load_array = vertcat([load_array;load_array(end)]);
            end
            % biases are alternating new values and holding
            if mod(timestep,2)==1
                % new value
                bias_time_array = vertcat([bias_time_array;timestep*timestep_size]);
                bias_matrix = vertcat([bias_matrix; bias_lb + bias_range .* rand([1,8]) ]);
            elseif mod(timestep,2)==0
                % hold previous value
                bias_time_array = vertcat([bias_time_array;timestep*timestep_size]);
                bias_matrix = vertcat([bias_matrix;bias_matrix(end,:)]);
            end
        end
        % add last two timesteps: both full load, no bias values
        last_two_timesteps = [(num_steps-1)*timestep_size; num_steps*timestep_size];
        load_time_array = vertcat([load_time_array;last_two_timesteps]);
        load_array = vertcat([load_array;[1;1]]);
        bias_time_array = vertcat([bias_time_array;last_two_timesteps]);
        bias_matrix = vertcat([bias_matrix;zeros([2,8])]);
        
        % load time and input arrays into cell arrays
        time_array_cellArray{1} = load_time_array;
        model_input_array_cellArray{1} = load_array;
        for i=2:9
            time_array_cellArray{i} = bias_time_array;
            model_input_array_cellArray{i} = bias_matrix(:,i-1);
        end
    end
    
    
    
    
    
    %% add to simIn array if we've arrived at the begin_index
    if current_index >= begin_index
%         try
        % create sim object and insert into array
        current_batch_size = current_batch_size +1;
        simIn(current_batch_size) = setup_simulation(model_name, time_array_cellArray, model_input_array_cellArray, use_full_load_switch, StopTime, TimeOut);

        %% run and record the batch if we either have a full batch or are on the last iteration
        if current_batch_size == num_sims_per_batch || current_index == end_index
            % truncate the simIn array to for the final batch
            simIn = simIn(1:current_batch_size);

            % run a batch of parsim 
            simOut = parsim(simIn, 'ShowProgress', 'on');

            % record everything in csvs
            for i=1:current_batch_size
                index_number = i+current_index-current_batch_size;
                simulation = simOut(i);
                save(strcat('simulation_results/',string(index_number)), 'simulation')
            end

            % reset for next batch
            current_batch_size = 0;
            simIn(1:num_sims_per_batch) = Simulink.SimulationInput(model_name);
            end
%         catch ME
%             disp('current batch failed. Error:')
%             disp(ME.identifier)
%         end
    end
    
end

toc

function simIn = setup_simulation(model_name, time_array_cellArray, model_input_array_cellArray, use_full_load_switch, StopTime, TimeOut)
    numInputs = length(model_input_array_cellArray);
    
    % initialize an inputDataSet to get the names of the inputs
    inports = createInputDataset(model_name);
    
    % input the provided input arrays as timeseries
    for j=1:numInputs
        % the +i makes sure each of the inputs is different
        % .Name retrieves the actual name of the input ports
        inports{j} = timeseries(model_input_array_cellArray{j}, time_array_cellArray{j}, 'Name', inports{j}.Name);
    end
    
    % create simIn object to run with sim() or parsim()
    simIn = Simulink.SimulationInput(model_name);
    simIn = simIn.setModelParameter('StopTime', string(StopTime)); % Set total simulation time to StopTime
    simIn = simIn.setModelParameter('TimeOut', TimeOut); % Set time out to TimeOut, in seconds
    simIn = simIn.setModelParameter('CaptureErrors', 'on'); % should capture errors into the simulation object
    simIn = simIn.setBlockParameter(strcat(model_name,'/Use full load, switch'), 'Value', string(use_full_load_switch));
    simIn = simIn.setExternalInput(inports);
    
end