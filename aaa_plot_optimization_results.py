
# from MASTER_python_matlab import plot_results
import pickle
import numpy as np
import matplotlib.pyplot as plt

files = []
files.append('machine_learning_data/13_best_W_D/aaa_most_recent_results_122.p')
files.append('machine_learning_data/14_best_W_SS/aaa_most_recent_results_122.p')
files.append('machine_learning_data/15_best_W_base/aaa_most_recent_results_122.p')

titles = ['Profile Type V, Dynamic Optimization',
          'Profile Type V, Steady-state Optimization',
          'Profile Type V, Baseline, No Optimization']

filenames = ['control_W_D',
             'control_W_SS',
             'control_W_baseline']

optimized_array = [True, True, False]
show_O2_predicted_array = [True, False, False]
show_fuel_predicted_array = [True, False, False]

def plot_results(return_values_np, predicted_time_array, predicted_fuel_array, predicted_CV_array, title, filename, optimized, show_O2_predicted, show_fuel_predicted):

    air_norm_factor = 2000

    x_loc_legend = 1.025
    
    time = return_values_np[:,0]
    
    CV = return_values_np[:,1]
    SP = return_values_np[:,2]
    thermal_efficiency = return_values_np[:,3]
    total_power_MW = return_values_np[:,4]
    fuel_fraction = return_values_np[:,5]
    n_inj_i = return_values_np[:,6:16] / air_norm_factor
    n_inj_noBias_i = return_values_np[:,16:26] / air_norm_factor
    normalized_power = return_values_np[:,26]
    power_SP = return_values_np[:,27]
    
    # convert to minutes
    time = time / 60
    predicted_time_array = predicted_time_array / 60
    
    x_axis_max = max(predicted_time_array)
    
    fig = plt.figure(figsize=(10, 8))
    #plt.subplots_adjust(right=0.70, bottom=0.15)
    plt.subplots_adjust(right=0.70)

    plt.subplot(5,1,1)
    plt.plot(time,CV,label='Actual')
    plt.plot(time,SP,label='SP')
    if show_O2_predicted:
        plt.plot(predicted_time_array,predicted_CV_array,'--',label='predicted')
    plt.xlim([0, x_axis_max])
    plt.legend(bbox_to_anchor=(x_loc_legend, 1))
    plt.ylabel('Excess O$_2$')
    plt.gca().set_xticklabels([])
    plt.title(title)

    plt.subplot(5,1,2)
    plt.plot(time,fuel_fraction,label='actual')
    if show_fuel_predicted:
        plt.plot(predicted_time_array,predicted_fuel_array,'--',label='predicted')
    plt.xlim([0, x_axis_max])
    if show_fuel_predicted:
        plt.legend(bbox_to_anchor=(x_loc_legend, 1))
    plt.ylabel('Fuel Fraction')
    plt.gca().set_xticklabels([])
    
    plt.subplot(5,1,3)
    #plt.plot(time,total_power_MW,label='total power')
    plt.plot(time,normalized_power,label='actual')
    plt.plot(time,power_SP,'--',label='SP')
    plt.xlim([0, x_axis_max])
    plt.legend(bbox_to_anchor=(x_loc_legend, 1))
    plt.ylabel('Power Fraction')
    plt.gca().set_xticklabels([])

    plt.subplot(5,1,4)
    plt.plot(time,thermal_efficiency)#,label='thermal_efficiency')
    plt.xlim([0, x_axis_max])
    #plt.legend(bbox_to_anchor=(1.05, 1))
    plt.ylabel('Thermal Efficiency')
    plt.gca().set_xticklabels([])

    plt.subplot(5,1,5);
    if optimized:
        for i in range(9):
            if i < 6:
                plt.plot(time,n_inj_i[:,i],'-',label='#'+str(i+1))
            elif i > 6:
                plt.plot(time,n_inj_i[:,i],'--',label='#'+str(i+1))
        plt.plot(time,n_inj_noBias_i[:,0],'-',label='no offset #1-6',linewidth=2.5)
        plt.plot(time,n_inj_noBias_i[:,7],'--',label='no offset #8-9',linewidth=2.5)
        plt.legend(bbox_to_anchor=(x_loc_legend, 1.05), ncol=2)
    else:
        plt.plot(time,n_inj_noBias_i[:,0],'-',label='#1-6',linewidth=2.5)
        plt.plot(time,n_inj_noBias_i[:,7],'--',label='#8-9',linewidth=2.5)
        plt.legend(bbox_to_anchor=(x_loc_legend, 1.05))
    plt.xlim([0, x_axis_max])
    plt.xlabel('time (min)')
    plt.ylabel('air inj')
    
    #plt.legend(bbox_to_anchor=(1.25, -0.05), ncol=4)
    plt.savefig('machine_learning_data/'+filename, transparent=True)
    plt.savefig('machine_learning_data/'+filename+'.eps', format='eps',bbox_inches="tight")
    

for index, file in enumerate(files):
    title = titles[index]
    filename = filenames[index]
    optimized = optimized_array[index]
    show_O2_predicted = show_O2_predicted_array[index]
    show_fuel_predicted = show_fuel_predicted_array[index]
    
    print('unpickling values')
    load_values = pickle.load( open( file, "rb" ) )
    return_values_np, predicted_fuel_array, predicted_CV_array = load_values

    print('plotting and saving figure')
    predicted_time_array = np.arange(1,len(predicted_fuel_array)+1) * 60 # convert to seconds
    plot_results(return_values_np, predicted_time_array, predicted_fuel_array, predicted_CV_array, title, filename, optimized, show_O2_predicted, show_fuel_predicted)
    
plt.show()
