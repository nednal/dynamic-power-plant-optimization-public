%% helpful links:
% timeout and StopTime (simulation time) options: https://www.mathworks.com/matlabcentral/answers/373719-is-there-a-timeout-option-with-parsim
% timout option and CaptureErrors options (for sim) https://www.mathworks.com/help/simulink/slref/sim.html

% test it out from command line:
% python_matlab_closed_loop_control([0; 60; 9*60; 15*60], [1; 1; 0.75; 0.75], zeros(4, 8), 1, 1, 'Simple_plant')
% SS, full load:
% python_matlab_closed_loop_control([0; 3*60], [1; 1], zeros(2, 8), 1, 1, 'Simple_plant')

function return_values = python_matlab_closed_loop_control(time_array, load_array, bias_matrix, is_first_time, use_full_load_switch, model_name, run_number)
tic
% t and u are the default simulink input variables for time and inputs
t = time_array;
start_time = t(1);
u = zeros([size(t,1) 9]);
u(:,1) = load_array;
u(:,2:end) = bias_matrix;

% disp('let us begin!')

load_parameters

StartTime = time_array(1);
StopTime = time_array(end);

% model_name = 'Simple_plant';
% model_name = 'Simple_plant_2019b';
% use_full_load_switch = 1;
if use_full_load_switch
    use_full_load_switch_logical = 1;
else
    use_full_load_switch_logical = 0;
end

options = simset('SrcWorkspace','current');
t_range = [StartTime, StopTime];

if is_first_time
    open_system(model_name); 
    set_param(model_name, 'LoadInitialState', 'off')
    set_param(model_name,...
        'SaveFinalState', 'on',...
        'FinalStateName','save_state_end',...
        'SaveCompleteFinalSimState', 'on')
else
    load('zz_saved_state','save_state_previous_end')
    set_param(model_name, ...
        'LoadInitialState', 'on',...
        'InitialState','save_state_previous_end',...
        'SaveFinalState', 'on',...
        'FinalStateName','save_state_end',...
        'SaveCompleteFinalSimState', 'on');
end


set_param(strcat(model_name,'/Use full load, switch'), 'Value', string(use_full_load_switch_logical))

simOut = sim(model_name, t_range, options);
if run_number==0
    save(strcat('simulation_results/closed_loop',string(start_time)), 'simOut')
else
    %let python create the folder
    %mkdir(strcat('simulation_results/run',string(run_number)))
    save(strcat('simulation_results/run',string(run_number),'/closed_loop',string(start_time)), 'simOut')
end

% save state for beginning of next run
save_state_previous_end = simOut.save_state_end;
save('zz_saved_state','save_state_previous_end')


% extract results and return them in a single matrix
% time                = simOut.tout; % this one doesn't correctly interpolate
time                = simOut.timeseries_CV.time;
CV                  = simOut.timeseries_CV.Data;
SP                  = simOut.timeseries_SP.Data;
thermal_efficiency  = simOut.timeseries_thermal_efficiency.Data;
total_power_MW      = simOut.timeseries_total_power_MW.Data;
normalized_power    = simOut.timeseries_normalized_power.Data;
power_SP            = simOut.timeseries_power_SP.Data;
fuel_fraction       = simOut.timeseries_fuel_fraction.Data;
% load_fraction       = simOut.timeseries_load_fraction.Data;
n_inj_i             = simOut.timeseries_n_inj_i.Data;
n_inj_noBias_i      = simOut.timeseries_n_inj_noBias_i.Data;
% squeeze() removes all dimensions of length 1. CV has a bunch of length-1 dimensions
CV = squeeze(CV);
if length(SP)==1
    SP = SP .* ones(size(time));
end

toc
return_values = [time'; CV'; SP'; thermal_efficiency'; total_power_MW'; fuel_fraction'; n_inj_i'; n_inj_noBias_i'; normalized_power'; power_SP';]';
% return_values = simOut; % for debugging

% figure()
% subplot(4,1,1);
% hold on 
% plot(time,CV,'DisplayName','CV')
% plot(time,SP,'DisplayName','SP')
% legend()
% hold off 
% 
% subplot(4,1,2); 
% hold on 
% plot(time,thermal_efficiency,'DisplayName','thermal efficiency')
% legend()
% hold off
% 
% subplot(4,1,3); 
% hold on 
% plot(time,load_fraction,'DisplayName','load fraction (left axis)')
% yyaxis right
% plot(time,total_power_MW,'DisplayName','total power (right axis)')
% legend()
% hold off
% 
% subplot(4,1,4); 
% hold on 
% for i=1:9
%     if i < 7
%         plot(time,n_inj_i(:,i),'DisplayName',strcat('air inj level #',string(i)))
%     elseif i > 7
%         plot(time,n_inj_i(:,i),'DisplayName',strcat('air inj level #',string(i)),'Linestyle','--')
%     end
% end
% plot(time,n_inj_noBias_i(:,1),'DisplayName','unbiased air inj #1-6','LineWidth',2)
% plot(time,n_inj_noBias_i(:,8),'DisplayName','unbiased air inj #8-9','LineWidth',2,'Linestyle','--')
% % plot the two "nobias" lines in bold for comparison:
% % n_inj_noBias_i
% legend()
% hold off
% 
    
end
