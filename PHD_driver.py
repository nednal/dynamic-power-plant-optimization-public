# I needed a name that was clearly above MASTER, and PHD seems appropriate
# this script will pull all the strings to get my final comparison paper results
# things it needs to do:
#   x call the master script to generate data. Automatically increment to save in appropriately named folders
#   x grab snapshots for optimization comparison
#   x call the tensorflow training scripts to repeatedly train the models and converge on the best parameters
#   x using optimal model, tune metahueristic to minimize function evaluations
#   x call the Matlab script that puts the simulation data in csv format (need to save a csv of filenames to pull, as well as a increment variable for which one we did last)
#   x increment all variables
#
# Changes to make:
# * get simulink to run in the background
# * get a saved state that is right after the start-up phase so we don't have to simulate the first 12 minutes every time
# * comment out all scopes blocks in simulink

# allow for multiple scripts to be run on the same GPU, need to restrict the memory allocation so it doesn't eat up the whole device (which it does by default):
##https://www.reddit.com/r/learnmachinelearning/comments/hzrkxn/training_multiple_tensorflow_models_concurrently/
import tensorflow as tf
physical_devices = tf.config.list_physical_devices('GPU')
try:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)
    print('Successfully set memory growth on GPU')
except:
    print('Failed to set GPU memory growth. Cannot run GPU in parallel.')




num_future_timesteps = 10 # best to just leave this alone. Too much trouble to vary this

##ML_model = 'test'

prediction_type_array = 'fuel'
##prediction_type_array = 'CV'

ML_model = 'LSTM'
# ML sweep complete; use these for metaoptimziation:
fuel_model_number = 44 # best training model = 44
CV_model_number = 78 # best training model = 78
sample_size = 5

##ML_model = 'GRU'
### ML sweep complete; use these for metaoptimziation:
##fuel_model_number = 80 # best training model = 80
##CV_model_number = 79 # best training model = 79
##sample_size = 5

##ML_model = 'SimpleRNN'
### ML sweep complete; use these for metaoptimziation:
##fuel_model_number = 44 # best training model = 44
##CV_model_number = 79 # best training model = 79
##sample_size = 5

# choosen because this file is out of the training set:
sim_file = "machine_learning_data/run_snapshot/aaa_most_recent_results_1252.p"
current_timestep = 100 # arbitrary, but gets deeper into the dataset (in minutes)

optimizer = 'PSO'
##optimizer = 'GA'
##optimizer = 'ACO'

##run_case = 'gather_sim_data'
##run_case = 'sweep_ML_params'
##run_case ='sweep_optimizer_params'
##run_case = 'sweep_refined_optimizer_params'
run_case = 'closed_loop_control'

# for metaoptimization
run_start_counter = 0
run_end_counter = None
file_add_on = '_test' # detault is empty string, creates a different CSV file so multiple scripts don't fight over it

# for refined metaoptimization
start_timestep_array = [200, 300, 400, 500, 600, 700, 800, 900, 1000]
refined_metaoptimization_start_counter = 1

LSTM_pareto_depth_dic      = {'PSO':5,'GA':5,'ACO':7}
GRU_pareto_depth_dic       = {'PSO':4,'GA':7,'ACO':8}
SimpleRNN_pareto_depth_dic = {'PSO':4,'GA':6,'ACO':9}

pareto_depth_dic_dic = {'LSTM':LSTM_pareto_depth_dic,
                        'GRU':GRU_pareto_depth_dic,
                        'SimpleRNN':SimpleRNN_pareto_depth_dic}

# for closed_loop_control
dataframe_index_array = [0,3,4,11,12,13,15,16,18,19] # ranges up to 37 inclusive
run_counter_offset = 11000 # started at 9000 in honor of Dragonball Z, but have incremented by 1000 everytime I have to rerun things


if run_case == 'gather_sim_data':
    from MASTER_python_matlab import Master_driver
    from incrementer import write_increment_variables, get_increment_variables

    # indefinitely gather simulation data 
    closed_loop_counter, FFNN_version_counter, LSTM_version_counter = get_increment_variables()
    result = None
    while result != "Error":
        result = Master_driver(closed_loop_counter,
                               FFNN_version_counter,
                               LSTM_version_counter)
        closed_loop_counter += 1
        write_increment_variables(closed_loop_counter, FFNN_version_counter, LSTM_version_counter)
    

if run_case == 'sweep_ML_params':
    from aaa_fit_tensorflow_model import fit_TF_model_LSTM

    folder = 'machine_learning_data/hyperparameter_optimization/'

    model_start_counter = 1
##    model_end_counter = 200
    total_runs = 1*3*4*9

    if ML_model=='LSTM':
        swap_with_GRU       = False
        swap_with_SimpleRNN = False
    elif ML_model=='GRU':
        swap_with_GRU       = True
        swap_with_SimpleRNN = False
    elif ML_model=='SimpleRNN':
        swap_with_GRU       = False
        swap_with_SimpleRNN = True
    
    model_counter = 1
    for batch_size in [25]:
        for patience in [5,10,15]:
            for sample_size in [5,10,15,20]:
                for LSTM_size in [5,10,15,20,30,40,50,75,100]:
                    if model_counter >= model_start_counter: #and model_counter < model_end_counter:
                        Model_name = ML_model +'_' + prediction_type_array +'_' +str(model_counter)
                        print('Starting run',model_counter,'of',total_runs)
                        loss_train, loss_test = fit_TF_model_LSTM(keras_file_array = [Model_name],
                                                                  ML_model_folder = folder,
                                                                  plotting = False,
                                                                  prediction_type_array = [prediction_type_array],
                                                                  LSTM_layers_array = [1],
                                                                  from_file = False,
                                                                  LSTM_size = LSTM_size,
                                                                  sample_size = sample_size,
                                                                  patience = patience,
                                                                  batch_size = batch_size,
                                                                  swap_with_GRU=swap_with_GRU,
                                                                  swap_with_SimpleRNN=swap_with_SimpleRNN)
                        myCsvRow = Model_name+", "+str(LSTM_size)+", "+str(sample_size)+", "+str(patience)+", "+str(batch_size)+", "+str(loss_train)+", "+str(loss_test) +"\n"
                        with open(folder + ML_model +'_' + prediction_type_array +'_paramater_sweep.csv','a') as fd:
                            fd.write(myCsvRow)
                    model_counter += 1

def get_objective_function(ML_model, current_timestep, num_future_timesteps):
    if ML_model=='test':
        # test function
        dimensions = 80
        def f(x_normalized):
            x = x_normalized * 10
            return np.sum( (x-5)**4 * ((x-2)**4+0.1) )
        
    elif ML_model=='LSTM':
        from get_optimization_snapshot import get_objective_function
        num_past_timesteps = sample_size
        keras_file_obj = "machine_learning_data/hyperparameter_optimization/LSTM_" + str(fuel_model_number)
        keras_file_con = "machine_learning_data/hyperparameter_optimization/LSTM_CV_" + str(CV_model_number)        
        f, dimensions = get_objective_function(sim_file, current_timestep, num_past_timesteps, num_future_timesteps, keras_file_obj, keras_file_con)

    elif ML_model=='GRU':
        from get_optimization_snapshot import get_objective_function
        num_past_timesteps = sample_size
        keras_file_obj = "machine_learning_data/hyperparameter_optimization/GRU_fuel_" + str(fuel_model_number)
        keras_file_con = "machine_learning_data/hyperparameter_optimization/GRU_CV_" + str(CV_model_number)        
        f, dimensions = get_objective_function(sim_file, current_timestep, num_past_timesteps, num_future_timesteps, keras_file_obj, keras_file_con)

    elif ML_model=='SimpleRNN':
        from get_optimization_snapshot import get_objective_function
        num_past_timesteps = sample_size
        keras_file_obj = "machine_learning_data/hyperparameter_optimization/SimpleRNN_fuel_" + str(fuel_model_number)
        keras_file_con = "machine_learning_data/hyperparameter_optimization/SimpleRNN_CV_" + str(CV_model_number)        
        f, dimensions = get_objective_function(sim_file, current_timestep, num_past_timesteps, num_future_timesteps, keras_file_obj, keras_file_con)

    return f, dimensions


if run_case == 'sweep_optimizer_params':
    
    from optimizers import PSO, ACO, GA
    import numpy as np
    
    folder = 'machine_learning_data/metaoptimization/'

##    if ML_model=='test':
##        # test function
##        dimensions = 80
##        def f(x_normalized):
##            x = x_normalized * 10
##            return np.sum( (x-5)**4 * ((x-2)**4+0.1) )
##        
##    elif ML_model=='LSTM':
##        from get_optimization_snapshot import get_objective_function
##        num_past_timesteps = sample_size
##        keras_file_obj = "machine_learning_data/hyperparameter_optimization/LSTM_" + str(fuel_model_number)
##        keras_file_con = "machine_learning_data/hyperparameter_optimization/LSTM_CV_" + str(CV_model_number)        
##        f, dimensions = get_objective_function(sim_file, current_timestep, num_past_timesteps, num_future_timesteps, keras_file_obj, keras_file_con)
##
##    elif ML_model=='GRU':
##        from get_optimization_snapshot import get_objective_function
##        num_past_timesteps = sample_size
##        keras_file_obj = "machine_learning_data/hyperparameter_optimization/GRU_fuel_" + str(fuel_model_number)
##        keras_file_con = "machine_learning_data/hyperparameter_optimization/GRU_CV_" + str(CV_model_number)        
##        f, dimensions = get_objective_function(sim_file, current_timestep, num_past_timesteps, num_future_timesteps, keras_file_obj, keras_file_con)
##
##    elif ML_model=='SimpleRNN':
##        from get_optimization_snapshot import get_objective_function
##        num_past_timesteps = sample_size
##        keras_file_obj = "machine_learning_data/hyperparameter_optimization/SimpleRNN_fuel_" + str(fuel_model_number)
##        keras_file_con = "machine_learning_data/hyperparameter_optimization/SimpleRNN_CV_" + str(CV_model_number)        
##        f, dimensions = get_objective_function(sim_file, current_timestep, num_past_timesteps, num_future_timesteps, keras_file_obj, keras_file_con)

    f, dimensions = get_objective_function(ML_model, current_timestep, num_future_timesteps)
    
    if optimizer=='PSO':
        total_runs = 4*6*6*6
        run_counter = 1
        for w in [0.25,0.5,0.75,1.0]:
            for c1 in [0.25,0.5,1.0,1.5,2.0,3.0]:
                for c2 in [0.25,0.5,1.0,1.5,2.0,3.0]:
                    for i_max in [5,10,25,50,75,100]:
                        if run_counter >= run_start_counter and (run_end_counter is None or run_counter < run_end_counter):
                            print('Starting run',run_counter,'of',total_runs,'using',ML_model,'and',optimizer,'from: [',run_start_counter,',',run_end_counter,']')
                            pso = PSO(dimensions, f, i_max=i_max, w=w, c1=c1, c2=c2)
                            x_best_global, obj_best_global, iterations, function_evals = pso.optimize()
                            myCsvRow = str(run_counter)+", "+str(w)+", "+str(c1)+", "+str(c2)+", "+str(i_max)+", "+str(obj_best_global)+", "+str(iterations)+", "+str(function_evals) +"\n"
                            with open(folder + optimizer +'_' + ML_model + '_paramater_sweep'+file_add_on+'.csv','a') as fd:
                                fd.write(myCsvRow)
                        run_counter += 1
    elif optimizer=='ACO':
        total_runs = 5*5*4*6
        run_counter = 1
        for Q in [1e2, 1e3, 1e4, 1e5, 1e6]:
            for rho in [0.8,0.9,0.95,0.99,1.0]:
                for discretizations in [10, 25, 100, 1000]:
                    for i_max in [5,10,25,50,75,100]: 
                        if run_counter >= run_start_counter and (run_end_counter is None or run_counter < run_end_counter):
                            print('Starting run',run_counter,'of',total_runs,'using',ML_model,'and',optimizer,'from: [',run_start_counter,',',run_end_counter,']')
                            aco = ACO(dimensions, f, i_max=i_max, Q=Q, rho=rho, discretizations=discretizations)
                            x_best_global, obj_best_global, iterations, function_evals = aco.optimize()
                            myCsvRow = str(run_counter)+", "+str(Q)+", "+str(rho)+", "+str(discretizations)+", "+str(i_max)+", "+str(obj_best_global)+", "+str(iterations)+", "+str(function_evals) +"\n"
                            with open(folder + optimizer +'_' + ML_model + '_paramater_sweep'+file_add_on+'.csv','a') as fd:
                                fd.write(myCsvRow)
                        run_counter += 1
    elif optimizer=='GA':
        total_runs = 6*3*3*4*4
        run_counter = 1
        for tournament_size in [2,3,5,10,15,20]:
            for bit_count in [4, 8, 16]:
                for m in [1/2,1/4,1/8]:#,1/16]:
                    for c in [0.80,0.9,0.95,0.99]:
                        for i_max in [5, 10, 25, 100]:#, 200, 500]:
                            if tournament_size>=i_max:
                                run_counter += 1
                                continue # no need to test these cases
                            if run_counter >= run_start_counter and (run_end_counter is None or run_counter < run_end_counter):
                                print('Starting run',run_counter,'of',total_runs,'using',ML_model,'and',optimizer,'from: [',run_start_counter,',',run_end_counter,']')
                                ga = GA(dimensions, f, i_max=i_max, tournament_size=tournament_size, bit_count=bit_count, m=m, c=c)
                                x_best_global, obj_best_global, iterations, function_evals = ga.optimize()
                                myCsvRow = str(run_counter)+", "+str(bit_count)+", "+str(m)+", "+str(c)+", "+str(tournament_size)+", "+str(i_max)+", "+str(obj_best_global)+", "+str(iterations)+", "+str(function_evals) +"\n"
                                with open(folder + optimizer +'_' + ML_model + '_paramater_sweep'+file_add_on+'.csv','a') as fd:
                                    fd.write(myCsvRow)
                            run_counter += 1


if run_case == 'sweep_refined_optimizer_params':
    from optimizers import PSO, ACO, GA
    import numpy as np
    from PHD_analysis_and_figures import Analysis

    folder = 'machine_learning_data/refined_metaoptimization/'

    run_counter = 1

    pareto_depth = pareto_depth_dic_dic[ML_model][optimizer]
    analysis = Analysis(ML_model, optimizer, pareto_cuttoff = pareto_depth)

    params_df, _ = analysis.get_pareto_front_parameters()

    total_runs = len(params_df) * len(start_timestep_array)
    
    for start_timestep in start_timestep_array:
        
        f, dimensions = get_objective_function(ML_model, start_timestep, num_future_timesteps)
        
        for i in range(len(params_df)):
            if run_counter >= run_start_counter and (run_end_counter is None or run_counter < run_end_counter):
                print('Starting run',run_counter,'of',total_runs,'at timestep',start_timestep,'using',ML_model,'and',optimizer,'from: [',run_start_counter,',',run_end_counter,']')
                if optimizer=='PSO':
                    i_max=params_df['i_max'].to_numpy()[i]
                    w=params_df['w'].to_numpy()[i]
                    c1=params_df['c1'].to_numpy()[i]
                    c2=params_df['c2'].to_numpy()[i]
                    pso = PSO(dimensions, f,
                              i_max=i_max,
                              w=w,
                              c1=c1,
                              c2=c2)
                    x_best_global, obj_best_global, iterations, function_evals = pso.optimize()
                    myCsvRow = str(run_counter)+", "+str(w)+", "+str(c1)+", "+str(c2)+", "+str(i_max)+", "+str(obj_best_global)+", "+str(iterations)+", "+str(function_evals)+", "+str(start_timestep) +"\n"
                    with open(folder + optimizer +'_' + ML_model + '_refined_paramater_sweep'+file_add_on+'.csv','a') as fd:
                        fd.write(myCsvRow)
                if optimizer=='ACO':
                    i_max=params_df['i_max'].to_numpy()[i]
                    Q=params_df['Q'].to_numpy()[i]
                    rho=params_df['rho'].to_numpy()[i]
                    discretizations=params_df['discretizations'].to_numpy()[i]
                    aco = ACO(dimensions, f,
                              i_max=i_max,
                              Q=Q,
                              rho=rho,
                              discretizations=discretizations)
                    x_best_global, obj_best_global, iterations, function_evals = aco.optimize()
                    myCsvRow = str(run_counter)+", "+str(Q)+", "+str(rho)+", "+str(discretizations)+", "+str(i_max)+", "+str(obj_best_global)+", "+str(iterations)+", "+str(function_evals)+", "+str(start_timestep) +"\n"
                    with open(folder + optimizer +'_' + ML_model + '_refined_paramater_sweep'+file_add_on+'.csv','a') as fd:
                        fd.write(myCsvRow)
                if optimizer=='GA':
                    i_max=params_df['i_max'].to_numpy()[i]
                    tournament_size=params_df['tournament_size'].to_numpy()[i]
                    bit_count=params_df['bit_count'].to_numpy()[i]
                    m=params_df['m'].to_numpy()[i]
                    c=params_df['c'].to_numpy()[i]
                    ga = GA(dimensions, f,
                            i_max=i_max,
                            tournament_size=tournament_size,
                            bit_count=bit_count,
                            m=m,
                            c=c)
                    x_best_global, obj_best_global, iterations, function_evals = ga.optimize()
                    myCsvRow = str(run_counter)+", "+str(bit_count)+", "+str(m)+", "+str(c)+", "+str(tournament_size)+", "+str(i_max)+", "+str(obj_best_global)+", "+str(iterations)+", "+str(function_evals)+", "+str(start_timestep) +"\n"
                    with open(folder + optimizer +'_' + ML_model + '_refined_paramater_sweep'+file_add_on+'.csv','a') as fd:
                        fd.write(myCsvRow)
            run_counter += 1

if run_case == 'closed_loop_control':
    from MASTER_python_matlab import Master_driver
    from optimizers import PSO, ACO, GA
    import numpy as np
    import pandas as pd

    class Closed_loop_vars:
        def __init__(self, df_index):
            file = 'machine_learning_data/refined_metaoptimization/combined_pareto_fronts.csv'
            df_raw = pd.read_csv(file)
            self.ML_type = df_raw.loc[df_index,'ML_type']
            self.optimizer = df_raw.loc[df_index,'optimizer']
            if self.optimizer == 'PSO':
                self.w = df_raw.loc[df_index,'w-bit_count-Q']
                self.c1 = df_raw.loc[df_index,'c1-m-rho']
                self.c2 = df_raw.loc[df_index,'c2-c-discretizations']
            elif self.optimizer == 'GA':
                self.bit_count = int(df_raw.loc[df_index,'w-bit_count-Q'])
                self.m = df_raw.loc[df_index,'c1-m-rho']
                self.c = df_raw.loc[df_index,'c2-c-discretizations']
                self.tournament_size = int(df_raw.loc[df_index,'N/A-tournament_size-N/A'])
            elif self.optimizer == 'ACO':
                self.Q = df_raw.loc[df_index,'w-bit_count-Q']
                self.rho = df_raw.loc[df_index,'c1-m-rho']
                self.discretizations = int(df_raw.loc[df_index,'c2-c-discretizations'])
            self.i_max = int(df_raw.loc[df_index,'i_max'])
            self.obj_best_global_median = df_raw.loc[df_index,'obj_best_global_median']
            self.function_evals_median = df_raw.loc[df_index,'function_evals_median']

            if self.ML_type == 'LSTM':
                self.keras_file_obj = "machine_learning_data/hyperparameter_optimization/LSTM_" + str(44)
                self.keras_file_con = "machine_learning_data/hyperparameter_optimization/LSTM_CV_" + str(78)        

            elif self.ML_type == 'GRU':
                self.keras_file_obj = "machine_learning_data/hyperparameter_optimization/GRU_fuel_" + str(80)
                self.keras_file_con = "machine_learning_data/hyperparameter_optimization/GRU_CV_" + str(79)        

            elif self.ML_type == 'SimpleRNN':
                self.keras_file_obj = "machine_learning_data/hyperparameter_optimization/SimpleRNN_fuel_" + str(44)
                self.keras_file_con = "machine_learning_data/hyperparameter_optimization/SimpleRNN_CV_" + str(79)
            
            self.num_predictions = 10
            self.decision_frequency = 1
            self.sample_size = 5
            self.random_seed = 12345 # just need a value so the same profile is generated each time
            self.simulation_length = 60*3 # in minutes

            self.df_index = df_index

            self.function_evals_array = []
            self.obj_best_global_array = []

        def perform_optimization(self, objective_function):
            dimensions = num_future_timesteps*8
            print('performing optimization with',self.ML_type,'and',self.optimizer)
            if self.optimizer == 'PSO':
                Optimizer = PSO(dimensions, objective_function,
                                i_max=self.i_max,
                                w=self.w,
                                c1=self.c1,
                                c2=self.c2)
            elif self.optimizer == 'GA':
                Optimizer = GA(dimensions, objective_function,
                               i_max=self.i_max,
                               tournament_size=self.tournament_size,
                               bit_count=self.bit_count,
                               m=self.m,
                               c=self.c)
            elif self.optimizer == 'ACO':
                Optimizer = ACO(dimensions, objective_function,
                                i_max=self.i_max,
                                Q=self.Q,
                                rho=self.rho,
                                discretizations=self.discretizations)
            else:
                print(self.optimizer,'did not match with an optimzier')
            x_best_global, obj_best_global, iterations, function_evals = Optimizer.optimize(output='Light')

            # these might be worth storing
            self.function_evals_array.append(function_evals)
            self.obj_best_global_array.append(obj_best_global)
            
            return x_best_global

    for dataframe_index in dataframe_index_array:
        print('\n>>>Running closed-loop simulation with dataframe_index:',dataframe_index)
        closed_loop_vars = Closed_loop_vars(dataframe_index)

        try:
            Master_driver(closed_loop_counter = run_counter_offset+dataframe_index,
                          text_and_plot_when_complete = False,
                          closed_loop_vars = closed_loop_vars)
            import sys
            from send_text_message import send_message
             send_message("Finished closed-loop index "+str(dataframe_index)+' ('+str(dataframe_index)+' of '+str(len(dataframe_index_array))+')')
        except Exception as e:
            import sys
            from send_text_message import send_message
            send_message("PhD Driver script closed-loop simulation crashed on Watson.")
            print(repr(e))


from send_text_message import send_message
send_message("PhD Driver script complete.")
    
