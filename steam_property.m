function property = steam_property(property_string, v1, v2)

% for testing:
% property_string = 'rho_pt';
% v1 = 100000 * ones([5,1]);
% v2 = 300;
% v2 = [600, 610, 620, 630, 640];

coder.extrinsic('XSteam.m');
% https://www.mathworks.com/matlabcentral/fileexchange/9817-x-steam-thermodynamic-properties-of-water-and-steam

k_to_decC = -273.15;
decC_toK  = 273.15;
bar_per_Pa = 1e-5;
kJ_per_J = 1e-3;
J_per_kJ = 1e3;

i_max = length(v1);
property = zeros([i_max 1]);

if strcmp(property_string,'rho_pt')
    p = v1 .* bar_per_Pa;
    T = v2 + k_to_decC;
    for i=1:i_max
        property(i) = XSteam(property_string,p(i),T(i));
    end
    % not unit conversion needed, units are kg/m3
    
elseif strcmp(property_string,'tc_pt')
    p = v1 .* bar_per_Pa;
    T = v2 + k_to_decC;
    for i=1:i_max
        property(i) = XSteam(property_string,p(i),T(i));
    end
    % not unit conversion needed, units are W/m K
    
elseif strcmp(property_string,'my_pt')
    p = v1 .* bar_per_Pa;
    T = v2 + k_to_decC;
    for i=1:i_max
        property(i) = XSteam(property_string,p(i),T(i));
    end
    % not unit conversion needed, units are Pa*s
    
elseif strcmp(property_string,'Cp_pt')
    p = v1 .* bar_per_Pa;
    T = v2 + k_to_decC;
    for i=1:i_max
        property(i) = XSteam(property_string,p(i),T(i));
    end
    property = property * J_per_kJ;
    
elseif strcmp(property_string,'Tsat_p')
    p = v1 .* bar_per_Pa;
    for i=1:i_max
        property(i) = XSteam(property_string,p(i));
    end
    property = property + decC_toK;
    
elseif strcmp(property_string,'s_pt')
    p = v1 .* bar_per_Pa;
    T = v2 + k_to_decC;
    for i=1:i_max
        property(i) = XSteam(property_string,p(i),T(i));
    end
    property = property .* J_per_kJ;
    
elseif strcmp(property_string,'h_pt')
    p = v1 .* bar_per_Pa;
    T = v2 + k_to_decC;
    for i=1:i_max
        property(i) = XSteam(property_string,p(i),T(i));
    end
    property = property .* J_per_kJ;
    
elseif strcmp(property_string,'h_ps')
    p = v1 .* bar_per_Pa;
    s = v2 .* kJ_per_J;
    for i=1:i_max
        property(i) = XSteam(property_string,p(i),s(i));
    end
    property = property .* J_per_kJ;

elseif strcmp(property_string,'T_ps')
    p = v1 .* bar_per_Pa;
    s = v2 .* kJ_per_J;
    for i=1:i_max
        property(i) = XSteam(property_string,p(i),s(i));
    end
    property = property + decC_toK;
    
elseif strcmp(property_string,'T_ph')
    p = v1 .* bar_per_Pa;
    h = v2 .* kJ_per_J;
    for i=1:i_max
        property(i) = XSteam(property_string,p(i),h(i));
    end
    property = property + decC_toK;
    
elseif strcmp(property_string,'h_px')
    p = v1 .* bar_per_Pa;
    x = v2;
    for i=1:i_max
        property(i) = XSteam(property_string,p(i),x(i));
    end
    property = property .* J_per_kJ;
    
elseif strcmp(property_string,'Pr_pt') % I'm adding this because we need the Prandtl number, and it's easier to calculate it here
    p = v1 .* bar_per_Pa;
    T = v2 + k_to_decC;
    for i=1:i_max
        cp_i = XSteam('Cp_pt',p(i),T(i));
        mu_i = XSteam('my_pt',p(i),T(i));
        k_i = XSteam('tc_pt',p(i),T(i));
        property(i) = cp_i .* mu_i ./ k_i; % Pr_steam_i = cp_i .* mu_i ./ k_i
    end
else
    throw(MException('XSteam:missingString','The XSteam string is not valid or not listed in my module'));
end

% display(property)

% x=1; % temp


%% from the Xsteam package script:
%*** Nomenclature ******************************************************************************************
% First the wanted property then a _ then the wanted input properties. 
% Example. T_ph is temperature as a function of pressure and enthalpy. 
% For a list of valid functions se bellow or XSteam for MS Excel.
% T     Temperature (deg C)
% p	    Pressure    (bar)
% h	    Enthalpy    (kJ/kg)
% v	    Specific volume (m3/kg)
% rho	Density (kg/m3)
% s	    Specific entropy (kJ/(kg*K)
% u	    Specific internal energy 
% Cp	Specific isobaric heat capacity (kJ/(kg*K)
% Cv	Specific isochoric heat capacity (kJ/(kg*K)
% w	    Speed of sound (m/s)
% my	Viscosity (Dynamic Viscosity) (pa*s)
% tc	Thermal Conductivity (W/m K)
% st	Surface Tension
% x	    Vapour fraction
% vx	Vapour Volume Fraction
%
%*** Valid Steam table functions. ****************************************************************************
%
%Temperature	
%Tsat_p	Saturation temperature
%T_ph	Temperture as a function of pressure and enthalpy
%T_ps	Temperture as a function of pressure and entropy
%T_hs	Temperture as a function of enthalpy and entropy
%
%Pressure	
%psat_T	Saturation pressure
%p_hs	Pressure as a function of h and s. 
%p_hrho Pressure as a function of h and rho. Very unaccurate for solid water region since it's almost incompressible!
%
%Enthalpy	
%hV_p	Saturated vapour enthalpy
%hL_p	Saturated liquid enthalpy
%hV_T	Saturated vapour enthalpy
%hL_T	Saturated liquid enthalpy
%h_pT	Entalpy as a function of pressure and temperature.
%h_ps	Entalpy as a function of pressure and entropy.
%h_px	Entalpy as a function of pressure and vapour fraction
%h_prho	Entalpy as a function of pressure and density. Observe for low temperatures (liquid) this equation has 2 solutions. 
%h_Tx	Entalpy as a function of temperature and vapour fraction
%
%Specific volume	
%vV_p	Saturated vapour volume
%vL_p	Saturated liquid volume
%vV_T	Saturated vapour volume
%vL_T	Saturated liquid volume
%v_pT	Specific volume as a function of pressure and temperature.
%v_ph	Specific volume as a function of pressure and enthalpy
%v_ps	Specific volume as a function of pressure and entropy.
%
%Density	
%rhoV_p	Saturated vapour density
%rhoL_p	Saturated liquid density
%rhoV_T	Saturated vapour density
%rhoL_T	Saturated liquid density
%rho_pT	Density as a function of pressure and temperature.
%rho_ph	Density as a function of pressure and enthalpy
%rho_ps	Density as a function of pressure and entropy.
%
%Specific entropy 	
%sV_p	Saturated vapour entropy
%sL_p	Saturated liquid entropy
%sV_T	Saturated vapour entropy
%sL_T	Saturated liquid entropy
%s_pT	Specific entropy as a function of pressure and temperature (Returns saturated vapour entalpy if mixture.)
%s_ph	Specific entropy as a function of pressure and enthalpy
%
%Specific internal energy 	
%uV_p	Saturated vapour internal energy
%uL_p	Saturated liquid internal energy
%uV_T	Saturated vapour internal energy
%uL_T	Saturated liquid internal energy
%u_pT	Specific internal energy as a function of pressure and temperature.
%u_ph	Specific internal energy as a function of pressure and enthalpy
%u_ps	Specific internal energy as a function of pressure and entropy.
%
%Specific isobaric heat capacity 	
%CpV_p	Saturated vapour heat capacity 
%CpL_p	Saturated liquid heat capacity 
%CpV_T	Saturated vapour heat capacity 
%CpL_T	Saturated liquid heat capacity 
%Cp_pT	Specific isobaric heat capacity as a function of pressure and temperature.
%Cp_ph	Specific isobaric heat capacity as a function of pressure and enthalpy
%Cp_ps	Specific isobaric heat capacity as a function of pressure and entropy.
%
%Specific isochoric heat capacity 	
%CvV_p	Saturated vapour isochoric heat capacity
%CvL_p	Saturated liquid isochoric heat capacity
%CvV_T	Saturated vapour isochoric heat capacity
%CvL_T	Saturated liquid isochoric heat capacity
%Cv_pT	Specific isochoric heat capacity as a function of pressure and temperature.
%Cv_ph	Specific isochoric heat capacity as a function of pressure and enthalpy
%Cv_ps	Specific isochoric heat capacity as a function of pressure and entropy.
%
%Speed of sound 	
%wV_p	Saturated vapour speed of sound
%wL_p	Saturated liquid speed of sound
%wV_T	Saturated vapour speed of sound
%wL_T	Saturated liquid speed of sound
%w_pT	Speed of sound as a function of pressure and temperature.
%w_ph	Speed of sound as a function of pressure and enthalpy
%w_ps	Speed of sound as a function of pressure and entropy.
%
%Viscosity (Dynamic Viscosity)
%Viscosity is not part of IAPWS Steam IF97. Equations from 
%"Revised Release on the IAPWS Formulation 1985 for the Viscosity of Ordinary Water Substance", 2003 are used.	
%Viscosity in the mixed region (4) is interpolated according to the density. This is not true since it will be two fases.	
%my_pT	Viscosity as a function of pressure and temperature.
%my_ph	Viscosity as a function of pressure and enthalpy
%my_ps	Viscosity as a function of pressure and entropy.
%
%Thermal Conductivity	
%Revised release on the IAPS Formulation 1985 for the Thermal Conductivity of ordinary water substance (IAPWS 1998)	
%tcL_p	Saturated vapour thermal conductivity
%tcV_p	Saturated liquid thermal conductivity
%tcL_T	Saturated vapour thermal conductivity
%tcV_T	Saturated liquid thermal conductivity
%tc_pT	Thermal conductivity as a function of pressure and temperature.
%tc_ph	Thermal conductivity as a function of pressure and enthalpy
%tc_hs	Thermal conductivity as a function of enthalpy and entropy
%
%Surface tension
%st_T	Surface tension for two phase water/steam as a function of T
%st_p	Surface tension for two phase water/steam as a function of T
%Vapour fraction
%x_ph	Vapour fraction as a function of pressure and enthalpy
%x_ps	Vapour fraction as a function of pressure and entropy.
%
%Vapour volume fraction
%vx_ph	Vapour volume fraction as a function of pressure and enthalpy
%vx_ps	Vapour volume fraction as a function of pressure and entropy.
