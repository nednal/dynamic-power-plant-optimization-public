
import matplotlib.pyplot as plt

T = 1500
cp = 3.4e7
UA_in = 5e4
UA_out = UA_in * .01
T_in = 4000
T_out = 298
delta_t = .1 # seconds
time = int(30*60/delta_t) # number of timesteps in 30 minutes
T_array = []
t_array = []

for t in range(time):
    dT_dt = UA_in / cp * (T_in - T) + UA_out / cp * (T_out - T) 
    T += dT_dt * delta_t
    T_array.append(T)
    t_array.append(t*delta_t)
    
print('Final T after 30 minutes:',T,'K')
plt.plot(t_array, T_array)