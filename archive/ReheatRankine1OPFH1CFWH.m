clc
clear 
% Reheat regenerative rankine cycle with one open feedwater heater (OFWH)
% and one closed feedwater heater (CFWH)

%validated with
%https://www.ohio.edu/mechanical/thermo/Applied/Chapt.7_11/Chapter8b.html 
% and chapter 2 of ther power plant engineering textbook

%Input parameters
 
T (1)=500; % superheater outlet temperature and inlet temperature to the high temperature turbine (1st turbine) in C
P (1)=90; % inlet pressure to the high temperature turbine (1st turbine) in Bar
P(5)= 0.08; %inlet pressure of the condenser
 
Etta_tur_HP=0.92; %high pressure turbine isentropic efficiency
Etta_tur_LP=0.90; %high pressure turbine isentropic efficiency
 
Etta_pump_CEP=0.75; %Condensate extraction pump isentropic efficiency
Etta_pump_BFP=1; %Boiler feed pump isentropic efficiency
 
TTD = 1.5; %Terminal temperature difference"
DCA= 5;  % Drain cooling approach"
  
%W_net = 50000 %Size of the plant (cycle) in kW 
m_dot_steam=1;

%Pressures calculation"
 
Tsat (1) = XSteam('TSat_p',P(1)); 
Tsat (5) = XSteam('TSat_p',P(5)); 
%P(2)=XSteam('psat_T',Tsat(1)-(Tsat(1)-Tsat(5))/3);
%P(4)=XSteam('psat_T',Tsat(1)-(Tsat(1)-Tsat(5))*2/3);
P(2)=18;
P(4)=0.1;

P(10)=P(1)*(1+0.1);
P(9) = P(10);
P(11)=P(2)*(1-0.025);
P(7)=P(4);
P(12)=P(4);
P(8) = P(7);
P(6) = P(5);
 
%______Turbine analysis __________% 
% HP turbine"
h(1) = XSteam('h_pT',P(1),T(1));
s(1) = XSteam('s_ph',P(1),h(1));

ss(2)=s(1)
h(1) = XSteam('h_pT',P(1),T(1));
hs(2)=XSteam('h_ps',P(2),ss(2));
h(2)=h(1)+Etta_tur_HP*(hs(2)-h(1));
T(2)=XSteam('T_ph', P(2),h(2)); 
s(2) = XSteam('s_ph',P(2),h(2));
x(2) = XSteam('x_ps',P(2),s(2));


%Reheat section"
T(3)=T(1);
P(3)=P(2)*(1-0.025);
h(3) = XSteam('h_pT',P(3),T(3));
s(3) = XSteam('s_ph',P(3),h(3));

%LP turbine 1"
ss(4)=s(3)
hs(4)=XSteam('h_ps',P(4),ss(4)) 
h(4)=h(3)+Etta_tur_LP*(hs(4)-h(3));
T(4)=XSteam('T_ph', P(4),h(4)); 
s(4) = XSteam('s_ph',P(4),h(4));
x(4) = XSteam('x_ps',P(4),s(4));

 
%LP turbine 2"
ss(5)=s(4)
hs(5)=XSteam('h_ps',P(5),ss(5)) 
h(5)=h(4)+Etta_tur_LP*(hs(5)-h(4));
T(5)=XSteam('T_ph', P(5),h(5)); 
s(5) = XSteam('s_ph',P(5),h(5));
x(5) = XSteam('x_ps',P(5),s(5));


%"Condenser exit pump or Pump 1 analysis"
T(6) = XSteam('TSat_p',P(6))-0.5;  %make sure that the condenser is slightly subcooled"
h(6) = XSteam('h_pT',P(6),T(6));
s(6) = XSteam('s_ph',P(6),h(6));

v(6)=XSteam('vL_p', P(6));

W_pump_CEP=(v(6)*(P(7)-P(6)))*100/Etta_pump_CEP; %100 is to convert bar to kpa
h(7)=h(6) + W_pump_CEP;   
T(7)=XSteam('T_ph', P(7),h(7)); 
s(7) = XSteam('s_ph',P(7),h(7));
x(7) = XSteam('x_ps',P(7),s(7));
 
%"Closed Feedwater Heater 1 (before boiler) analysis"  
Tsat (2) = XSteam('TSat_p',P(2)); 
T(10)=Tsat (2)-TTD;
h(10) = XSteam('h_Tx',T(10),0);
s(10) = XSteam('s_ph',P(10),h(10));

%Open Feedwater Heater analysis" 
 Tsat (4) = XSteam('TSat_p',P(4)); 
T(8)=Tsat (4)-TTD;
h(8) = XSteam('h_pT',P(8),T(8));
s(8) = XSteam('s_ph',P(8),h(8)); 
  
%Pump 2 analysis"
 
v(8)=XSteam('vL_p', P(8));
W_pump_BFP=(v(8)*(P(9)-P(8)))*100/Etta_pump_BFP;
h(9)=h(8) + W_pump_BFP;   
T(9)=XSteam('T_ph', P(9),h(9)); 
s(9) = XSteam('s_ph',P(9),h(9));
x(9) = XSteam('x_ps',P(9),s(9));
 
%Steam Trap 1"
 
T(11)=T(9)+DCA;
h(11) = XSteam('h_pT',P(11),T(11));
s(11) = XSteam('s_ph',P(11),h(11)); 

h(12)=h(11);
T(12)=XSteam('T_ph', P(12),h(12)); 
s(12) = XSteam('s_ph',P(12),h(12)); 


a=(h(10)-h(9))/(h(2)-h(11));
b=(h(8)-a*h(12)-(1-a)*h(7))/(h(4)-h(7));
 
%preheater outlet: boil_in"
P(13) = P(1); 
T(13) = XSteam('TSat_p',P(13));
h(13) = XSteam('h_Tx',T(13),0);
s(13) = XSteam('s_ph',P(13),h(13));

q_preheater = h(13)-h(10);
q_dot_preheater = q_preheater*m_dot_steam; 


%boiler outlet:superheater inlet
P(14) = P(13);
T(14) = XSteam('TSat_p',P(14));
h(14) = XSteam('h_Tx',T(14),1);
s(14) = XSteam('s_ph',P(14),h(14));
 
q_boil = h(14)-h(13);
q_dot_boil = q_boil*m_dot_steam; 

% superheater"
q_sh = h(1)-h(14);
 q_dot_sh = q_sh*m_dot_steam; 

% reheater"
q_RH = h(3)-h(2); 
q_dot_RH = q_RH*m_dot_steam*(1-a);

%"Calculate condenser properties --------------------"
q_cond = (1-a-b)* (h(5)-h(6));

q_dot_cond = q_cond*m_dot_steam;
 
  
 %Cycle Calculations"
 
W_tur (1)= h(1)-h(2);
W_tur (2)=(1-a)* (h(3)-h(4));
W_tur (3)=(1-a-b)* (h(4)-h(5));
 
q_dot_hot_tot = q_dot_preheater+q_dot_boil+q_dot_sh+q_dot_RH
 
W_tur_total_WO=(W_tur (1)+W_tur (2)+W_tur (3)); %specific turbine work without mass flow rate
 
W_pump_total_WO=((1-a-b)*W_pump_CEP+W_pump_BFP); %specific pump work without mass flow rate
 
W_WO=W_tur_total_WO-W_pump_total_WO;
 
W_tur_total=(W_tur (1)+W_tur (2)+W_tur (3))*m_dot_steam;
 
W_pump_total=((1-a-b)*W_pump_CEP*m_dot_steam+W_pump_BFP*m_dot_steam);
 
W_net=W_tur_total-W_pump_total
 
W_final_cycle=W_net*0.98 %0.98 is generator efficinecy
 
Etta_cycle=W_net/q_dot_hot_tot
 
