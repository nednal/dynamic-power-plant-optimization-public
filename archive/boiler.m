function [dT_i, dn_ij] = boiler(T_inj_i, n_inj_ij, T_i, n_ij)

% n_inj_ij = reshape(n_inj_ij,3,4); % simulink seems to think it's funny to flatten my matrix
% n_inj_ij_T = n_inj_ij';

% A + B => C + D
% i means spacial node, j means chemical species, 
% "inj" means injection
% "rxn" means chemical reaction

% parameters:
V = 10; % m^3
P = 1; % atm
R = 1; % 8.20573 m^3 atm / mol K 
H_rxn = 10; % J/mol
C_p_j = [1, 1, 1, 1]'; % J/mol K
A = 10000; % Arhenius equation constant
E_a = 100*R; % activation energy
stoic_rxn_j = [-1, -1, 1, 1]';
order_rxn_j = [1, 1, 0, 0]';

% intermediates:
n_tot_i = sum(n_ij,2); 
% C_p_avg = n_ij' * C_p_j ./ n_tot_i;
temp = n_ij * C_p_j;
C_p_avg = temp ./ n_tot_i;
% n_inj_ij = reshape(n_inj_ij,3,4);
temp = n_inj_ij ./ ones(4,3);
temp2 = order_rxn_j ./ ones(4,1);
n_rxn_i = A * exp(-E_a/(R*T_i)) .* prod( (n_inj_ij / V ) .^ order_rxn_j );
% temp2 = n_inj_ij_T / V;
% temp4 = temp2 ./ ones(4,3);
% temp3 = temp2 .^ order_rxn_j;
% n_rxn_i = A * exp(-E_a/(R*T_i)) * prod( temp3 );
% n_rxn_i = A * exp(-E_a/(R*T_i)) * prod( (reshape(n_inj_ij,3,4)' / V ) .^ order_rxn_j );

% confirmed that simulink will allow matrix division in a matlab function
% x = A\B
% A = pascal(3);
% u = [3; 1; 4];
% x = A\u;

dT_i = [0;0;0];
dn_ij = [[0,0,0,0];[0,0,0,0];[0,0,0,0]];

end
