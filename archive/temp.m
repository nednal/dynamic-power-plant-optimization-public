solid_x_i = 0.1;
gas_x_i = [0.21; 0.79; 0];
T = 1500;
T_wall = 1500;

Q_previous_in = 0;
Q_injection_air_in = 0;
Q_injection_fuel_in = 0.01;
solid_x_i_previous = 0;
solid_x_i_injection = 700;
gas_x_i_previous = [.21; .79; 0];
gas_x_i_injection = [.21; .79; 0];
T_previous = 1500;
T_injection = 1500;


AAA_run_the_load_file; % little trick to remind you to run the load file


% https://marcepinc.com/blog/coal-combustion-process-and-its-products % simple breakdown of combustion chemistry
% https://nvlpubs.nist.gov/nistpubs/Legacy/NSRDS/nbsnsrds67.pdf % source for combustion reaction terms
% note: 12 gm of pure carbon = 1 mol, so start there then dilute it with ash as needed

% array ordering for species:
% gas: [O2 N2 CO2] % add in CO after this gets working!
% solid: [C] % perhaps add in ash/non-combustibles after this gets working

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% master list of variables %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% inputs:
% Q_previous_in         = ;     %m^3/s,     fixed,      Flow into the reactor from previous reactor
% Q_injection_in        = ;     %m^3/s,     fixed,      Flow into the reactor from injection (combines air and fuel injection, since both should have the same gas composition)
%`solid_x_i             = ;     %kg/m^3,    dynamic,    Mass concentration array of solid species in reactor
% solid_x_i_previous    = ;     %kg/m^3,    fixed,      Mass concentration array of solid species from previous reactor
% solid_x_i_injection   = ;     %kg/m^3,    fixed,      Mass concentration array of solid species from injection
%`gas_x_i               = ;     %percent_vol,dynamic,   Concentration array of gaseous species in reactor
% gas_x_i_previous      = ;     %percent_vol,fixed,     Concentration array of gaseous species from previous reactor
% gas_x_i_injection     = ;     %percent_vol,fixed,     Concentration array of gaseous species from injection
%`T                     = ;     %K,         dynamic,    Temperature of reactor
% T_previous            = ;     %K,         fixed,      Temperature from previous reactor
% T_injection           = ;     %K,         fixed,      Temperature of injection
%`T_wall                = ;     %K,         dynamic,    Temperature of reactor wall


% parameters:
% P         = ;         %Pa,            fixed,      Pressure of the reactor
% V         = ;         %m^3,           fixed,      Total volume of reactor
% k_0_rxn_1 = ;         %m^3/mol*s,     fixed,      Reaction rate preexponential term of rnx_1
% E_a_rxn_1 = ;         %J/mol,         fixed,      Reaction activation energy of rnx_1
% R         = 8.314;    %J/mol*K,       fixed,      Ideal gas constant
% gas_c_p_i = ;         %J/mol*K,       fixed,      Gas molar heat capacity array
% solid_c_p_i= ;        %J/mol*K,       fixed,      Solid molar heat capacity array
% gas_MW_i  = ;         %kg/mol,        fixed,      Gas molecular weight array (note: kg/mol)
% solid_MW_i= ;         %kg/mol,        fixed,      Solid molecular weight array (note: kg/mol)
% delta_t   = ;         %seconds,       fixed,      Length of a timestep

% outputs:
% Q_out         %m^3/s,         Flow out of the reactor
% dxi_solid_dt  %kg/m^3/s,      change in solid mass concentration array in reactor with time
% dxi_gas_dt    %percent_vol/s, change in gaseous concentration array in reactor with time
% dT_dt         %K/s,           change in Temperature with time
% dTwall_dt     %K/s,           change in wall Temperature with time


%%%%%%%%%%%%%%%%
% calculations %
%%%%%%%%%%%%%%%%

Q_injection_in = Q_injection_air_in + Q_injection_fuel_in; % m^3/s

% get mols and molar concentrations of gas and solids (assume solids are evenly dispersed)
gas_mols_i      = gas_x_i .* P*V / (R*T);
gas_C_i         = gas_mols_i ./ V;
solid_mols_i    = solid_x_i .* P*V / (R*T);
solid_C_i       = solid_mols_i ./ V;

% reaction of C + O2 => CO2
R_rxn_1 = k_0_rxn_1 * exp(-E_a_rxn_1/(R*T)) * prod(gas_C_i .^ rxn_1_gas_order) * prod(solid_C_i .^ rxn_1_solid_order) * V;  % mol/s
% apply throttle factor, that accounts for not all carbon particles being available for reaction
R_rxn_1 = R_rxn_1 * rxn_1_throttle_factor;



% calc generation of each species
gas_mols_i_gen_rate   = rxn_1_gas_stoic   .* R_rxn_1;  % mol/s
solid_mols_i_gen_rate = rxn_1_solid_stoic .* R_rxn_1;  % mol/s

% find incoming mols
gas_mols_i_previous_rate  = P * (gas_x_i_previous  .* Q_previous_in)  / (R * T_previous );  % mol/s
gas_mols_i_injection_rate = P * (gas_x_i_injection .* Q_injection_in) / (R * T_injection);  % mol/s
solid_mols_i_previous_rate  = solid_x_i_previous  ./ solid_MW_i .* Q_previous_in  ;  % mol/s
solid_mols_i_injection_rate = solid_x_i_injection ./ solid_MW_i .* Q_injection_fuel_in ;  % mol/s

% energy incoming
gas_energy_previous  = gas_mols_i_previous_rate'  * gas_c_p_i * (T_previous  - T);  % J/s
gas_energy_injection = gas_mols_i_injection_rate' * gas_c_p_i * (T_injection - T);  % J/s
solid_energy_previous  = solid_mols_i_previous_rate'  * solid_c_p_i * (T_previous  - T);  % J/s
solid_energy_injection = solid_mols_i_injection_rate' * solid_c_p_i * (T_injection - T);  % J/s
energy_previous = gas_energy_previous + solid_energy_previous;  % J/s
energy_injection = gas_energy_injection + solid_energy_injection;  % J/s

% energy generation
energy_gen = -H_rxn_1 * R_rxn_1; % J/s

% net energy change rate
energy_net_rate = energy_previous + energy_injection + energy_gen;  % J/s
% energy_net = energy_net_rate * delta_t; % J/timestep

% find a blended molar heat capacity for the current material
total_molar_concentration = P / (R*T); % mols/m^3
gas_molar_conc_i = gas_x_i .* total_molar_concentration; % mols/m^3
solid_molar_conc_i = solid_x_i ./ solid_MW_i; % mols/m^3
avg_c_p = (gas_molar_conc_i' * gas_c_p_i + solid_molar_conc_i' * solid_c_p_i) / (sum(gas_molar_conc_i) + sum(solid_molar_conc_i)); % J/mol*K

% find outgoing mols (needs to account for expanding volume, so requires solution to system of two equations for new mols and new temperature based on heat input)
n_current = P*V / (R*T); % mol
n_new = n_current - energy_net_rate / (T * avg_c_p) * delta_t; % mol (at next timestep)
% T_new = (n_new * T^2 * avg_c_p) / (n_current * T * avg_c_p - energy_net); % K
T_new = T + (energy_net_rate * delta_t) / (n_current * avg_c_p - energy_net_rate * delta_t/T); % K (at next timestep)
volume_expansion_molar_flowrate = (n_new - n_current) * delta_t; % mol/timestep
gas_mols_leaving_total_rate = sum(gas_mols_i_previous_rate + gas_mols_i_injection_rate) * delta_t + volume_expansion_molar_flowrate; % mol/timestep
volume_leaving_total_rate = gas_mols_leaving_total_rate * R * T / P; % m^3/timestep
gas_mols_leaving_i_rate = gas_x_i .* gas_mols_leaving_total_rate; % mol/timestep
solid_mols_leaving_i_rate = solid_molar_conc_i .* volume_leaving_total_rate; % mol/timestep

% mass balances to find new mol values, accume = in - out + gen
dmol_i_solid_dt = (solid_mols_i_previous_rate + solid_mols_i_injection_rate + solid_mols_i_gen_rate) * delta_t - solid_mols_leaving_i_rate; % mol/timestep
dmol_i_gas_dt = (gas_mols_i_previous_rate + gas_mols_i_injection_rate + gas_mols_i_gen_rate) * delta_t - gas_mols_leaving_i_rate; % mol/timestep
gas_molar_conc_new = gas_molar_conc_i + dmol_i_gas_dt; % mol/m^3 (at next timestep)

% TODO: IMPORTANT, NEED TO MAKE SURE SOME VALUES NEVER GO NEGATIVE!

gas_xi_new = gas_molar_conc_new / sum(gas_molar_conc_new); % mol/mol (at next timestep)

% calculate the change in variables we want to integrate
Q_out = gas_mols_leaving_total_rate * R * T / (P * delta_t);    %m^3/s
dxi_solid_dt = dmol_i_solid_dt .* solid_MW_i ./ V;   % kg/(m^3*timestep)
dxi_gas_dt = (gas_xi_new - gas_x_i);    %percent_vol/timestep
dT_dt = (T_new - T);         %K/timestep
dTwall_dt = 0 * T_wall;     %K/timestep,           change in wall Temperature with time





1 + 2