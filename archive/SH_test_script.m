% clear all;

m_dot_gas = 1.589e4;
gas_mol_fraction_j = [0.02078; 0.79; 0.1892; 2.9e-10; 4.38e-5];
m_dot_steam = 1.054e13;
T_gas_SH_in = 3032;
% T_gas_SH_i = [3000;3000;3000;3000;3000;3000;3000;3000;3000;3000];
T_gas_SH_i = [3031;3030;3029;3028;3027;3026;3025;3024;3023;3022];
T_pipe_SH_i = [900;900;900;900;900;900;900;900;900;900]; 
T_steam_SH_in = 625.3;
% T_steam_SH_i = [813;813;813;813;813;813;813;813;813;813;];
T_steam_SH_i = [626;627;628;629;630;631;632;633;634;635];

c_p_pipe_SH = 490;
mass_pipe_SH = 9.859502812140370e+02;
P_SH_avg = 1.697625010000000e+07;
s_layer_thickness = 1.000000000000000e-03;
epsilon_inf = 0.625000000000000;
k_flame_parameter = 0.750000000000000;
epsilon_SH = 0.600000000000000;
C_s_black_body = 5.670000000000000e-08;
radiation_cross_sectional_area_SH = 2.463788620800000e+02;
surface_area_outer_single_pipelength_SH = 2.477110045249208;
surface_area_inner_single_pipelength_SH = 2.018192815813566;
length_SH = 15.544800000000000;
width_SH = 15.849600000000000;
gas_volume_SH_i = 3.195567533829436e+03;
n_pipes_SH = 10;
cross_sectional_area_pipe_SH = 0.001897226810924;
internal_pipe_volume_SH_i = 20.181928158135655;
outer_diameter_SH = 0.060325000000000;
inner_diameter_SH = 0.049149000000000;
C_1_SH = 0.900000000000000;
C_2_SH = 0.950000000000000;
P_boiler = 101325;


%%

% calculate Q_radiation_gas, Q_convection_gas, Q_convection_steam
% sign convention is in direction of intended heat transfer (gas to steam), so variables are generally positive:
% Q_radiation_gas => positive is gas to pipe
% Q_convection_gas => positive is gas to pipe
% Q_convection_steam => positive is pipe to steam

% need to define/calculate:
% c_p_gas_SH_i, 
% mass_gas_SH_i, 
% mass_steam_SH_i, 

% calculated in the load parameters file:
% c_p_pipe_SH, 
% mass_pipe_SH, 
% P_SH_avg

% create the T_gas_previous_i array of temperatures for bulk flow 
T_gas_previous_i = zeros(size(T_gas_SH_i));
T_gas_previous_i(2:end) = T_gas_SH_i(1:end-1);
T_gas_previous_i(1) = T_gas_SH_in;

% create the T_steam_previous_i array of temperatures for bulk flow 
T_steam_previous_i = zeros(size(T_steam_SH_i));
T_steam_previous_i(2:end) = T_steam_SH_i(1:end-1);
T_steam_previous_i(1) = T_steam_SH_in;

Q_radiation_gas_i  = radiation_gas(T_pipe_SH_i, T_gas_SH_i, s_layer_thickness, epsilon_inf, k_flame_parameter, epsilon_SH, C_s_black_body, radiation_cross_sectional_area_SH);

P_boiler_i = P_boiler * ones(size(T_gas_SH_i));
P_SH_avg_i = P_SH_avg * ones(size(T_steam_SH_i));

% gas properties
c_p_gas_SH_i  = gas_property('Cp_mass_pt', P_boiler_i, T_gas_SH_i, gas_mol_fraction_j);
Pr_gas_SH_i   = gas_property('Pr_pt', P_boiler_i, T_gas_SH_i, gas_mol_fraction_j);
rho_gas_SH_i  = gas_property('rho_pt', P_boiler_i, T_gas_SH_i, gas_mol_fraction_j);
k_gas_SH_i    = gas_property('tc_pt', P_boiler_i, T_gas_SH_i, gas_mol_fraction_j);
mu_gas_SH_i   = gas_property('my_pt', P_boiler_i, T_gas_SH_i, gas_mol_fraction_j);
v_gas_SH_i    = m_dot_gas .* rho_gas_SH_i ./ (length_SH * width_SH);
mass_gas_SH_i = rho_gas_SH_i .* gas_volume_SH_i;

% pipe properties
Pr_gas_surface_temp_SH_i = gas_property('Pr_pt', P_boiler_i, T_pipe_SH_i, gas_mol_fraction_j);

% steam properties
Pr_steam_i          = steam_property('Pr_pt', P_SH_avg_i, T_steam_SH_i);
rho_steam_i         = steam_property('rho_pt',P_SH_avg_i, T_steam_SH_i);
k_steam             = steam_property('tc_pt', P_SH_avg_i, T_steam_SH_i);
mu_steam_i          = steam_property('my_pt', P_SH_avg_i, T_steam_SH_i);
c_p_steam_SH_i      = steam_property('Cp_pt', P_SH_avg_i, T_steam_SH_i); 
c_p_steam_in_SH     = steam_property('Cp_pt', P_SH_avg, T_steam_SH_in); % used the steam in temepratures because this is for the bulk heat flow
m_dot_steam_per_pipe = m_dot_steam / n_pipes_SH;
v_steam_i = m_dot_steam_per_pipe ./ (rho_steam_i .* cross_sectional_area_pipe_SH);
mass_steam_SH_i = rho_steam_i * internal_pipe_volume_SH_i;

c_p_steam_SH_previous_i = zeros(size(T_steam_previous_i));
c_p_steam_SH_previous_i(2:end) = c_p_steam_SH_i(1:end-1);
c_p_steam_SH_previous_i(1) = c_p_steam_in_SH;

% fill in with the correct variables:
Q_convection_gas_i = convection_gas_tubebank(surface_area_outer_single_pipelength_SH, outer_diameter_SH, C_1_SH, C_2_SH, T_gas_SH_i, T_pipe_SH_i, Pr_gas_SH_i, Pr_gas_surface_temp_SH_i, rho_gas_SH_i, v_gas_SH_i, k_gas_SH_i, mu_gas_SH_i); 

% calculate the heat transfer of a single pipe length in the super heater:

Q_convection_steam_per_pipe = convection_steam_tubebank(surface_area_inner_single_pipelength_SH, inner_diameter_SH, T_steam_SH_i, T_pipe_SH_i, Pr_steam_i, rho_steam_i, v_steam_i, k_steam, mu_steam_i);
Q_convection_steam = Q_convection_steam_per_pipe * n_pipes_SH;

Q_bulk_in_gas_i = m_dot_gas * c_p_gas_SH_i .* (T_gas_previous_i - T_gas_SH_i);
Q_bulk_in_steam_i = m_dot_steam .* c_p_steam_SH_previous_i .* (T_steam_previous_i - T_steam_SH_i);

% Q_bulk_in_gas_i = zeros(size(Q_bulk_in_gas_i));
Q_bulk_in_steam_i = zeros(size(Q_bulk_in_steam_i));
% Q_radiation_gas_i = zeros(size(Q_radiation_gas_i));
Q_convection_gas_i = zeros(size(Q_convection_gas_i));
Q_convection_steam = zeros(size(Q_convection_steam));

d_T_gas_SH_dt_i   = (Q_bulk_in_gas_i   - Q_radiation_gas_i - Q_convection_gas_i                     ) ./ (c_p_gas_SH_i   .* mass_gas_SH_i)
d_T_pipe_SH_dt_i  = (                    Q_radiation_gas_i + Q_convection_gas_i - Q_convection_steam) ./ (c_p_pipe_SH    .* mass_pipe_SH)
d_T_steam_SH_dt_i = (Q_bulk_in_steam_i                                          + Q_convection_steam) ./ (c_p_steam_SH_i .* mass_steam_SH_i)

x=1; % temp


