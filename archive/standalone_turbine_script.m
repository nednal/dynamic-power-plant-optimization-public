
%% conversion factors to get everything in base SI units:
pa_per_psi = 6894.76;
MW_per_W = 1e-6;

%% parameters (pressures are converted from psig to PA):

P_atm               = 101325;   % Pa, atmospheric pressure
P_HP_turbine_inlet  = P_atm + (2425) * pa_per_psi;   % Pa, atmospheric pressure plus 2425 psig
P_RH_turbine_inlet  = P_atm + (253) * pa_per_psi;   % Pa, atmospheric pressure plus 253 psig
P_MP_turbine_inlet  = P_atm + (253 - 15) * pa_per_psi;   % Pa, atmospheric pressure plus 253 psig minus 15 psig pressure drop
P_LP_turbine_inlet  = P_atm + 3/4 * (P_MP_turbine_inlet - P_atm); % need to choose a pressure. Let's just say it's 3/4 of the way in between the MP inlet pressure and atmospheric

T_steam_HP_in = 813; % K (1005 degF) after superheater
T_steam_MP_in = 813; % K (1005 degF) after reheater
m_dot_steam = 165.8; % kg/s (1,316,000 lb/hr)
x_steam_out = 0.9; % steam quality exiting the LP turbine is ~90%
HP_efficiency = 0.9;
MP_efficiency = 0.9;

%% HP turbine:

[HP_power, T_steam_HP_out] = turbine(T_steam_HP_in, P_HP_turbine_inlet, P_RH_turbine_inlet, m_dot_steam, HP_efficiency);
HP_power_MW = HP_power * MW_per_W % convert to megawatts

%% MP turbine:

[MP_power, T_steam_MP_out] = turbine(T_steam_MP_in, P_MP_turbine_inlet, P_LP_turbine_inlet, m_dot_steam, MP_efficiency);
MP_power_MW = MP_power * MW_per_W % convert to megawatts

%% LP turbine:

% P_steam_in = P_LP_turbine_inlet;
% P_steam_out = P_atm;
% efficiency = 0.9; % move this to load parameters file
% [LP_power, T_steam_LP_out] = turbine(T_steam_in, P_steam_in, P_steam_out, m_dot_steam, efficiency);
enthalpy_in = steam_property('h_pt', P_LP_turbine_inlet, T_steam_MP_out);
enthalpy_out = steam_property('h_px', P_atm, x_steam_out);
T_steam_LP_out = steam_property('Tsat_p', P_atm, 0);
LP_power = (enthalpy_in - enthalpy_out) * m_dot_steam;
LP_power_MW = LP_power * MW_per_W % convert to megawatts

%% total up the power from all the turbines:

Total_power_MW = HP_power_MW + MP_power_MW + LP_power_MW


%% generic turbine function:

function [power, T_steam_out] = turbine(T_steam_in, P_steam_in, P_steam_out, m_dot_steam, efficiency)

entropy_steam_in            = steam_property('s_pt', P_steam_in, T_steam_in);
entropy_steam_out           = entropy_steam_in;

enthalpy_steam_in           = steam_property('h_pt', P_steam_in, T_steam_in);
enthalpy_steam_ideal_out    = steam_property('h_ps', P_steam_out, entropy_steam_out);
enthalpy_steam_out          = enthalpy_steam_in + efficiency * (enthalpy_steam_ideal_out - enthalpy_steam_in);

T_steam_out                 = steam_property('T_ph', P_steam_out, enthalpy_steam_out);

power = (enthalpy_steam_in - enthalpy_steam_out) * m_dot_steam;

end