
##good source: https://nathanrooy.github.io/posts/2016-08-17/simple-particle-swarm-optimization-with-python/

import numpy as np
import multiprocessing
from joblib import Parallel, delayed
import matplotlib.pyplot as plt

num_cores = multiprocessing.cpu_count()
print('There are',num_cores,'cores on this machine')

class PSO:
    '''
    boundaries are "stick" type
    no neighborhoods
    i = particle
    j = problem dimension
    k = iteration

    to avoid scaling issues, wrap objective funciton with bounds [0-1] and rescale as needed
    '''
    
    def __init__(self,
                 j_max, # number of problem dimensions
                 obj_function, # objective function, should accept options between [0-1] as a single numpy array
                 k_max=2000, # maximum number of iterations (in case it hangs somehow)
                 i_max=100, # number of particles
                 w=0.5, # inertia weight
                 c1=1, # cognitive weight
                 c2=1, # social weight
                 tolerance=1e-5, # magnitude of change of global objective for terminating
                 tolerance_window=10, # number of iterations with 'tolerance' change of global objective for terminating
                 plotting=False): 

        self.i_max = i_max
        self.j_max = j_max
        self.k_max = k_max
        self.obj_function = obj_function
        self.w = w
        self.c1 = c1
        self.c2 = c2
        self.tolerance = tolerance
        self.tolerance_window = tolerance_window
        self.plotting = plotting 
        
        
        # initalize everything:
        self.k = 0
        self.function_evals = 0
        self.x = np.random.rand(self.i_max, self.j_max) # 2-D vector, particle positions
        self.v = np.random.rand(self.i_max, self.j_max)*2-1 # 2-D vector, particle velocities
        self.x_best_i = np.copy(self.x) # 2-D vector, particle best positions
        self.obj_best_i = [self.obj_function(self.x_best_i[i]) for i in range(self.i_max)] # 1-D vector, particle best objective values
        
        i_global_best = np.argmin(self.obj_best_i)
        self.x_best_global = self.x_best_i[i_global_best] # 1-D vector, global best position
        self.obj_best_global = self.obj_best_i[i_global_best] # global best objective value
        self.obj_best_global_array = [self.obj_best_global]

    def optimize(self, output=None, parallel=False):
        exit_flag = False
        while not exit_flag: 
            # determine velocities
            self.v = (self.w * self.v + 
                      self.c1 * np.random.rand(self.i_max, self.j_max) * (self.x_best_i - self.x) +
                      self.c2 * np.random.rand(self.i_max, self.j_max) * (self.x_best_global - self.x) )
            # update positions (apply stick condition)
            self.x += self.v
            self.x[self.x < 0] = 0
            self.x[self.x > 1] = 1
            # determine objective values and update best values
            if parallel:
                def evaluate(i):
                    objective_value = self.obj_function(self.x[i])
                    if objective_value < self.obj_best_i[i]:
                        self.obj_best_i[i] = objective_value
                        self.x_best_i[i] = self.x[i]
                def evaluate2(i,f,x):
                    objective_value = f(self.x[i])
                    if objective_value < self.obj_best_i[i]:
                        self.obj_best_i[i] = objective_value
                        self.x_best_i[i] = self.x[i]
                # do in parallel 
                Parallel(n_jobs=num_cores)(delayed(evaluate)(i) for i in range(self.i_max))
                # update global best
                i_global_best = np.argmin(self.obj_best_i)
                self.x_best_global = self.x_best_i[i_global_best]
                self.obj_best_global = self.obj_best_i[i_global_best]
            else:
                for i in range(self.i_max):
                    objective_value = self.obj_function(self.x[i])
                    if objective_value < self.obj_best_i[i]:
                        self.obj_best_i[i] = objective_value
                        self.x_best_i[i] = self.x[i]
                    if objective_value < self.obj_best_global:
                        self.obj_best_global = objective_value
                        self.x_best_global = self.x[i]
            self.obj_best_global_array.append(self.obj_best_global)

            # increment
            self.function_evals += self.i_max
            self.k += 1
            
            # check for termination
            if self.k < self.tolerance_window:
                exit_flag = False
            elif self.k >= self.k_max:
                exit_flag = True
            else:
                exit_flag = self.obj_best_global_array[-1] + self.tolerance > + self.obj_best_global_array[-self.tolerance_window]
                
            # display output if requested 
            if output=='Full':
                self.output()
            elif output=='Light':
                print('iteration:',self.k,'of',self.k_max)
                
        # return results of the solution and the objective value
        return self.x_best_global, self.obj_best_global, self.k, self.function_evals

    def output(self):
        print('')
        print('-'*10,'OUTPUT iteration',self.k,'-'*10)
        print('x:',self.x)
        print('v:',self.v)
        print('x_best_i:',self.x_best_i)
        print('obj_best_i:',self.obj_best_i)
        print('x_best_global:',self.x_best_global)
        print('obj_best_global:',self.obj_best_global)

    def plot(self):
        if self.plotting:
            plt.plot(np.linspace(0,self.k,self.k+1), self.obj_best_global_array)
            plt.show()
        else:
            print('plotting disabled!')




class ACO:
    '''
    attractiveness factors are all 1, since there aren't known edge distances
    need to provide a matrix of allowable one-way decisons that connect row/column decisions/augmented by start/end points
    '''
    def __init__(self,
                 j_max, # number of problem dimensions
                 obj_function, # objective function, should accept options between [0-1] as a single numpy array
                 k_max=2000, # maximum number of iterations (in case it hangs somehow)
                 i_max=10, # number of ants
                 Q=1e-2, # magnitude of pheromone deposit relative to solution value
                 rho=0.75, # evaporation constant
                 discretizations=10, # the probelm inputs are normalized between 0-1, number of discretized deciions in that range
                 tolerance=1e-5, # magnitude of change of global objective for terminating
                 tolerance_window=10, # number of iterations with 'tolerance' change of global objective for terminating
                 plotting=False):

        self.j_max = j_max
        self.obj_function = obj_function
        self.k_max = k_max
        self.Q = Q
        self.rho = rho
        self.i_max = i_max
        self.discretizations = discretizations
        self.tolerance = tolerance
        self.tolerance_window = tolerance_window
        self.plotting = plotting 
        
        # initalize everything:
        self.k = 0
        self.function_evals = 0
        self.pheromone_matrix = np.ones([j_max,discretizations])
        # rescale pheromones (can run into numerical error issues with them getting too close to zero)
        for j in range(self.j_max):
            self.pheromone_matrix[j,:] /= np.sum(self.pheromone_matrix[j,:])
        self.input_lookup_values = np.linspace(0,1,discretizations)
        self.x = np.zeros([i_max, j_max], dtype=int) # dtype is important to use these an indecies
        self.ant_values = np.zeros(i_max)
        self.obj_best_global = None
        self.obj_best_global_array = []
        self.x_best_global = []


    def optimize(self, output=None):
        exit_flag = False
        while not exit_flag:
            # determine each ant's decisions
            for j in range(self.j_max):
                # rescale so the random choice propabilities add up to 1
                p = self.pheromone_matrix[j,:] / np.sum(self.pheromone_matrix[j,:])
##                self.x[:,j] = np.random.choice(self.discretizations, self.i_max, p=self.pheromone_matrix[j,:], replace=True)
                self.x[:,j] = np.random.choice(self.discretizations, self.i_max, p=p, replace=True)

            # evalutate each ant
            for i in range(self.i_max):
                self.ant_values[i] = self.obj_function(self.input_lookup_values[self.x[i,:]])
            # update global best array
            current_best = np.min(self.ant_values)
            current_best_ant_index = np.argmin(self.ant_values)
            if self.obj_best_global_array ==[] or current_best < self.obj_best_global_array[-1]:
                self.obj_best_global = current_best
                self.obj_best_global_array.append(current_best)
                self.x_best_global = self.input_lookup_values[self.x[current_best_ant_index]]
            else:
                self.obj_best_global_array.append(self.obj_best_global_array[-1])
                
            # update pheromones
            for j in range(self.j_max):
                for d in range(self.discretizations):
                    # evaporate once for each node
                    self.pheromone_matrix[j,d] = (1-self.rho)*self.pheromone_matrix[j,d]
                    # reset any trails below the baseline of 1
                    self.pheromone_matrix[self.pheromone_matrix<1]=1
                    for i in range(self.i_max):
                        # deposit pheromones from each ant who used the node
                        if self.x[i,j] == d:
                            self.pheromone_matrix[j,d] += self.Q / self.ant_values[i]
            # rescale pheromones (can run into numerical error issues with them getting too close to zero)
##            for j in range(self.j_max):
##                self.pheromone_matrix[j,:] /= np.sum(self.pheromone_matrix[j,:])
            
            # increment
            self.function_evals += self.i_max
            self.k += 1
            
            # check for termination
            if self.k < self.tolerance_window:
                exit_flag = False
            elif self.k >= self.k_max:
                exit_flag = True
            else:
                exit_flag = self.obj_best_global_array[-1] + self.tolerance > + self.obj_best_global_array[-self.tolerance_window]

            # display output if requested 
            if output=='Full':
                self.output()
            elif output=='Light':
                print('iteration:',self.k,'of',self.k_max)
            
        return self.x_best_global, self.obj_best_global, self.k, self.function_evals

    def output(self):
        print('')
        print('-'*10,'OUTPUT iteration',self.k,'-'*10)
        print('x:',self.x)
        print('pheromone_matrix:',self.pheromone_matrix)
        print('x_best_global:',self.x_best_global)
        print('obj_best_global:',self.obj_best_global)

    def plot(self):
        if self.plotting:
            plt.plot(np.linspace(1,self.k,self.k), self.obj_best_global_array)
            plt.show()
        else:
            print('plotting disabled!')


class GA:
    '''
    https://machinelearningmastery.com/simple-genetic-algorithm-from-scratch-in-python/
    '''
    
    def __init__(self,
                 j_max, # number of problem dimensions
                 obj_function, # objective function, should accept options between [0-1] as a single numpy array
                 k_max=2000, # maximum number of iterations (in case it hangs somehow)
                 i_max=10, # population size (should be even)
                 c=0.9, # cross over rate
                 m=0.1, # mutation rate
                 bit_count=16, # number of bits per variable
                 tournament_size = 3, # size of tournament for selecting parents
                 tolerance=1e-5, # magnitude of change of global objective for terminating
                 tolerance_window=10, # number of iterations with 'tolerance' change of global objective for terminating
                 plotting=False,
                 elitism=False, # use Elitism (best parents survive) 
                 elitism_count=2, # should be even number and less than i_max
                 cross_over_type='single' # options are single, double, gene
                 ):

        self.j_max = j_max
        self.obj_function = obj_function
        self.k_max = k_max
        self.i_max = i_max
        self.c = c
        self.m = m
        self.bit_count = bit_count
        self.tournament_size = tournament_size
        self.tolerance = tolerance
        self.tolerance_window = tolerance_window
        self.plotting = plotting
        
        self.elitism=elitism
        self.elitism_count=elitism_count
        if cross_over_type == 'double':
            self.breed = self.breed_double
        elif cross_over_type == 'gene':
            self.breed = self.breed_gene
        else:
            self.breed = self.breed_single
        
        # initalize everything:
        self.k = 0
        self.function_evals = 0
        self.max_value = 2**bit_count-1
        self.x = np.random.randint(2, size=[i_max,j_max,bit_count])
        
        self.obj_best_global = None
        self.obj_best_global_array = []
        self.x_best_global = []

    def decode(self, i):
        decoded = np.zeros(self.j_max)
        for j in range(self.j_max):
            bitstring = ''.join([str(bit) for bit in self.x[i,j]])
            decoded[j] = int(bitstring,2)/self.max_value
        return decoded

    def decode_all(self):
        decoded_array = np.zeros([self.i_max,self.j_max])
        for i in range(self.i_max):
            decoded_array[i,:] = self.decode(i)
        return decoded_array

    # single crossover for all points at once
    def breed_single(self, parent_1, parent_2):
        original_shape = np.shape(parent_1)
        #unfold parents DNA
        parent_1 = np.reshape(parent_1, [np.size(parent_1)])#, order='F')
        parent_2 = np.reshape(parent_2, [np.size(parent_2)])#, order='F')

        
        # crossover
        if np.random.rand() <= self.c:
            crossover_index = np.random.randint(1,len(parent_1)-1)
            child_1 = np.hstack([parent_1[:crossover_index], parent_2[crossover_index:]])
            child_2 = np.hstack([parent_2[:crossover_index], parent_1[crossover_index:]])
        else:
            child_1 = np.copy(parent_1)
            child_2 = np.copy(parent_2)

        # mutation
        mutation_array_1 = np.random.rand(len(child_1)) <= self.m
        child_1 = np.logical_not(child_1, where=mutation_array_1)
        
        mutation_array_2 = np.random.rand(len(child_2)) <= self.m
        child_2 = np.logical_not(child_2, where=mutation_array_2)

        # reshape DNA back to 2D matrix
        child_1 = np.reshape(child_1, original_shape)#, order='F')
        child_2 = np.reshape(child_2, original_shape)#, order='F')
        
        return child_1, child_2

    # double crossover for all points at once
    def breed_double(self, parent_1, parent_2):
        original_shape = np.shape(parent_1)
        #unfold parents DNA
        parent_1 = np.reshape(parent_1, [np.size(parent_1)])
        parent_2 = np.reshape(parent_2, [np.size(parent_2)])
        
        # crossover
        if np.random.rand() <= self.c:
            crossover_index_1 = np.random.randint(0,len(parent_1)-2)
            crossover_index_2 = np.random.randint(crossover_index_1+1,len(parent_1))
            child_1 = np.hstack([parent_1[:crossover_index_1],
                                 parent_2[crossover_index_1:crossover_index_2],
                                 parent_1[crossover_index_2:]
                                 ])
            child_2 = np.hstack([parent_2[:crossover_index_1],
                                 parent_1[crossover_index_1:crossover_index_2],
                                 parent_2[crossover_index_2:]
                                 ])
        else:
            child_1 = np.copy(parent_1)
            child_2 = np.copy(parent_2)

        # mutation
        mutation_array_1 = np.random.rand(len(child_1)) <= self.m
        child_1 = np.logical_not(child_1, where=mutation_array_1)
        
        mutation_array_2 = np.random.rand(len(child_2)) <= self.m
        child_2 = np.logical_not(child_2, where=mutation_array_2)

        # reshape DNA back to 2D matrix
        child_1 = np.reshape(child_1, original_shape)
        child_2 = np.reshape(child_2, original_shape)
        
        return child_1, child_2


    # single crossover for each point
    def breed_gene(self, parent_1, parent_2):

        child_1 = np.copy(parent_1)
        child_2 = np.copy(parent_2)
        
        # crossover
        for j in range(self.j_max):
            if np.random.rand() <= self.c:
                crossover_index = np.random.randint(1,len(parent_1[j])-2)
                child_1[j] = np.hstack([parent_1[j,:crossover_index], parent_2[j,crossover_index:]])
                child_2[j] = np.hstack([parent_2[j,:crossover_index], parent_1[j,crossover_index:]])

        # mutation
        mutation_array_1 = np.random.rand(*np.shape(child_1)) <= self.m
        child_1 = np.logical_not(child_1, where=mutation_array_1)
        
        mutation_array_2 = np.random.rand(*np.shape(child_2)) <= self.m
        child_2 = np.logical_not(child_2, where=mutation_array_2)
        
        return child_1, child_2


    def tournament_selection(self, fitness_array):
        selected_indexes = np.random.randint(self.i_max,size=self.tournament_size)
        selected_fitness_array = [fitness_array[i] for i in selected_indexes]
        return selected_indexes[np.argmin(selected_fitness_array)]

    def optimize(self, output=None):
        exit_flag = False
        while not exit_flag:
            # decode and evaluate solutions
            self.x_decoded = self.decode_all()
            self.fitness_array = np.array([self.obj_function(self.x_decoded[i]) for i in range(self.i_max)])
            
            # find best solution and update global best
            if self.obj_best_global is None or np.min(self.fitness_array) < self.obj_best_global:
                best_index = np.argmin(self.fitness_array)
                self.x_best_global = self.decode(best_index)
                self.obj_best_global = self.fitness_array[best_index]
            self.obj_best_global_array.append(self.obj_best_global)

            if self.elitism:
                # with elitism (best elitism_count parents; should be an even number)
                next_gen = np.zeros([self.i_max,self.j_max,self.bit_count],dtype=int)
                for i in range(0, self.i_max-self.elitism_count, 2):
                    parent_1_i = self.tournament_selection(self.fitness_array)
                    parent_2_i = self.tournament_selection(self.fitness_array)
                    child_1, child_2 = self.breed(self.x[parent_1_i], self.x[parent_2_i])
                    next_gen[i] = child_1
                    if i+1<self.i_max: # deal with odd population sizes
                        next_gen[i+1] = child_2
                for e_counter in range(self.elitism_count):
                    next_gen[-e_counter] = self.x[np.argpartition(self.fitness_array, e_counter)[e_counter]]
            else:
                # determine next generation (no elitism)
                next_gen = np.zeros([self.i_max,self.j_max,self.bit_count],dtype=int)
                for i in range(0, self.i_max, 2):
                    parent_1_i = self.tournament_selection(self.fitness_array)
                    parent_2_i = self.tournament_selection(self.fitness_array)
                    child_1, child_2 = self.breed(self.x[parent_1_i], self.x[parent_2_i])
                    next_gen[i] = child_1
                    if i+1<self.i_max: # deal with odd population sizes
                        next_gen[i+1] = child_2

            # replace generation
            self.x = next_gen
            
            # increment
            self.function_evals += self.i_max
            self.k += 1
            
            # check for termination
            if self.k < self.tolerance_window:
                exit_flag = False
            elif self.k >= self.k_max:
                exit_flag = True
            else:
                exit_flag = self.obj_best_global_array[-1] + self.tolerance > + self.obj_best_global_array[-self.tolerance_window]

            # display output if requested 
            if output=='Full':
                self.output()
            elif output=='Light':
                print('iteration:',self.k,'of',self.k_max)
            
        return self.x_best_global, self.obj_best_global, self.k, self.function_evals

    def output(self):
        print('')
        print('-'*10,'OUTPUT iteration',self.k,'-'*10)
        print('x_decoded:',self.x_decoded)
        print('fitness_array:',self.fitness_array)
        print('x_best_global:',self.x_best_global)
        print('obj_best_global:',self.obj_best_global)

    def plot(self):
        if self.plotting:
            plt.plot(np.linspace(1,self.k,self.k), self.obj_best_global_array)
            plt.show()
        else:
            print('plotting disabled!')


# numerical gradient (might help for gradient decent): https://stackoverflow.com/questions/38854363/is-there-any-standard-way-to-calculate-the-numerical-gradient/38859687







        
if __name__ == "__main__":
    
    def f(x_normalized):
        x = x_normalized * 10
        return np.sum( (x-5)**2 * ((x-2)**2+1) )

##    dimensions = 5
##    pso = PSO(dimensions,f,
##              i_max=50, # number of particles
##              w=0.5, # inertia weight
##              c1=1, # cognitive weight
##              c2=1, # social weight
##              plotting=True)
##    x_best_global, obj_best_global, iterations, function_evals = pso.optimize(output='Light')

    dimensions = 10
    aco = ACO(dimensions,f,
              i_max=200, # number of particles
              Q=2, # magnitude of pheromone deposit relative to solution value
              rho=0.99, # evaporation constant
              discretizations=10, # the probelm inputs are normalized between 0-1, number of discretized deciions in that range
              tolerance_window=1000,
              plotting=True)
    x_best_global, obj_best_global, iterations, function_evals = aco.optimize(output='Light')

##    dimensions = 10
##    bit_count=16
##    ga =   GA(dimensions,f,
##              i_max=15, # population size
##              c=0.90, # cross over rate
##              m=1/(dimensions), # mutation rate
##              bit_count=bit_count, # number of bits per variable
##              tournament_size = 3, # size of tournament for selecting parents
##              k_max=10, # maximum number of iterations (in case it hangs somehow)
##              tolerance_window=10,
##              plotting=True)
##    x_best_global, obj_best_global, iterations, function_evals = ga.optimize(output='Light')
    
    print('x_best_global:',x_best_global)
    print('obj_best_global:',obj_best_global)
    print('iterations:',iterations)
    print('function_evals:',function_evals)
    
##    pso.plot()
    aco.plot()
##    ga.plot()
   
