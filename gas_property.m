% property_string = 'Cp_mass_pt';
% property_string = 'Cp_mol_pt';
% property_string = 'Pr_pt';
% P = 101325;
% T_i = 3250 * ones(4,1);
% T_i = 3250 * ones(10,1);
% mol_fraction_ji = [0.02, 0.79, 0.18, 0.005, 0.005; ...
%                    0.02, 0.79, 0.18, 0.005, 0.005; ...
%                    0.02, 0.79, 0.18, 0.005, 0.005; ...
%                    0.02, 0.79, 0.18, 0.005, 0.005]';
% mol_fraction_ji = [1, 0, 0, 0, 0; ...
%                    0, 1, 0, 0, 0; ...
%                    0.5, 0.5, 0, 0, 0; ...
%                    0.5, 0.5, 0, 0, 0; ...
%                    0.5, 0.5, 0, 0, 0; ...
%                    0.5, 0.5, 0, 0, 0; ...
%                    0.5, 0.5, 0, 0, 0; ...
%                    0.5, 0.5, 0, 0, 0; ...
%                    0.5, 0.5, 0, 0, 0; ...
%                    0.5/MW_j(1) / sum([0.5;0.5]./MW_j(1:2)), 0.5/MW_j(2) / sum([0.5;0.5]./MW_j(1:2)), 0, 0, 0]';
% mol_fraction_ji = [1, 0, 0, 0, 0, 0, 0; ...
%                    0, 1, 0, 0, 0, 0, 0; ...
%                    0.5, 0.5, 0, 0, 0, 0, 0; ...
%                    0.5, 0.5, 0, 0, 0, 0, 0; ...
%                    0.5, 0.5, 0, 0, 0, 0, 0; ...
%                    0.5, 0.5, 0, 0, 0, 0, 0; ...
%                    0.5, 0.5, 0, 0, 0, 0, 0; ...
%                    0.5, 0.5, 0, 0, 0, 0, 0; ...
%                    0.5, 0.5, 0, 0, 0, 0, 0; ...
%                    0.5, 0.5, 0, 0, 0, 0, 0]';

% mol_fraction_ji = [0.5; 0.5; 0; 0; 0];


% test: 
% gas_property2(property_string, P, T_i, mol_fraction_ji)


function property = gas_property2(property_string, P, T_i, mol_fraction_ji)


%% define property data
% note: csv reader can't be compiled 
% note 2: simulink has a separate global variable space that is difficult to work with

% global tabulated_Cp_O2_mass tabulated_Cp_N2_mass tabulated_Cp_CO2_mass tabulated_Cp_CO_mass single_value_Cp_C_mass;
% global tabulated_Cp_O2_mol tabulated_Cp_N2_mol tabulated_Cp_CO2_mol tabulated_Cp_CO_mol single_value_Cp_C_mol;
% global single_value_tc_O2 single_value_tc_N2 single_value_tc_CO2 single_value_tc_CO single_value_tc_C;
% global single_value_mu_O2 single_value_mu_N2 single_value_mu_CO2 single_value_mu_CO single_value_mu_C;

% is_gas_j = [1;1;1;1;0]; % [O2 N2 CO2 CO C]
% gas_c_p_j = [29.4071; 29.1335; 37.1444; 28.5702; 10.68]; % J/mol*K,  Gas molar heat capacity array, source is in kg, so covert to mols: https://www.engineeringtoolbox.com/specific-heat-capacity-gases-d_159.html
% gas_c_p_j_mass = [919; 1040; 844; 1020; 710]; % J/mol*K, same source as above, but note that this is the carbon source for both: https://www.engineeringtoolbox.com/specific-heat-solids-d_154.html

% tabulated_Cp_O2_mass    = [175,910;200,910;225,911;250,913;275,915;300,918;325,923;350,928;375,934;400,941;450,956;500,972;550,988;600,1003.00000000000;650,1017.00000000000;700,1031;750,1043;800,1054;850,1065;900,1074;950,1082;1000,1090;1050,1097;1100,1103;1150,1109;1200,1115;1250,1120;1300,1125;1350,1130;1400,1134;1500,1143;1600,1151;1700,1158;1800,1166;1900,1173;2000,1181;2100,1188;2200,1195;2300,1202;2400,1209;2500,1216;2600,1223;2700,1230;2800,1236;2900,1243;3000,1249;3500,1276;4000,1299;4500,1316;5000,1328;5500,1337;6000,1344];
% tabulated_Cp_N2_mass    = [175,1039;200,1039;225,1039;250,1039;275,1039;300,1040;325,1040;350,1041;375,1042;400,1044;450,1049;500,1056;550,1065;600,1075;650,1086;700,1098;750,1110;800,1122;850,1134;900,1146;950,1157;1000,1167;1050,1177;1100,1187;1150,1196;1200,1204;1250,1212;1300,1219;1350,1226;1400,1232;1500,1244;1600,1254;1700,1263;1800,1271;1900,1278;2000,1284;2100,1290;2200,1295;2300,1300;2400,1304;2500,1307;2600,1311;2700,1314;2800,1317;2900,1320;3000,1323;3500,1333;4000,1342;4500,1349;5000,1355;5500,1362;6000,1369];
% tabulated_Cp_CO2_mass   = [175,709;200,735;225,763;250,791;275,819;300,846;325,871;350,895;375,918;400,939;450,978;500,1014;550,1046;600,1075;650,1102;700,1126;750,1148;800,1168;850,1187;900,1204;950,1220;1000,1234;1050,1247;1100,1259;1150,1270;1200,1280;1250,1290;1300,1298;1350,1306;1400,1313;1500,1326;1600,1338;1700,1348;1800,1356;1900,1364;2000,1371;2100,1377;2200,1383;2300,1388;2400,1393;2500,1397;2600,1401;2700,1404;2800,1408;2900,1411;3000,1414;3500,1427;4000,1437;4500,1446;5000,1455;5500,1465;6000,1476];
% tabulated_Cp_CO_mass    = [175,1039;200,1039;225,1039;250,1039;275,1040;300,1040;325,1041;350,1043;375,1045;400,1048;450,1054;500,1064;550,1075;600,1087;650,1100;700,1113;750,1126;800,1139;850,1151;900,1163;950,1174;1000,1185;1050,1194;1100,1203;1150,1212;1200,1220;1250,1227;1300,1234;1350,1240;1400,1246;1500,1257;1600,1267;1700,1275;1800,1282;1900,1288;2000,1294;2100,1299;2200,1304;2300,1308;2400,1311;2500,1315;2600,1318;2700,1321;2800,1324;2900,1326;3000,1329;3500,1339;4000,1346;4500,1353;5000,1359;5500,1365;6000,1370];
% single_value_Cp_C_mass  = [1380];
% https://www.engineeringtoolbox.com/specific-heat-capacity-gases-d_159.html
tabulated_Cp_O2_mass    = [298,919;6000,919];
tabulated_Cp_N2_mass    = [298,1040;6000,1040];
tabulated_Cp_CO2_mass   = [298,844;6000,844];
tabulated_Cp_CO_mass    = [298,1020;6000,1020];
single_value_Cp_C_mass  = [710];
tabulated_Cp_H2_mass    = [298,14320;6000,14320]; 
tabulated_Cp_H2O_mass   = [298,1930;6000,1930]; 

% tabulated_Cp_O2_mol     = [175,28.4392774548409;200,28.4392774548409;225,28.4705294080880;250,28.5330333145822;275,28.5955372210763;300,28.6892930808176;325,28.8455528470529;350,29.0018126132883;375,29.1893243327708;400,29.4080880055003;450,29.8768673042065;500,30.3768985561598;550,30.8769298081130;600,31.3457091068192;650,31.7832364522783;700,32.2207637977374;750,32.5957872367023;800,32.9395587224202;850,33.2833302081380;900,33.5645977873617;950,33.8146134133383;1000,34.0646290393150;1050,34.2833927120445;1100,34.4709044315270;1150,34.6584161510094;1200,34.8459278704919;1250,35.0021876367273;1300,35.1584474029627;1350,35.3147071691981;1400,35.4397149821864;1500,35.7209825614101;1600,35.9709981873867;1700,36.1897618601163;1800,36.4397774860929;1900,36.6585411588224;2000,36.9085567847990;2100,37.1273204575286;2200,37.3460841302581;2300,37.5648478029877;2400,37.7836114757172;2500,38.0023751484468;2600,38.2211388211763;2700,38.4399024939059;2800,38.6274142133883;2900,38.8461778861179;3000,39.0336896056004;3500,39.8774923432715;4000,40.5962872679542;4500,41.1275704731546;5000,41.5025939121195;5500,41.7838614913432;6000,42.0026251640728];
% tabulated_Cp_N2_mol     = [175,37.0893929333819;200,37.0893929333819;225,37.0893929333819;250,37.0893929333819;275,37.0893929333819;300,37.1250901354352;325,37.1250901354352;350,37.1607873374885;375,37.1964845395418;400,37.2678789436484;450,37.4463649539149;500,37.6962453682880;550,38.0175201867678;600,38.3744922073008;650,38.7671614298871;700,39.1955278545268;750,39.6238942791664;800,40.0522607038060;850,40.4806271284457;900,40.9089935530853;950,41.3016627756716;1000,41.6586347962047;1050,42.0156068167377;1100,42.3725788372707;1150,42.6938536557505;1200,42.9794312721769;1250,43.2650088886033;1300,43.5148893029764;1350,43.7647697173496;1400,43.9789529296694;1500,44.4073193543090;1600,44.7642913748420;1700,45.0855661933218;1800,45.3711438097482;1900,45.6210242241213;2000,45.8352074364411;2100,46.0493906487610;2200,46.2278766590275;2300,46.4063626692940;2400,46.5491514775072;2500,46.6562430836671;2600,46.7990318918803;2700,46.9061234980402;2800,47.0132151042001;2900,47.1203067103600;3000,47.2273983165200;3500,47.5843703370530;4000,47.9056451555327;4500,48.1555255699058;5000,48.3697087822257;5500,48.6195891965988;6000,48.8694696109719];
% tabulated_Cp_CO2_mol    = [175,16.1104508881198;200,16.7012431632836;225,17.3374809980754;250,17.9737188328671;275,18.6099566676589;300,19.2234717226367;325,19.7915412179864;350,20.3368879335222;375,20.8595118692440;400,21.3366902453379;450,22.2228786580835;500,23.0408987313872;550,23.7680276854349;600,24.4269883000407;650,25.0405033550184;700,25.5858500705542;750,26.0857512264620;800,26.5402068227419;850,26.9719396392077;900,27.3582268960456;950,27.7217913730694;1000,28.0399102904653;1050,28.3353064280472;1100,28.6079797858151;1150,28.8579303637690;1200,29.0851581619089;1250,29.3123859600488;1300,29.4941681985607;1350,29.6759504370727;1400,29.8350098957706;1500,30.1304060333525;1600,30.4030793911204;1700,30.6303071892603;1800,30.8120894277722;1900,30.9938716662842;2000,31.1529311249821;2100,31.2892678038661;2200,31.4256044827500;2300,31.5392183818200;2400,31.6528322808899;2500,31.7437234001459;2600,31.8346145194018;2700,31.9027828588438;2800,31.9936739780998;2900,32.0618423175418;3000,32.1300106569837;3500,32.4254067945656;4000,32.6526345927055;4500,32.8571396110315;5000,33.0616446293574;5500,33.2888724274973;6000,33.5388230054512];
% tabulated_Cp_CO_mol     = [175,37.0942923344413;200,37.0942923344413;225,37.0942923344413;250,37.0942923344413;275,37.1299942519913;300,37.1299942519913;325,37.1656961695413;350,37.2371000046413;375,37.3085038397412;400,37.4156095923912;450,37.6298210976912;500,37.9868402731911;550,38.3795613662410;600,38.8079843768409;650,39.2721093049908;700,39.7362342331407;750,40.2003591612906;800,40.6644840894404;850,41.0929071000403;900,41.5213301106402;950,41.9140512036902;1000,42.3067722967401;1050,42.6280895546900;1100,42.9494068126399;1150,43.2707240705898;1200,43.5563394109898;1250,43.8062528338397;1300,44.0561662566896;1350,44.2703777619896;1400,44.4845892672895;1500,44.8773103603395;1600,45.2343295358394;1700,45.5199448762393;1800,45.7698582990892;1900,45.9840698043892;2000,46.1982813096891;2100,46.3767908974391;2200,46.5553004851891;2300,46.6981081553890;2400,46.8052139080390;2500,46.9480215782390;2600,47.0551273308889;2700,47.1622330835389;2800,47.2693388361889;2900,47.3407426712889;3000,47.4478484239388;3500,47.8048675994388;4000,48.0547810222887;4500,48.3046944451386;5000,48.5189059504386;5500,48.7331174557385;6000,48.9116270434885];
% single_value_Cp_C_mol   = [114.897549684864];
% https://www.engineeringtoolbox.com/specific-heat-capacity-gases-d_159.html
tabulated_Cp_O2_mol     = [298,29.4071;6000,29.4071];
tabulated_Cp_N2_mol     = [298,29.1335;6000,29.1335];
tabulated_Cp_CO2_mol    = [298,37.1444;6000,37.1444];
tabulated_Cp_CO_mol     = [298,28.5702;6000,28.5702];
single_value_Cp_C_mol   = [10.68];
tabulated_Cp_H2_mol     = [298,28.8645;6000,28.8645]; 
tabulated_Cp_H2O_mol    = [298,34.7683;6000,34.7683]; 

% https://www.engineersedge.com/heat_transfer/thermal-conductivity-gases.htm
single_value_tc_O2      = [0.0481000000000000];
single_value_tc_N2      = [0.0440000000000000];
single_value_tc_CO2     = [0.0416000000000000];
single_value_tc_CO      = [0.0457000000000000];
% single_value_tc_C 
% single_value_tc_H2      = [0.2304000000000000];
single_value_tc_H2O     = [0.0471000000000000];

% source?
single_value_mu_O2      = [4.91200000000000e-05];
single_value_mu_N2      = [4.15400000000000e-05];
single_value_mu_CO2     = [4.26900000000000e-05];
single_value_mu_CO      = [3.86000000000000e-05];
% single_value_mu_C 
% https://www.engineeringtoolbox.com/gases-absolute-dynamic-viscosity-d_1888.html
% single_value_mu_H2      = [1.84000000000000e-05];
single_value_mu_H2O     = [1.30000000000000e-05];


%%




% calculates an array of values given an array of inputs for these properties:
% viscocity
% thermal conductivity
% heat capacity

% species: [O2 N2 CO2 CO C] 
% for everything except heat capacity, assume carbon is negligible

R = 8.314; % ideal gas constant

is_gas_j = logical([1; 1; 1; 1; 0; 0; 1]); % [O2 N2 CO2 CO C H2 H20] % let's just say hydrogen isn't a gas since it's trapped in the coal. Also, that way the reaction expands instead of contracts mol-wise
O_MW = 15.999;
N_MW = 14.0067;
C_MW = 12.0107;
H_MW = 1.00784;
MW_j = [O_MW*2; N_MW*2; C_MW+O_MW*2; C_MW+O_MW; C_MW; 2*H_MW; 2*H_MW+O_MW];
j_max = length(is_gas_j);

MW_gas_j = MW_j(is_gas_j);
j_gas_max = sum(is_gas_j); %j_max-1;

i_max = length(T_i);
% property = zeros([i_max 1]);

% determine if the mol fraciton needs to be repmat for all i indexes
mol_fraction_size = size(mol_fraction_ji);
if mol_fraction_size(2) > 1
    mol_fraction_ij = mol_fraction_ji';
    mol_gas_fraction_ji = mol_fraction_ji(is_gas_j,:);
    mol_gas_fraction_ij = mol_gas_fraction_ji';
else
    mol_fraction_ij = repmat(mol_fraction_ji', i_max, 1);
    mol_gas_fraction_ji = mol_fraction_ji(is_gas_j);
    mol_gas_fraction_ij = repmat(mol_gas_fraction_ji', i_max, 1);
end

P_i = ones(i_max,1) .* P;



relative_mass_ij = mol_fraction_ij .* repmat(MW_j', i_max, 1);
mass_fraction_ij = relative_mass_ij ./ repmat(sum(relative_mass_ij,2), 1, j_max);
avg_MW_i = mol_fraction_ij * MW_j ./ sum(mol_fraction_ij,2);

relative_gas_mass_ij = mol_gas_fraction_ij .* repmat(MW_gas_j', i_max, 1);
mass_gas_fraction_ij = relative_gas_mass_ij ./ repmat(sum(relative_gas_mass_ij,2), 1, j_gas_max);
avg_MW_gas_i = mol_gas_fraction_ij * MW_gas_j ./ sum(mol_gas_fraction_ij,2);
% disp(size(relative_gas_mass_ij))
% disp(size(mass_gas_fraction_ij))
% disp(size(avg_MW_gas_i))

if strcmp(property_string,'rho_pt')
    % using ideal gas equation: PV = nRT
    total_molar_density_i = P_i ./ (R .* T_i);
    property = total_molar_density_i .* avg_MW_gas_i;
    
elseif strcmp(property_string,'Cp_mass_pt')
    Cp_ij = zeros(size(mass_fraction_ij)); % [O2 N2 CO2 CO C H2 H20]
    Cp_ij(:,1) = interp1(tabulated_Cp_O2_mass(:,1), tabulated_Cp_O2_mass(:,2), T_i);
    Cp_ij(:,2) = interp1(tabulated_Cp_N2_mass(:,1), tabulated_Cp_N2_mass(:,2), T_i);
    Cp_ij(:,3) = interp1(tabulated_Cp_CO2_mass(:,1),tabulated_Cp_CO2_mass(:,2),T_i);
    Cp_ij(:,4) = interp1(tabulated_Cp_CO_mass(:,1), tabulated_Cp_CO_mass(:,2), T_i);
    Cp_ij(:,5) = single_value_Cp_C_mass;
    Cp_ij(:,6) = interp1(tabulated_Cp_H2_mass(:,1), tabulated_Cp_H2_mass(:,2), T_i);
    Cp_ij(:,7) = interp1(tabulated_Cp_H2O_mass(:,1), tabulated_Cp_H2O_mass(:,2), T_i);
    
    Cp_ij_weighted = Cp_ij .* mass_fraction_ij;
    property = sum(Cp_ij_weighted,2);
    
elseif strcmp(property_string,'Cp_mol_pt')
    Cp_ij = zeros(size(mol_fraction_ij)); % [O2 N2 CO2 CO C H2 H20]
    Cp_ij(:,1) = interp1(tabulated_Cp_O2_mol(:,1), tabulated_Cp_O2_mol(:,2), T_i);
    Cp_ij(:,2) = interp1(tabulated_Cp_N2_mol(:,1), tabulated_Cp_N2_mol(:,2), T_i);
    Cp_ij(:,3) = interp1(tabulated_Cp_CO2_mol(:,1),tabulated_Cp_CO2_mol(:,2),T_i);
    Cp_ij(:,4) = interp1(tabulated_Cp_CO_mol(:,1), tabulated_Cp_CO_mol(:,2), T_i);
    Cp_ij(:,5) = single_value_Cp_C_mol;
    Cp_ij(:,6) = interp1(tabulated_Cp_H2_mol(:,1), tabulated_Cp_H2O_mol(:,2), T_i);
    Cp_ij(:,7) = interp1(tabulated_Cp_H2O_mol(:,1), tabulated_Cp_H2O_mol(:,2), T_i);
    
    Cp_ij_weighted = Cp_ij .* mol_fraction_ij;
    property = sum(Cp_ij_weighted,2);
    
elseif strcmp(property_string,'tc_pt')
    tc_ij = zeros(size(mass_gas_fraction_ij)); % [O2 N2 CO2 CO C H2 H20] excluding C, H
    tc_ij(:,1) = single_value_tc_O2;
    tc_ij(:,2) = single_value_tc_N2;
    tc_ij(:,3) = single_value_tc_CO2;
    tc_ij(:,4) = single_value_tc_CO;
%     tc_ij(:,5) = single_value_tc_C;
%     tc_ij(:,6) = single_value_tc_H2;
    tc_ij(:,5) = single_value_tc_H2O;
    
    tc_ij_weighted = tc_ij .* mass_gas_fraction_ij;
    property = sum(tc_ij_weighted,2);

elseif strcmp(property_string,'my_pt')
    mu_ij = zeros(size(mass_gas_fraction_ij)); % [O2 N2 CO2 CO C H2 H20] excluding C, H
    mu_ij(:,1) = single_value_mu_O2;
    mu_ij(:,2) = single_value_mu_N2;
    mu_ij(:,3) = single_value_mu_CO2;
    mu_ij(:,4) = single_value_mu_CO;
%     mu_ij(:,5) = single_value_mu_C;
%     mu_ij(:,6) = single_value_mu_H2;
    mu_ij(:,5) = single_value_mu_H2O;
    
    mu_ij_weighted = mu_ij .* mass_gas_fraction_ij;
    property = sum(mu_ij_weighted,2);

elseif strcmp(property_string,'Pr_pt') % I'm adding this because we need the Prandtl number, and it's easier to calculate it here
    Cp_i = gas_property('Cp_mass_pt', P_i, T_i, mol_fraction_ji);
    mu_i = gas_property('my_pt', P_i, T_i, mol_fraction_ji);
    tc_i = gas_property('tc_pt', P_i, T_i, mol_fraction_ji);
    property = Cp_i .* mu_i ./ tc_i;
    
else
    throw(MException('gas_property:missingString','The gas_property string is not valid or not listed in my module'));
end
end

% property

%% from the Xsteam package script (trying to copy this as much as possible for consistency in syntax):
%*** Nomenclature ******************************************************************************************
% First the wanted property then a _ then the wanted input properties. 
% Example. T_ph is temperature as a function of pressure and enthalpy. 
% For a list of valid functions se bellow or XSteam for MS Excel.
% T     Temperature (deg C)
% p	    Pressure    (bar)
% h	    Enthalpy    (kJ/kg)
% v	    Specific volume (m3/kg)
% rho	Density
% s	    Specific entropy (kJ/(kg*K)
% u	    Specific internal energy 
% Cp	Specific isobaric heat capacity (kJ/(kg*K)
% Cv	Specific isochoric heat capacity (kJ/(kg*K)
% w	    Speed of sound (m/s)
% my	Viscosity (Dynamic Viscosity)
% tc	Thermal Conductivity
% st	Surface Tension
% x	    Vapour fraction
% vx	Vapour Volume Fraction
%
%*** Valid Steam table functions. ****************************************************************************
%
%Temperature	
%Tsat_p	Saturation temperature
%T_ph	Temperture as a function of pressure and enthalpy
%T_ps	Temperture as a function of pressure and entropy
%T_hs	Temperture as a function of enthalpy and entropy
%
%Pressure	
%psat_T	Saturation pressure
%p_hs	Pressure as a function of h and s. 
%p_hrho Pressure as a function of h and rho. Very unaccurate for solid water region since it's almost incompressible!
%
%Enthalpy	
%hV_p	Saturated vapour enthalpy
%hL_p	Saturated liquid enthalpy
%hV_T	Saturated vapour enthalpy
%hL_T	Saturated liquid enthalpy
%h_pT	Entalpy as a function of pressure and temperature.
%h_ps	Entalpy as a function of pressure and entropy.
%h_px	Entalpy as a function of pressure and vapour fraction
%h_prho	Entalpy as a function of pressure and density. Observe for low temperatures (liquid) this equation has 2 solutions. 
%h_Tx	Entalpy as a function of temperature and vapour fraction
%
%Specific volume	
%vV_p	Saturated vapour volume
%vL_p	Saturated liquid volume
%vV_T	Saturated vapour volume
%vL_T	Saturated liquid volume
%v_pT	Specific volume as a function of pressure and temperature.
%v_ph	Specific volume as a function of pressure and enthalpy
%v_ps	Specific volume as a function of pressure and entropy.
%
%Density	
%rhoV_p	Saturated vapour density
%rhoL_p	Saturated liquid density
%rhoV_T	Saturated vapour density
%rhoL_T	Saturated liquid density
%rho_pT	Density as a function of pressure and temperature.
%rho_ph	Density as a function of pressure and enthalpy
%rho_ps	Density as a function of pressure and entropy.
%
%Specific entropy 	
%sV_p	Saturated vapour entropy
%sL_p	Saturated liquid entropy
%sV_T	Saturated vapour entropy
%sL_T	Saturated liquid entropy
%s_pT	Specific entropy as a function of pressure and temperature (Returns saturated vapour entalpy if mixture.)
%s_ph	Specific entropy as a function of pressure and enthalpy
%
%Specific internal energy 	
%uV_p	Saturated vapour internal energy
%uL_p	Saturated liquid internal energy
%uV_T	Saturated vapour internal energy
%uL_T	Saturated liquid internal energy
%u_pT	Specific internal energy as a function of pressure and temperature.
%u_ph	Specific internal energy as a function of pressure and enthalpy
%u_ps	Specific internal energy as a function of pressure and entropy.
%
%Specific isobaric heat capacity 	
%CpV_p	Saturated vapour heat capacity 
%CpL_p	Saturated liquid heat capacity 
%CpV_T	Saturated vapour heat capacity 
%CpL_T	Saturated liquid heat capacity 
%Cp_pT	Specific isobaric heat capacity as a function of pressure and temperature.
%Cp_ph	Specific isobaric heat capacity as a function of pressure and enthalpy
%Cp_ps	Specific isobaric heat capacity as a function of pressure and entropy.
%
%Specific isochoric heat capacity 	
%CvV_p	Saturated vapour isochoric heat capacity
%CvL_p	Saturated liquid isochoric heat capacity
%CvV_T	Saturated vapour isochoric heat capacity
%CvL_T	Saturated liquid isochoric heat capacity
%Cv_pT	Specific isochoric heat capacity as a function of pressure and temperature.
%Cv_ph	Specific isochoric heat capacity as a function of pressure and enthalpy
%Cv_ps	Specific isochoric heat capacity as a function of pressure and entropy.
%
%Speed of sound 	
%wV_p	Saturated vapour speed of sound
%wL_p	Saturated liquid speed of sound
%wV_T	Saturated vapour speed of sound
%wL_T	Saturated liquid speed of sound
%w_pT	Speed of sound as a function of pressure and temperature.
%w_ph	Speed of sound as a function of pressure and enthalpy
%w_ps	Speed of sound as a function of pressure and entropy.
%
%Viscosity (Dynamic Viscosity)
%Viscosity is not part of IAPWS Steam IF97. Equations from 
%"Revised Release on the IAPWS Formulation 1985 for the Viscosity of Ordinary Water Substance", 2003 are used.	
%Viscosity in the mixed region (4) is interpolated according to the density. This is not true since it will be two fases.	
%my_pT	Viscosity as a function of pressure and temperature.
%my_ph	Viscosity as a function of pressure and enthalpy
%my_ps	Viscosity as a function of pressure and entropy.
%
%Thermal Conductivity	
%Revised release on the IAPS Formulation 1985 for the Thermal Conductivity of ordinary water substance (IAPWS 1998)	
%tcL_p	Saturated vapour thermal conductivity
%tcV_p	Saturated liquid thermal conductivity
%tcL_T	Saturated vapour thermal conductivity
%tcV_T	Saturated liquid thermal conductivity
%tc_pT	Thermal conductivity as a function of pressure and temperature.
%tc_ph	Thermal conductivity as a function of pressure and enthalpy
%tc_hs	Thermal conductivity as a function of enthalpy and entropy
%
%Surface tension
%st_T	Surface tension for two phase water/steam as a function of T
%st_p	Surface tension for two phase water/steam as a function of T
%Vapour fraction
%x_ph	Vapour fraction as a function of pressure and enthalpy
%x_ps	Vapour fraction as a function of pressure and entropy.
%
%Vapour volume fraction
%vx_ph	Vapour volume fraction as a function of pressure and enthalpy
%vx_ps	Vapour volume fraction as a function of pressure and entropy.
