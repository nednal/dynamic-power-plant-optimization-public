# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 07:41:32 2020

@author: Landen
"""

import numpy as np
import pandas as pd
from tensorflow.keras.models import load_model

# if we need to use python in real-time
# https://www.mathworks.com/matlabcentral/answers/458589-can-i-interact-with-a-simulink-real-time-simulation-from-python
# https://www.mathworks.com/help/matlab/matlab-engine-for-python.html

def make_prediction(past_outputs, past_inputs, future_inputs, keras_file = 'machine_learning_data/model_1', model = None): 
    
    # add folder:
    #keras_file = 'machine_learning_data/' + keras_file
    
    num_predictions = len(future_inputs)
    num_timesteps = len(past_outputs)
    num_inputs = len(past_inputs[0])
    num_features = 1 + num_predictions * num_inputs
    stacked_inputs = np.concatenate([past_inputs,future_inputs],axis=0)
    #print('stacked_inputs',np.shape(stacked_inputs))
    # build 3D input matrix
    X = np.zeros([1,num_timesteps,num_features])
    X[:,:,0] = np.reshape(past_outputs, np.shape(X[:,:,0]))
    for timestep in range(num_timesteps):
        for prediction in range(num_predictions):
            start_index = 1+prediction*num_inputs
            end_index = 1+(prediction+1)*num_inputs
            #print('X[0,timestep,start_index:end_index]',np.shape(X[0,timestep,start_index:end_index]))
            #print('stacked_inputs[timestep+prediction,:]',np.shape(stacked_inputs[timestep+prediction,:]))
            X[0,timestep,start_index:end_index] = stacked_inputs[timestep+prediction,:]
    # return X
    
    # load model
    #model = tf.keras.models.load_model(keras_file)
    if model is None:
        model = load_model(keras_file)
    #print('X',np.shape(X))
    #print('X',X)
    # make predictions
    #print('making predictions')
    predictions = model.predict(X, verbose=0)
    return predictions[-1][-1] # return the prediction that starts at the current timestep, meaning the prediction of only the future timesteps

def make_prediction_FF(current_inputs, keras_file = 'machine_learning_data/model_CV_ff_v1', model = None):
    if model is None:
        model = load_model(keras_file)
    #print('current_inputs:',current_inputs)
    return model.predict(current_inputs, verbose=0)
    
