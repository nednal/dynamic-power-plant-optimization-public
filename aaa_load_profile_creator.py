import matplotlib.pyplot as plt
import numpy as np
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})

sample_size = 12

sacle_factor = 1

for profile in range(1,6):
    
    
    # ramp down fast profile
    if profile==1:
        load_profile = np.ones(sample_size)
        load_profile = np.hstack([load_profile, np.linspace(1,0.5,24)])
        load_profile = np.hstack([load_profile, np.ones(5)*0.5])
        name = 'Type I'
        plt.figure(figsize=(2*sacle_factor, 3*sacle_factor))

    # ramp down slow profile
    if profile==2:
        load_profile = np.ones(sample_size)
        load_profile = np.hstack([load_profile, np.linspace(1,0.5,48)])
        load_profile = np.hstack([load_profile, np.ones(5)*0.5])
        name = 'Type II'
        plt.figure(figsize=(3*sacle_factor, 3*sacle_factor))


    # ramp up fast profile
    if profile==3:
        load_profile = 0.5 * np.ones(sample_size)
        load_profile = np.hstack([load_profile, np.linspace(0.5,1,24)])
        load_profile = np.hstack([load_profile, np.ones(5)])
        name = 'Type III'
        plt.figure(figsize=(2*sacle_factor, 3*sacle_factor))


    # ramp up slow profile
    if profile==4:
        load_profile = 0.5 * np.ones(sample_size)
        load_profile = np.hstack([load_profile, np.linspace(0.5,1,48)])
        load_profile = np.hstack([load_profile, np.ones(5)])
        name = 'Type IV'
        plt.figure(figsize=(3*sacle_factor, 3*sacle_factor))


    # W profile (most complicated)
    if profile==5:
        load_profile = np.ones(sample_size)
        load_profile = np.hstack([load_profile, np.linspace(1,0.5,24)])
        load_profile = np.hstack([load_profile, np.ones(5)*0.5])
        load_profile = np.hstack([load_profile, np.linspace(0.5,1,24)])
        load_profile = np.hstack([load_profile, np.ones(5)])
        load_profile = np.hstack([load_profile, np.linspace(1,0.5,24)])
        load_profile = np.hstack([load_profile, np.linspace(0.5,1,24)])
        load_profile = np.hstack([load_profile, np.ones(12)])
        name = 'Type V'
        plt.figure(figsize=(6*sacle_factor, 3*sacle_factor))

    plt.tight_layout()
    plt.title(name)
    plt.xlabel('time (minutes)')
    plt.ylabel('Power setpoint')
    plt.plot(load_profile)
    plt.savefig('machine_learning_data/profile_'+name.replace(" ", "_")+'.png', transparent=True)
    plt.savefig('machine_learning_data/profile_'+name.replace(" ", "_")+'.eps', format='eps',bbox_inches="tight")
    
plt.show()
