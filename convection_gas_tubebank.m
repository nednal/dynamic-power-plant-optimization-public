function Q_flu_gas_to_tubebank_i = convection_gas_tubebank(surface_area_i, D, C_1, C_2, ...
                                                           T_gas_i, T_pipe_i, Pr_gas, Pr_gas_surface_temp, ...
                                                           rho_gas_i, v_gas_i, k_gas, mu_gas_i)
% Fundamentals of Heat and Mass Transfer 7th edition (Bergman, Lavine, Incopera, Dewitt)
% Equation 7.58 and 7.59, with tables 7.5 and 7.6

Re_D_i = rho_gas_i .* v_gas_i .* D ./ mu_gas_i;
Nu_i = C_1 .* C_2 .* Re_D_i .* Pr_gas.^0.36 .* (Pr_gas ./ Pr_gas_surface_temp).^0.5;
h_i = Nu_i .* k_gas / D;
Q_flu_gas_to_tubebank_i = h_i .* surface_area_i .* (T_gas_i - T_pipe_i);

