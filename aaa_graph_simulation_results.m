

simulation_number = 16;

% load(strcat('simulation_results/',string(simulation_number)), 'simulation')
load(strcat('simulation_results/closed_loop',string(720)), 'simOut')
simulation = simOut;


%time                = simulation.tout;
time                = simulation.timeseries_CV.time;
CV                  = simulation.timeseries_CV.Data;
SP                  = simulation.timeseries_SP.Data;
thermal_efficiency  = simulation.timeseries_thermal_efficiency.Data;
total_power_MW      = simulation.timeseries_total_power_MW.Data;
normalized_power    = simulation.timeseries_normalized_power.Data;
power_SP            = simulation.timeseries_power_SP.Data;
fuel_fraction       = simulation.timeseries_fuel_fraction.Data;
% load_fraction       = simulation.timeseries_load_fraction.Data;
n_inj_i             = simulation.timeseries_n_inj_i.Data;
% added for large computer run:
n_inj_noBias_i      = simulation.timeseries_n_inj_noBias_i.Data;

% squeeze() removes all dimensions of length 1. CV has a bunch of length-1 dimensions
CV = squeeze(CV);
if length(SP)==1
    SP = SP .* ones(size(time));
end


figure()
subplot(4,1,1);
hold on 
plot(time,CV,'DisplayName','CV')
plot(time,SP,'DisplayName','SP')
legend()
hold off 

subplot(4,1,2); 
hold on 
plot(time,thermal_efficiency,'DisplayName','thermal efficiency')
legend()
hold off

subplot(4,1,3); 
hold on 
plot(time,fuel_fraction,'DisplayName','load fraction')% (left axis)')
plot(time,normalized_power,'DisplayName','normalized power')% (left axis)')
plot(time,power_SP,'DisplayName','power SP')% (left axis)')
% yyaxis right
% plot(time,total_power_MW,'DisplayName','total power (right axis)')
legend()
hold off

subplot(4,1,4); 
hold on 
for i=1:9
    if i < 7
        plot(time,n_inj_i(:,i),'DisplayName',strcat('air inj level #',string(i)))
    elseif i > 7
        plot(time,n_inj_i(:,i),'DisplayName',strcat('air inj level #',string(i)),'Linestyle','--')
    end
end
plot(time,n_inj_noBias_i(:,1),'DisplayName','unbiased air inj #1-6','LineWidth',2)
plot(time,n_inj_noBias_i(:,8),'DisplayName','unbiased air inj #8-9','LineWidth',2,'Linestyle','--')
% plot the two "nobias" lines in bold for comparison:
% n_inj_noBias_i
legend()
hold off